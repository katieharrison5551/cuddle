//
//  APIHeaderProtocol.swift
//  cuddle
//
//  Created by Saeed on 8/9/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

protocol APIHeaderProtocol {
    func getDefaultHeader() -> [String: String]
    func getTokenHeader() -> [String: String]
}
extension APIHeaderProtocol {
    func getDefaultHeader() -> [String: String] {
        if let token = UserInfoModel.shared.getUserToken() {
            return ["Content-Type": "application/json", "Accept": "application/json", "Authorization": token]
        }
        return ["Content-Type": "application/json", "Accept": "application/json"]
    }
    func getTokenHeader() -> [String: String] {
        if let header =  "sampleClient:sampleSecret".data(using: .utf8)?.base64EncodedString() {
            return ["Content-Type": "application/x-www-form-urlencoded", "Accept": "application/json", "Authorization": "Basic \(header)"]
        }
        return getDefaultHeader()
    }
}
