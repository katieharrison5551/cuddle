//
//  ProfileUserService.swift
//  cuddle
//
//  Created by MohammadReza on 8/17/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias ProfileUserServiceComplitationHandler = ((Bool) -> Void)

protocol ProfileUserServiceProtocol {
    func loadProfileUser(withID: String, completionHandler : @escaping ProfileUserServiceComplitationHandler)
    func updateProfileUser(profileModel: ProfileModel, completionHandler : @escaping ProfileUserServiceComplitationHandler)
}
class ProfileUserService: ClientAPI {
}
extension ProfileUserService: ProfileUserServiceProtocol {
    func loadProfileUser(withID: String, completionHandler: @escaping ProfileUserServiceComplitationHandler) {
        completionHandler(true)
    }
    func updateProfileUser(profileModel: ProfileModel, completionHandler: @escaping ProfileUserServiceComplitationHandler) {
        completionHandler(true)
    }
}
