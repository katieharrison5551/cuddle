//
//  ClientAPI.swift
//  cuddle
//
//  Created by Saeed on 8/9/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

class ClientAPI: APIHeaderProtocol, NetworkValidationProtocol {
    init() {
        SwaggerClientAPI.customHeaders = getDefaultHeader()
    }
}
