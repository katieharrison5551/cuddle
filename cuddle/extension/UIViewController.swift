//
//  UIViewController.swift
//  cuddle
//
//  Created by MohammadReza on 8/10/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    class var storyboardID: String {
        return "\(self)"
    }
    static func instantiate(fromAppStoryboard: AppStoryboard) -> Self {
        return fromAppStoryboard.viewController(viewControllerClass: self)
    }

    func showErrorDialog(title: String? = "error".getString(), message: String, okButtonTitle: String?, cancelButtonTitle: String?, okButtonCompletion: ButtonDelegate? = nil, cancelButtonCompletion: ButtonDelegate? = nil) {
        let errorVC = AppStoryboard.CustomDialog.viewController(viewControllerClass: CustomErrorViewController.self)
        errorVC.setupAlert(title: title!, desc: message, okButtonTitle: okButtonTitle, cancelButtonTitle: cancelButtonTitle, okButtonCompletion: okButtonCompletion, cancelButtonCompletion: cancelButtonCompletion)
        errorVC.providesPresentationContextTransitionStyle = true
        errorVC.definesPresentationContext = true
        errorVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        errorVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve

        self.present(errorVC, animated: true, completion: nil)
    }

    func showActionSheet(title: String, message: String, style: UIAlertController.Style, actions: [UIAlertAction?]) {
        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: style)
        for action in actions {
            if let action = action {
                actionSheetController.addAction(action)
            }
        }
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func actionMessageOK(_ completion: (() -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default) { _ in
            completion?()
        }
    }
    func actionMessageCancel(_ completion: (() -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel) { _ in
            completion?()
        }
    }
    func actionMessageClose(_ completion: (() -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("close", comment: ""), style: .default) { _ in
            completion?()
        }
    }

    func actionMessageRetry(_ completion: (() -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: NSLocalizedString("retry", comment: ""), style: .default) { _ in
            completion?()
        }
    }

    func actionMessage(_ title: String, style: UIAlertAction.Style, _ completion: (() -> Void)? = nil) -> UIAlertAction {
        return UIAlertAction(title: title, style: style) { _ in
            completion?()
        }
    }
}
