//
//  UIView.swift
//  VASL App
//
//  Created by Siamak on 10/29/18.
//  Copyright © 2018 VASL. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setGradientsBackground(_ leftColor: UIColor, _ rightColor: UIColor, _ radius: CGFloat) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [leftColor.cgColor, rightColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradientLayer.cornerRadius = radius
        gradientLayer.frame = self.bounds
        layer.name = "gradient"
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    @IBDesignable
    class FAUGradientView: UIView {
        @IBInspectable var firstColor: UIColor = UIColor.clear
        @IBInspectable var secondColor: UIColor = UIColor.clear
        @IBInspectable var startPoint: CGPoint = CGPoint(x: 0.0, y: 1.0)
        @IBInspectable var endPoint: CGPoint = CGPoint(x: 1.0, y: 0.0)
        var gradientLayer: CAGradientLayer!
        override func draw(_ rect: CGRect) {
            super.draw(rect)
            gradientLayer = CAGradientLayer()
            self.gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
            self.gradientLayer.startPoint = self.startPoint
            self.gradientLayer.endPoint = self.endPoint
            self.gradientLayer.frame = self.frame
            self.layer.addSublayer(self.gradientLayer)
        }
    }
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        } set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        } set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        } set {
            layer.shadowRadius = newValue
        }
    }
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        } set {
            layer.shadowOpacity = newValue
        }
    }
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        } set {
            layer.shadowOffset = newValue
        }
    }
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        } set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        } set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 5.0,
                   cornerRadius: CGFloat = 20) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.cornerRadius = cornerRadius
    }
    func showNoDataView(titleDesc: String, viewFrame: CGRect, color: UIColor) {

        let noDataLabel = UILabel(frame: CGRect(x: viewFrame.origin.x, y: viewFrame.origin.y, width: viewFrame.size.width, height: viewFrame.size.height))
        noDataLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        noDataLabel.textAlignment = .center
        noDataLabel.font = UIFont(name: CustomFont.fontBold, size: 18.0)
        noDataLabel.textColor = color
        noDataLabel.numberOfLines = 0
        noDataLabel.text = titleDesc
        noDataLabel.tag = 222
        self.addSubview(noDataLabel)
    }

    func removeNoDataView() {
        let subViews = self.subviews
        for subview in subViews {
            if subview.tag == 222 {
                subview.removeFromSuperview()
            }
        }
    }
}
