//
//  Date.swift
//  cuddle
//
//  Created by Saeed on 11/14/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

extension Date {
    func getString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        return formatter.string(from: self)
    }

    func getWeekDay() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: self)
    }
}
