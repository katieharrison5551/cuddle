//
//  UIColor.swift
//  VASL App
//
//  Created by Siamak on 10/29/18.
//  Copyright © 2018 VASL. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    // MARK: Cuddle colors for public reuse
    static let lightRed = UIColor(named: "lightRed") ?? .red
    static let darkRed = UIColor(named: "darkRed") ?? .red
    static let darkestRed = UIColor(named: "darkestRed") ?? .red
    static let clightGray = UIColor(named: "clightGray") ?? .red
    static let cmiddleGray = UIColor(named: "cmiddleGray") ?? .red
    static let cdarkGray = UIColor(named: "cdarkGray") ?? .red
}
