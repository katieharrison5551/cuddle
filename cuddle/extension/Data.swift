//
//  Data.swift
//  cuddle
//
//  Created by Saeed on 10/4/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

extension Data {
    public var bytes: [UInt8] {
      Array(self)
    }
    public func toHexString() -> String {
      self.bytes.toHexString()
    }
}
