//
//  Array.swift
//  cuddle
//
//  Created by Saeed on 10/4/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

public extension Array where Element == UInt8 {
  func toBase64() -> String? {
    Data( self).base64EncodedString()
  }

  init(base64: String) {
    self.init()

    guard let decodedData = Data(base64Encoded: base64) else {
      return
    }

    append(contentsOf: decodedData.bytes)
  }
}

extension Array where Element == UInt8 {
    public func toHexString() -> String {
      `lazy`.reduce(into: "") {
        var result = String($1, radix: 16)
        if result.count == 1 {
            result = "0" + result
        }
        $0 += result
      }
    }
}
