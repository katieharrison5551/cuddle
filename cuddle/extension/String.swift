//
//  String.swift
//  VASL App
//
//  Created by Siamak on 10/29/18.
//  Copyright © 2018 VASL. All rights reserved.
//

import Foundation
import UIKit
import CommonCrypto

extension String {
    var md5: String {
        let data = Data(self.utf8)
        let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
            var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
            CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
            return hash
        }
        return hash.map { String(format: "%02x", $0) }.joined()
    }

    func getString() -> String {
        return NSLocalizedString(self, comment: self)
    }

    func showPrice(price: Int) -> String {
        if price == 0 {
            return "0"
        }
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal

        if let formattedString = formatter.string(for: price) {
        return "\("currency".getString())\(formattedString)"
        }
        return ""
    }
    func showPrice(price: Int64) -> String {
        return showPrice(price: Int(price))
    }
}
extension String {
    func allStringsBetween(_ start: String, andString end: String) -> String {
        var strings = String()
        var startRange: NSRange = (self as NSString).range(of: start)
        while true {
            if startRange.location != NSNotFound {
                var targetRange = NSRange()
                targetRange.location = startRange.location + startRange.length
                targetRange.length = self.count - targetRange.location
                let endRange: NSRange = (self as NSString).range(of: end, options: [], range: targetRange)
                if endRange.location != NSNotFound {
                    targetRange.length = endRange.location - targetRange.location
                    strings.append((self as NSString).substring(with: targetRange))
                    var restOfString =  NSRange()
                    restOfString.location = endRange.location + endRange.length
                    restOfString.length = self.count - restOfString.location
                    startRange = (self as NSString).range(of: start, options: [], range: restOfString)
                } else {
                    break
                }
            } else {
                break
            }
        }
        return strings
    }
    func components(withMaxLength length: Int) -> [String] {
        return stride(from: 0, to: self.count, by: length).map {
            let start = self.index(self.startIndex, offsetBy: $0)
            let end = self.index(start, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
            return String(self[start..<end])
        }
    }
}

extension String {
    public var bytes: [UInt8] {
        data(using: String.Encoding.utf8, allowLossyConversion: true)?.bytes ?? Array(utf8)
    }
}
