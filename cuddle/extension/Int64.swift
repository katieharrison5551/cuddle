//
//  Int64.swift
//  cuddle
//
//  Created by Saeed on 10/14/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

extension Int64 {
    func toDate() -> String {
        let timeInSecond = self / 1000
        let date = Date(timeIntervalSince1970: Double(integerLiteral: timeInSecond))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }

    func calculateDuration() -> String {
        let time = self / 1000
        let second = time % 60
        let minute = time / 60 % 60
        let hours = time / 60 / 60

        return String.init(format: "%02d:%02d:%02d", hours, minute, second)
    }
}
