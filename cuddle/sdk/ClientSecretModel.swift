//
//  AuthenticationAPI.swift
//  cuddle
//
//  Created by Shima on 10/13/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//
import Foundation
public struct ClientSecretModel : Codable {
    let paymentSecret : String?
    public enum CodingKeys: String, CodingKey {
        
        case paymentSecret = "paymentSecret"
    }
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        paymentSecret = try values.decodeIfPresent(String.self, forKey: .paymentSecret)
    }
}
