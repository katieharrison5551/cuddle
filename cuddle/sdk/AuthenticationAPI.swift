//
//  AuthenticationAPI.swift
//  cuddle
//
//  Created by Shima on 9/28/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import Alamofire

open class AuthenticationAPI {
    open class func postTokenRequest(username: String? = nil, password: String? = nil, completion: @escaping ((_ data: AccessTokenModel?,_ error: Error?) -> Void)) {
        postTokenRequestWithRequestBuilder(username: username, password: password).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }
    open class func postTokenRequestWithRequestBuilder(username: String? = nil, password: String? = nil) -> RequestBuilder<AccessTokenModel> {
        let path = "/oauth/token"
        let URLString = SwaggerClientAPI.basePath + path
        let formParams: [String: Any?] = [
            "grant_type" : "password",
            "username": username,
            "password": password]
        let nonNullParameters = APIHelper.rejectNil(formParams)
        let parameters = APIHelper.convertBoolToString(nonNullParameters)
        let url = NSURLComponents(string: URLString)
        let requestBuilder: RequestBuilder<AccessTokenModel>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()
        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }
    
}
