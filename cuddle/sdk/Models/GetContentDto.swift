//
// GetContentDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct GetContentDto: Codable {

    public enum Language: String, Codable { 
        case en = "EN"
        case fa = "FA"
        case fr = "FR"
    }
    public enum Status: String, Codable { 
        case draft = "DRAFT"
        case published = "PUBLISHED"
    }
    public var author: Author?
    public var averageRate: Float?
    public var banner: ContentRowDto?
    public var catsId: [String]?
    public var children: [GetContentDto]?
    public var content: [ContentRowDto]?
    public var _id: String?
    public var language: Language?
    public var lat: Double?
    public var lng: Double?
    public var metaData: [String:String]?
    public var publishDate: Int64?
    public var ratesCount: Int?
    public var ratesSum: Int?
    public var status: Status?
    public var studyDuration: Int?
    public var summary: ContentRowDto?
    public var tagsId: [String]?
    public var title: ContentRowDto?
    public var type: String?
    public var visitedCount: Int?

    public init(author: Author?, averageRate: Float?, banner: ContentRowDto?, catsId: [String]?, children: [GetContentDto]?, content: [ContentRowDto]?, _id: String?, language: Language?, lat: Double?, lng: Double?, metaData: [String:String]?, publishDate: Int64?, ratesCount: Int?, ratesSum: Int?, status: Status?, studyDuration: Int?, summary: ContentRowDto?, tagsId: [String]?, title: ContentRowDto?, type: String?, visitedCount: Int?) {
        self.author = author
        self.averageRate = averageRate
        self.banner = banner
        self.catsId = catsId
        self.children = children
        self.content = content
        self._id = _id
        self.language = language
        self.lat = lat
        self.lng = lng
        self.metaData = metaData
        self.publishDate = publishDate
        self.ratesCount = ratesCount
        self.ratesSum = ratesSum
        self.status = status
        self.studyDuration = studyDuration
        self.summary = summary
        self.tagsId = tagsId
        self.title = title
        self.type = type
        self.visitedCount = visitedCount
    }

    public enum CodingKeys: String, CodingKey { 
        case author
        case averageRate
        case banner
        case catsId
        case children
        case content
        case _id = "id"
        case language
        case lat
        case lng
        case metaData
        case publishDate
        case ratesCount
        case ratesSum
        case status
        case studyDuration
        case summary
        case tagsId
        case title
        case type
        case visitedCount
    }


}

