//
// TicketMessageDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct TicketMessageDto: Codable {

    public var ticketId: String?
    public var content: String?
    public var subscriberId: String?
    public var _id: String?
    public var createdDateMilli: Int64?
    public var fullName: String?

    public init(ticketId: String?, content: String?, subscriberId: String?, _id: String?, createdDateMilli: Int64?, fullName: String?) {
        self.ticketId = ticketId
        self.content = content
        self.subscriberId = subscriberId
        self._id = _id
        self.createdDateMilli = createdDateMilli
        self.fullName = fullName
    }

    public enum CodingKeys: String, CodingKey { 
        case ticketId
        case content
        case subscriberId
        case _id = "id"
        case createdDateMilli
        case fullName
    }


}

