//
// ActivationResponseDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ActivationResponseDto: Codable {

    public var username: String?

    public init(username: String?) {
        self.username = username
    }


}

