//
// PushTokenDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct PushTokenDto: Codable {

    public enum ModelType: String, Codable { 
        case unspecified = "UNSPECIFIED"
        case andriod = "ANDRIOD"
        case ios = "IOS"
        case web = "WEB"
    }
    public var _id: String?
    public var type: ModelType?
    public var token: String?

    public init(_id: String?, type: ModelType?, token: String?) {
        self._id = _id
        self.type = type
        self.token = token
    }

    public enum CodingKeys: String, CodingKey { 
        case _id = "id"
        case type
        case token
    }


}

