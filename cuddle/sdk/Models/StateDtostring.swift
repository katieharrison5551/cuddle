//
// StateDtostring.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct StateDtostring: Codable {

    public enum State: String, Codable { 
        case opened = "OPENED"
        case processing = "PROCESSING"
        case closed = "CLOSED"
    }
    public var at: Int64?
    public var state: State?

    public init(at: Int64?, state: State?) {
        self.at = at
        self.state = state
    }


}

