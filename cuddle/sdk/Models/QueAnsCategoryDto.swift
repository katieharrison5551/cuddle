//
// QueAnsCategoryDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct QueAnsCategoryDto: Codable {

    public var _id: String?
    public var parentId: String?
    public var title: String?
    public var _description: String?

    public init(_id: String?, parentId: String?, title: String?, _description: String?) {
        self._id = _id
        self.parentId = parentId
        self.title = title
        self._description = _description
    }

    public enum CodingKeys: String, CodingKey { 
        case _id = "id"
        case parentId
        case title
        case _description = "description"
    }


}

