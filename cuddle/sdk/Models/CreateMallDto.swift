//
// CreateMallDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct CreateMallDto: Codable {

    public enum Language: String, Codable { 
        case en = "EN"
        case fa = "FA"
        case fr = "FR"
    }
    public enum Status: String, Codable { 
        case draft = "DRAFT"
        case published = "PUBLISHED"
    }
    public var banner: ContentRowDto?
    public var catsId: [String]?
    public var closingTime: String?
    public var content: [ContentRowDto]?
    public var costPerHour: Int64?
    public var initialFee: Int64?
    public var language: Language
    public var lat: Double
    public var lng: Double
    public var metaData: [String:String]?
    public var openingTime: String?
    public var publishDate: String
    public var status: Status
    public var summary: ContentRowDto?
    public var tagsId: [String]?
    public var title: ContentRowDto?

    public init(banner: ContentRowDto?, catsId: [String]?, closingTime: String?, content: [ContentRowDto]?, costPerHour: Int64?, initialFee: Int64?, language: Language, lat: Double, lng: Double, metaData: [String:String]?, openingTime: String?, publishDate: String, status: Status, summary: ContentRowDto?, tagsId: [String]?, title: ContentRowDto?) {
        self.banner = banner
        self.catsId = catsId
        self.closingTime = closingTime
        self.content = content
        self.costPerHour = costPerHour
        self.initialFee = initialFee
        self.language = language
        self.lat = lat
        self.lng = lng
        self.metaData = metaData
        self.openingTime = openingTime
        self.publishDate = publishDate
        self.status = status
        self.summary = summary
        self.tagsId = tagsId
        self.title = title
    }


}

