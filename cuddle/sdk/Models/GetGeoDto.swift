//
// GetGeoDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct GetGeoDto: Codable {

    public var lat: Double
    public var lng: Double
    public var metaData: [String:String]?
    public var tagsTitle: [String]
    public var type: String

    public init(lat: Double, lng: Double, metaData: [String:String]?, tagsTitle: [String], type: String) {
        self.lat = lat
        self.lng = lng
        self.metaData = metaData
        self.tagsTitle = tagsTitle
        self.type = type
    }


}

