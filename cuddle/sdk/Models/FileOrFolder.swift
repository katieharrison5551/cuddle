//
// FileOrFolder.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct FileOrFolder: Codable {

    public enum ModelType: String, Codable { 
        case file = "FILE"
        case folder = "FOLDER"
    }
    public var bucketName: String?
    public var childFolders: [FileOrFolder]?
    public var filename: String?
    public var _id: String?
    public var name: String?
    public var parentId: String?
    public var type: ModelType?
    public var uploadedAt: Int64?
    public var url: String?
    public var uuid: String?

    public init(bucketName: String?, childFolders: [FileOrFolder]?, filename: String?, _id: String?, name: String?, parentId: String?, type: ModelType?, uploadedAt: Int64?, url: String?, uuid: String?) {
        self.bucketName = bucketName
        self.childFolders = childFolders
        self.filename = filename
        self._id = _id
        self.name = name
        self.parentId = parentId
        self.type = type
        self.uploadedAt = uploadedAt
        self.url = url
        self.uuid = uuid
    }

    public enum CodingKeys: String, CodingKey { 
        case bucketName
        case childFolders
        case filename
        case _id = "id"
        case name
        case parentId
        case type
        case uploadedAt
        case url
        case uuid
    }


}

