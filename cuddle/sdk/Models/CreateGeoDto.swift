//
// CreateGeoDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct CreateGeoDto: Codable {

    public var lat: Double
    public var lng: Double
    public var metaData: [String:String]?
    public var relatedId: String?
    public var type: String

    public init(lat: Double, lng: Double, metaData: [String:String]?, relatedId: String?, type: String) {
        self.lat = lat
        self.lng = lng
        self.metaData = metaData
        self.relatedId = relatedId
        self.type = type
    }


}

