//
// StripeTransactionProcessDto.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct StripeTransactionProcessDto: Codable {

    public var cardId: String?

    public init(cardId: String?) {
        self.cardId = cardId
    }


}

