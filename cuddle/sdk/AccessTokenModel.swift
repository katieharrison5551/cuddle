
import Foundation
public struct AccessTokenModel : Codable {
    public var access_token : String?
    public var token_type : String?
    public var refresh_token : String?
    public var expires_in : Int64?
    public enum  CodingKeys: String, CodingKey {
        case access_token = "access_token"
        case token_type = "token_type"
        case refresh_token = "refresh_token"
        case expires_in = "expires_in"
    }
    public init(access_token: String?, token_type: String?, refresh_token: String?, expires_in: Int64?) {
           self.access_token = access_token
           self.token_type = token_type
           self.refresh_token = refresh_token
           self.expires_in = expires_in
       }
    
}
