//
//  StripeClientSecretAPI.swift
//  cuddle
//
//  Created by Shima on 10/13/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

open class StripeClientSecretAPI {
    open class func getClientSecret(url: String, completion: @escaping ((_ data: ClientSecretModel?,_ error: Error?) -> Void)) {
        getClientSecretWithRequestBuilder(url: url).execute { (response, error) -> Void in
            completion(response?.body, error);
        }
    }
    open class func getClientSecretWithRequestBuilder(url: String) -> RequestBuilder<ClientSecretModel> {
        let URLString = url
        let url = NSURLComponents(string: URLString)
        let requestBuilder: RequestBuilder<ClientSecretModel>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()
        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: [:], isBody: false)
    }
}
