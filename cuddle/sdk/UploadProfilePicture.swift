//
//  UploadProfilePicture.swift
//  cuddle
//
//  Created by Saeed on 11/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import Alamofire

class UploadProfilePicture {
    func uploadPicture(imageData: Data, completion: @escaping ((Data?) -> Void)) {
        var url = SwaggerClientAPI.basePath
        url = "\(url)/api/v1/subscriber/profile/picture"
        guard let token = UserInfoModel.shared.getUserToken() else {
            return
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(imageData, withName: "file", fileName: "file", mimeType: "image/png")
        }, to: url, headers: ["accept":"*/*","Content-Type":"multipart/form-data","Authorization":token]) { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            print("Upload Progress: \(progress.fractionCompleted)")
                        })

                        upload.responseJSON { response in
                            if let JSON = response.data {

                                completion(JSON)
                                return
                            }

                            completion(nil)
                        }

                    case .failure(let encodingError):
                        print("Error")
                        print(encodingError)
                        completion(nil)
                    }
                }
    }
}
