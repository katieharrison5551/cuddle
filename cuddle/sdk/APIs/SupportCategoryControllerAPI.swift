//
// SupportCategoryControllerAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire




public typealias createCategoryCompletation = ((_ data: SupportCategoryDto?,_ error: Error?) -> Void)

public protocol createCategoryProtocol {
    func createCategory(supportCategoryDto: SupportCategoryDto, completion: @escaping createCategoryCompletation)
}

extension SupportCategoryControllerAPI: createCategoryProtocol{
    /**
     createCategory
     
     - parameter supportCategoryDto: (body) supportCategoryDto 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func createCategory(supportCategoryDto: SupportCategoryDto, completion: @escaping createCategoryCompletation) {
        SupportCategoryControllerAPI.createCategoryWithRequestBuilder(supportCategoryDto: supportCategoryDto).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias deleteCategory1Completation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol deleteCategory1Protocol {
    func deleteCategory1(_id: String, completion: @escaping deleteCategory1Completation)
}

extension SupportCategoryControllerAPI: deleteCategory1Protocol{
    /**
     deleteCategory
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func deleteCategory1(_id: String, completion: @escaping deleteCategory1Completation) {
        SupportCategoryControllerAPI.deleteCategory1WithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}


public typealias getCategory1Completation = ((_ data: SupportCategoryDto?,_ error: Error?) -> Void)

public protocol getCategory1Protocol {
    func getCategory1(_id: String, completion: @escaping getCategory1Completation)
}

extension SupportCategoryControllerAPI: getCategory1Protocol{
    /**
     getCategory
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getCategory1(_id: String, completion: @escaping getCategory1Completation) {
        SupportCategoryControllerAPI.getCategory1WithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}

    /**
     * enum for parameter sort
     */
    public enum Sort_getCategoryList: String { 
        case asc = "ASC"
        case desc = "DESC"
    }


public typealias getCategoryListCompletation = ((_ data: PageSupportCategoryDto?,_ error: Error?) -> Void)

public protocol getCategoryListProtocol {
    func getCategoryList(pageNumber: String?, pageSize: String?, sort: Sort_getCategoryList?, sortKey: [String]?, completion: @escaping getCategoryListCompletation)
}

extension SupportCategoryControllerAPI: getCategoryListProtocol{
    /**
     getCategoryList
     
     - parameter pageNumber: (query) starts from 0 (optional)
     - parameter pageSize: (query) must be greater than 0 (optional)
     - parameter sort: (query) ASC&#x3D;ascending ,DESC&#x3D;descending (optional)
     - parameter sortKey: (query) sort will be based on these words (optional)
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getCategoryList(pageNumber: String?, pageSize: String?, sort: Sort_getCategoryList?, sortKey: [String]?, completion: @escaping getCategoryListCompletation) {
        SupportCategoryControllerAPI.getCategoryListWithRequestBuilder(pageNumber: pageNumber, pageSize: pageSize, sort: sort, sortKey: sortKey).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias updateCategoryCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol updateCategoryProtocol {
    func updateCategory(_id: String, supportCategoryDto: SupportCategoryDto, completion: @escaping updateCategoryCompletation)
}

extension SupportCategoryControllerAPI: updateCategoryProtocol{
    /**
     updateCategory
     
     - parameter _id: (path) id 
     - parameter supportCategoryDto: (body) supportCategoryDto 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func updateCategory(_id: String, supportCategoryDto: SupportCategoryDto, completion: @escaping updateCategoryCompletation) {
        SupportCategoryControllerAPI.updateCategoryWithRequestBuilder(_id: _id, supportCategoryDto: supportCategoryDto).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}




public class SupportCategoryControllerAPI {

public init(){
    
}



    /**
     createCategory
     - POST /api/v1/support/categories
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter supportCategoryDto: (body) supportCategoryDto 

     - returns: RequestBuilder<SupportCategoryDto> 
     */
    open class func createCategoryWithRequestBuilder(supportCategoryDto: SupportCategoryDto) -> RequestBuilder<SupportCategoryDto> {
        let path = "/api/v1/support/categories"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: supportCategoryDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<SupportCategoryDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }



    /**
     deleteCategory
     - DELETE /api/v1/support/categories/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<Void> 
     */
    open class func deleteCategory1WithRequestBuilder(_id: String) -> RequestBuilder<Void> {
        var path = "/api/v1/support/categories/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "DELETE", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getCategory
     - GET /api/v1/support/categories/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<SupportCategoryDto> 
     */
    open class func getCategory1WithRequestBuilder(_id: String) -> RequestBuilder<SupportCategoryDto> {
        var path = "/api/v1/support/categories/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<SupportCategoryDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getCategoryList
     - GET /api/v1/support/categories
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter pageNumber: (query) starts from 0 (optional)
     - parameter pageSize: (query) must be greater than 0 (optional)
     - parameter sort: (query) ASC&#x3D;ascending ,DESC&#x3D;descending (optional)
     - parameter sortKey: (query) sort will be based on these words (optional)

     - returns: RequestBuilder<PageSupportCategoryDto> 
     */
    open class func getCategoryListWithRequestBuilder(pageNumber: String? = nil, pageSize: String? = nil, sort: Sort_getCategoryList? = nil, sortKey: [String]? = nil) -> RequestBuilder<PageSupportCategoryDto> {
        let path = "/api/v1/support/categories"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
            "pageNumber": pageNumber, 
            "pageSize": pageSize, 
            "sort": sort?.rawValue, 
            "sortKey": sortKey
        ])

        let requestBuilder: RequestBuilder<PageSupportCategoryDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     updateCategory
     - PUT /api/v1/support/categories/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 
     - parameter supportCategoryDto: (body) supportCategoryDto 

     - returns: RequestBuilder<Void> 
     */
    open class func updateCategoryWithRequestBuilder(_id: String, supportCategoryDto: SupportCategoryDto) -> RequestBuilder<Void> {
        var path = "/api/v1/support/categories/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: supportCategoryDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "PUT", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

}
