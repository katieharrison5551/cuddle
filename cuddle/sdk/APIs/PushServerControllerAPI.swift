//
// PushServerControllerAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire




public typealias createPushServerCompletation = ((_ data: PushServerDto?,_ error: Error?) -> Void)

public protocol createPushServerProtocol {
    func createPushServer(pushServerDto: PushServerDto, completion: @escaping createPushServerCompletation)
}

extension PushServerControllerAPI: createPushServerProtocol{
    /**
     create
     
     - parameter pushServerDto: (body) pushServerDto 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func createPushServer(pushServerDto: PushServerDto, completion: @escaping createPushServerCompletation) {
        PushServerControllerAPI.createPushServerWithRequestBuilder(pushServerDto: pushServerDto).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias deletePushServerCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol deletePushServerProtocol {
    func deletePushServer(_id: String, completion: @escaping deletePushServerCompletation)
}

extension PushServerControllerAPI: deletePushServerProtocol{
    /**
     deletePushServer
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func deletePushServer(_id: String, completion: @escaping deletePushServerCompletation) {
        PushServerControllerAPI.deletePushServerWithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}

    /**
     * enum for parameter sort
     */
    public enum Sort_getAllPushServers: String { 
        case asc = "ASC"
        case desc = "DESC"
    }


public typealias getAllPushServersCompletation = ((_ data: PagePushServerDto?,_ error: Error?) -> Void)

public protocol getAllPushServersProtocol {
    func getAllPushServers(pageNumber: String?, pageSize: String?, sort: Sort_getAllPushServers?, sortKey: [String]?, completion: @escaping getAllPushServersCompletation)
}

extension PushServerControllerAPI: getAllPushServersProtocol{
    /**
     getAllPushServers
     
     - parameter pageNumber: (query) starts from 0 (optional)
     - parameter pageSize: (query) must be greater than 0 (optional)
     - parameter sort: (query) ASC&#x3D;ascending ,DESC&#x3D;descending (optional)
     - parameter sortKey: (query) sort will be based on these words (optional)
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getAllPushServers(pageNumber: String?, pageSize: String?, sort: Sort_getAllPushServers?, sortKey: [String]?, completion: @escaping getAllPushServersCompletation) {
        PushServerControllerAPI.getAllPushServersWithRequestBuilder(pageNumber: pageNumber, pageSize: pageSize, sort: sort, sortKey: sortKey).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias getPushServerCompletation = ((_ data: PushServerDto?,_ error: Error?) -> Void)

public protocol getPushServerProtocol {
    func getPushServer(_id: String, completion: @escaping getPushServerCompletation)
}

extension PushServerControllerAPI: getPushServerProtocol{
    /**
     getPushServer
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getPushServer(_id: String, completion: @escaping getPushServerCompletation) {
        PushServerControllerAPI.getPushServerWithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias updatePushServerCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol updatePushServerProtocol {
    func updatePushServer(_id: String, pushServerDto: PushServerDto, completion: @escaping updatePushServerCompletation)
}

extension PushServerControllerAPI: updatePushServerProtocol{
    /**
     update
     
     - parameter _id: (path) id 
     - parameter pushServerDto: (body) pushServerDto 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func updatePushServer(_id: String, pushServerDto: PushServerDto, completion: @escaping updatePushServerCompletation) {
        PushServerControllerAPI.updatePushServerWithRequestBuilder(_id: _id, pushServerDto: pushServerDto).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}




public class PushServerControllerAPI {

public init(){
    
}



    /**
     create
     - POST /api/v1/mgs/push/server
     - don't fill id
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter pushServerDto: (body) pushServerDto 

     - returns: RequestBuilder<PushServerDto> 
     */
    open class func createPushServerWithRequestBuilder(pushServerDto: PushServerDto) -> RequestBuilder<PushServerDto> {
        let path = "/api/v1/mgs/push/server"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: pushServerDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<PushServerDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }



    /**
     deletePushServer
     - DELETE /api/v1/mgs/push/server/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<Void> 
     */
    open class func deletePushServerWithRequestBuilder(_id: String) -> RequestBuilder<Void> {
        var path = "/api/v1/mgs/push/server/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "DELETE", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getAllPushServers
     - GET /api/v1/mgs/push/server
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter pageNumber: (query) starts from 0 (optional)
     - parameter pageSize: (query) must be greater than 0 (optional)
     - parameter sort: (query) ASC&#x3D;ascending ,DESC&#x3D;descending (optional)
     - parameter sortKey: (query) sort will be based on these words (optional)

     - returns: RequestBuilder<PagePushServerDto> 
     */
    open class func getAllPushServersWithRequestBuilder(pageNumber: String? = nil, pageSize: String? = nil, sort: Sort_getAllPushServers? = nil, sortKey: [String]? = nil) -> RequestBuilder<PagePushServerDto> {
        let path = "/api/v1/mgs/push/server"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
            "pageNumber": pageNumber, 
            "pageSize": pageSize, 
            "sort": sort?.rawValue, 
            "sortKey": sortKey
        ])

        let requestBuilder: RequestBuilder<PagePushServerDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getPushServer
     - GET /api/v1/mgs/push/server/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<PushServerDto> 
     */
    open class func getPushServerWithRequestBuilder(_id: String) -> RequestBuilder<PushServerDto> {
        var path = "/api/v1/mgs/push/server/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<PushServerDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     update
     - PUT /api/v1/mgs/push/server/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 
     - parameter pushServerDto: (body) pushServerDto 

     - returns: RequestBuilder<Void> 
     */
    open class func updatePushServerWithRequestBuilder(_id: String, pushServerDto: PushServerDto) -> RequestBuilder<Void> {
        var path = "/api/v1/mgs/push/server/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: pushServerDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "PUT", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

}
