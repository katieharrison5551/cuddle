//
// ReferralUserControllerAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire




public typealias createReferralUserCompletation = ((_ data: ReferralUserDto?,_ error: Error?) -> Void)

public protocol createReferralUserProtocol {
    func createReferralUser(completion: @escaping createReferralUserCompletation)
}

extension ReferralUserControllerAPI: createReferralUserProtocol{
    /**
     create
     
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func createReferralUser(completion: @escaping createReferralUserCompletation) {
        ReferralUserControllerAPI.createReferralUserWithRequestBuilder().execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias deleteReferralUserCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol deleteReferralUserProtocol {
    func deleteReferralUser(_id: String, completion: @escaping deleteReferralUserCompletation)
}

extension ReferralUserControllerAPI: deleteReferralUserProtocol{
    /**
     deleteReferralUser
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func deleteReferralUser(_id: String, completion: @escaping deleteReferralUserCompletation) {
        ReferralUserControllerAPI.deleteReferralUserWithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}


public typealias getReferralUserCompletation = ((_ data: ReferralUserDto?,_ error: Error?) -> Void)

public protocol getReferralUserProtocol {
    func getReferralUser(_id: String, completion: @escaping getReferralUserCompletation)
}

extension ReferralUserControllerAPI: getReferralUserProtocol{
    /**
     getReferralUser
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getReferralUser(_id: String, completion: @escaping getReferralUserCompletation) {
        ReferralUserControllerAPI.getReferralUserWithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}

    /**
     * enum for parameter sort
     */
    public enum Sort_getReferralUserList: String { 
        case asc = "ASC"
        case desc = "DESC"
    }


public typealias getReferralUserListCompletation = ((_ data: PageReferralUserDto?,_ error: Error?) -> Void)

public protocol getReferralUserListProtocol {
    func getReferralUserList(pageNumber: String?, pageSize: String?, sort: Sort_getReferralUserList?, sortKey: [String]?, completion: @escaping getReferralUserListCompletation)
}

extension ReferralUserControllerAPI: getReferralUserListProtocol{
    /**
     getReferralUserList
     
     - parameter pageNumber: (query) starts from 0 (optional)
     - parameter pageSize: (query) must be greater than 0 (optional)
     - parameter sort: (query) ASC&#x3D;ascending ,DESC&#x3D;descending (optional)
     - parameter sortKey: (query) sort will be based on these words (optional)
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getReferralUserList(pageNumber: String?, pageSize: String?, sort: Sort_getReferralUserList?, sortKey: [String]?, completion: @escaping getReferralUserListCompletation) {
        ReferralUserControllerAPI.getReferralUserListWithRequestBuilder(pageNumber: pageNumber, pageSize: pageSize, sort: sort, sortKey: sortKey).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias updateReferralUserCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol updateReferralUserProtocol {
    func updateReferralUser(_id: String, referralUserDto: ReferralUserDto, completion: @escaping updateReferralUserCompletation)
}

extension ReferralUserControllerAPI: updateReferralUserProtocol{
    /**
     Update
     
     - parameter _id: (path) id 
     - parameter referralUserDto: (body) referralUserDto 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func updateReferralUser(_id: String, referralUserDto: ReferralUserDto, completion: @escaping updateReferralUserCompletation) {
        ReferralUserControllerAPI.updateReferralUserWithRequestBuilder(_id: _id, referralUserDto: referralUserDto).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}




public class ReferralUserControllerAPI {

public init(){
    
}



    /**
     create
     - POST /api/v1/campaign/referral/subscriber
     -  create ReferralUser
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]

     - returns: RequestBuilder<ReferralUserDto> 
     */
    open class func createReferralUserWithRequestBuilder() -> RequestBuilder<ReferralUserDto> {
        let path = "/api/v1/campaign/referral/subscriber"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<ReferralUserDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     deleteReferralUser
     - DELETE /api/v1/campaign/referral/subscriber/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<Void> 
     */
    open class func deleteReferralUserWithRequestBuilder(_id: String) -> RequestBuilder<Void> {
        var path = "/api/v1/campaign/referral/subscriber/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "DELETE", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getReferralUser
     - GET /api/v1/campaign/referral/subscriber/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<ReferralUserDto> 
     */
    open class func getReferralUserWithRequestBuilder(_id: String) -> RequestBuilder<ReferralUserDto> {
        var path = "/api/v1/campaign/referral/subscriber/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<ReferralUserDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getReferralUserList
     - GET /api/v1/campaign/referral/subscriber
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter pageNumber: (query) starts from 0 (optional)
     - parameter pageSize: (query) must be greater than 0 (optional)
     - parameter sort: (query) ASC&#x3D;ascending ,DESC&#x3D;descending (optional)
     - parameter sortKey: (query) sort will be based on these words (optional)

     - returns: RequestBuilder<PageReferralUserDto> 
     */
    open class func getReferralUserListWithRequestBuilder(pageNumber: String? = nil, pageSize: String? = nil, sort: Sort_getReferralUserList? = nil, sortKey: [String]? = nil) -> RequestBuilder<PageReferralUserDto> {
        let path = "/api/v1/campaign/referral/subscriber"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        var url = URLComponents(string: URLString)
        url?.queryItems = APIHelper.mapValuesToQueryItems([
            "pageNumber": pageNumber, 
            "pageSize": pageSize, 
            "sort": sort?.rawValue, 
            "sortKey": sortKey
        ])

        let requestBuilder: RequestBuilder<PageReferralUserDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     Update
     - PUT /api/v1/campaign/referral/subscriber/{id}
     -  update ReferralUser
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 
     - parameter referralUserDto: (body) referralUserDto 

     - returns: RequestBuilder<Void> 
     */
    open class func updateReferralUserWithRequestBuilder(_id: String, referralUserDto: ReferralUserDto) -> RequestBuilder<Void> {
        var path = "/api/v1/campaign/referral/subscriber/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: referralUserDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "PUT", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

}
