//
// BankCardControllerAPI.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation
import Alamofire




public typealias createBankCardCompletation = ((_ data: BankCardDto?,_ error: Error?) -> Void)

public protocol createBankCardProtocol {
    func createBankCard(bankCardDto: BankCardDto, completion: @escaping createBankCardCompletation)
}

extension BankCardControllerAPI: createBankCardProtocol{
    /**
     create
     
     - parameter bankCardDto: (body) bankCardDto 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func createBankCard(bankCardDto: BankCardDto, completion: @escaping createBankCardCompletation) {
        BankCardControllerAPI.createBankCardWithRequestBuilder(bankCardDto: bankCardDto).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias deleteBankCardCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol deleteBankCardProtocol {
    func deleteBankCard(_id: String, completion: @escaping deleteBankCardCompletation)
}

extension BankCardControllerAPI: deleteBankCardProtocol{
    /**
     deleteBankCard
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func deleteBankCard(_id: String, completion: @escaping deleteBankCardCompletation) {
        BankCardControllerAPI.deleteBankCardWithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}


public typealias getAllBankCardsCompletation = ((_ data: [BankCardDto]?,_ error: Error?) -> Void)

public protocol getAllBankCardsProtocol {
    func getAllBankCards(completion: @escaping getAllBankCardsCompletation)
}

extension BankCardControllerAPI: getAllBankCardsProtocol{
    /**
     getAllBankCards
     
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getAllBankCards(completion: @escaping getAllBankCardsCompletation) {
        BankCardControllerAPI.getAllBankCardsWithRequestBuilder().execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias getBankCardCompletation = ((_ data: BankCardDto?,_ error: Error?) -> Void)

public protocol getBankCardProtocol {
    func getBankCard(_id: String, completion: @escaping getBankCardCompletation)
}

extension BankCardControllerAPI: getBankCardProtocol{
    /**
     getBankCard
     
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func getBankCard(_id: String, completion: @escaping getBankCardCompletation) {
        BankCardControllerAPI.getBankCardWithRequestBuilder(_id: _id).execute { (response, error) -> Void in
            completion(response?.body, error)
        }
    }
}


public typealias updateBankCardCompletation = ((_ data: Void?,_ error: Error?) -> Void)

public protocol updateBankCardProtocol {
    func updateBankCard(bankCardDto: BankCardDto, _id: String, completion: @escaping updateBankCardCompletation)
}

extension BankCardControllerAPI: updateBankCardProtocol{
    /**
     update
     
     - parameter bankCardDto: (body) bankCardDto 
     - parameter _id: (path) id 
     - parameter completion: completion handler to receive the data and the error objects
     */
    public func updateBankCard(bankCardDto: BankCardDto, _id: String, completion: @escaping updateBankCardCompletation) {
        BankCardControllerAPI.updateBankCardWithRequestBuilder(bankCardDto: bankCardDto, _id: _id).execute { (response, error) -> Void in
            if error == nil {
                completion((), error)
            } else {
                completion(nil, error)
            }
        }
    }
}




public class BankCardControllerAPI {

public init(){
    
}



    /**
     create
     - POST /api/v1/wallet/bank-card
     - don't fill id
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter bankCardDto: (body) bankCardDto 

     - returns: RequestBuilder<BankCardDto> 
     */
    open class func createBankCardWithRequestBuilder(bankCardDto: BankCardDto) -> RequestBuilder<BankCardDto> {
        let path = "/api/v1/wallet/bank-card"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: bankCardDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<BankCardDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "POST", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }



    /**
     deleteBankCard
     - DELETE /api/v1/wallet/bank-card/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<Void> 
     */
    open class func deleteBankCardWithRequestBuilder(_id: String) -> RequestBuilder<Void> {
        var path = "/api/v1/wallet/bank-card/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "DELETE", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getAllBankCards
     - GET /api/v1/wallet/bank-card
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]

     - returns: RequestBuilder<[BankCardDto]> 
     */
    open class func getAllBankCardsWithRequestBuilder() -> RequestBuilder<[BankCardDto]> {
        let path = "/api/v1/wallet/bank-card"
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<[BankCardDto]>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     getBankCard
     - GET /api/v1/wallet/bank-card/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     - examples: [{output=none}]
     
     - parameter _id: (path) id 

     - returns: RequestBuilder<BankCardDto> 
     */
    open class func getBankCardWithRequestBuilder(_id: String) -> RequestBuilder<BankCardDto> {
        var path = "/api/v1/wallet/bank-card/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters: [String:Any]? = nil
        
        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<BankCardDto>.Type = SwaggerClientAPI.requestBuilderFactory.getBuilder()

        return requestBuilder.init(method: "GET", URLString: (url?.string ?? URLString), parameters: parameters, isBody: false)
    }



    /**
     update
     - PUT /api/v1/wallet/bank-card/{id}
     - API Key:
       - type: apiKey Authorization 
       - name: JWT_TOKEN
     
     - parameter bankCardDto: (body) bankCardDto 
     - parameter _id: (path) id 

     - returns: RequestBuilder<Void> 
     */
    open class func updateBankCardWithRequestBuilder(bankCardDto: BankCardDto, _id: String) -> RequestBuilder<Void> {
        var path = "/api/v1/wallet/bank-card/{id}"
        let _idPreEscape = "\(_id)"
        let _idPostEscape = _idPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        path = path.replacingOccurrences(of: "{id}", with: _idPostEscape, options: .literal, range: nil)
        let URLString = SwaggerClientAPI.basePath + path
        let parameters = JSONEncodingHelper.encodingParameters(forEncodableObject: bankCardDto)

        let url = URLComponents(string: URLString)

        let requestBuilder: RequestBuilder<Void>.Type = SwaggerClientAPI.requestBuilderFactory.getNonDecodableBuilder()

        return requestBuilder.init(method: "PUT", URLString: (url?.string ?? URLString), parameters: parameters, isBody: true)
    }

}
