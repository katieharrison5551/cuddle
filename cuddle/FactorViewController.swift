//
//  FactorViewController.swift
//  cuddle
//
//  Created by Saeed on 10/20/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class FactorViewController: UIViewController {

    @IBOutlet private var lblInitialFee: UILabel!
    @IBOutlet private var lblCostHourFee: UILabel!
    @IBOutlet private var lblDuration: UILabel!
    @IBOutlet private var lblTotalPay: UILabel!
    @IBOutlet private var btnPayment: UIButton!

    var factorModel: InProgressRentModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        initView()
    }

    override func viewDidAppear(_ animated: Bool) {
        btnPayment.setGradientsBackground(.lightRed, .darkRed, 8)
    }

    fileprivate func initView() {
        lblInitialFee.text = "$\(factorModel.initialFee ?? 0)"
        lblCostHourFee.text = "$\(factorModel.costPerHour ?? 0)"
        lblDuration.text = factorModel.rentDurationMilli?.calculateDuration()
        lblTotalPay.text = "$\(factorModel.finalCost ?? 0)"
    }

    @IBAction private func onPaymentButtonClick(_ sender: Any) {
        self.navigationController?.pushViewController(AppStoryboard.BankCard.viewController(viewControllerClass: CreditCardListViewController.self), animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
