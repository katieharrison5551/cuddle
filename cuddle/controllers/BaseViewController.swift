//
//  BaseViewController.swift
//  cuddle
//
//  Created by Saeed on 11/7/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import Instructions

class BaseViewController: UIViewController {

    var introCount = 0
    var introText: [String]!
    var introView: [UIView]!
    var introController: CoachMarksController!
    var isDisplayOverlay: [Bool]!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    func showIntro() {
        introController = CoachMarksController()
        introController.dataSource = self
        introController.delegate = self
        let skipView = CoachMarkSkipDefaultView()
        skipView.setTitle("intro-skip".getString(), for: .normal)
        introController.skipView = skipView
        introController.overlay.areTouchEventsForwarded = false
        introController.overlay.isUserInteractionEnabled = true
        introController.overlay.backgroundColor = (UIColor(named: "darkestRed") ?? UIColor.darkGray).withAlphaComponent(0.3)
        introController.start(in: .viewController(self))
    }
}

extension BaseViewController: CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: (UIView & CoachMarkBodyView), arrowView: (UIView & CoachMarkArrowView)?) {
        let coachViews = introController.helper.makeDefaultCoachViews(
            withArrow: true,
            arrowOrientation: coachMark.arrowOrientation
        )

        coachViews.bodyView.nextLabel.text = "intro-next".getString()
        coachViews.bodyView.hintLabel.text = introText[index]

        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }

    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkAt index: Int) -> CoachMark {
        var mark = introController.helper.makeCoachMark(for: introView[index], cutoutPathMaker: { (frame: CGRect) -> UIBezierPath in
            return UIBezierPath(rect: frame)
        })
        mark.isDisplayedOverCutoutPath = isDisplayOverlay[index]
        return mark
    }

    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return introCount
    }

    func coachMarksController(_ coachMarksController: CoachMarksController,
                              willLoadCoachMarkAt index: Int) -> Bool {
//        if index == 0 {
//            return false
//        }

        return true
    }

    func shouldHandleOverlayTap(in coachMarksController: CoachMarksController,
                                at index: Int) -> Bool {
        return true
    }
}
