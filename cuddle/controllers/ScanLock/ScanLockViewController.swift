//  ScanLockViewController.swift
//  cuddle
//
//  Created by MohammadReza on 9/30/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader

class ScanLockViewController: BaseViewController, SSASideMenuDelegate {
    static let ScanLockIntroKey = "ScanLockView"

    var sideMenu : SSASideMenu!

    @IBOutlet weak var backButton: UIBarButtonItem! {
        didSet {
            backButton.title = String.fontIconString(name: "cancel")
            let backButtonAttributes = [NSAttributedString.Key.font: UIFont(name: "icomoon", size: 20) ?? UIFont(), NSAttributedString.Key.foregroundColor: UIColor.white]
            backButton.setTitleTextAttributes(backButtonAttributes, for: .normal)
            backButton.setTitleTextAttributes(backButtonAttributes, for: .selected)
        }
    }
    @IBOutlet private var lockIcon: UILabel! {
        didSet {
            lockIcon.text = String.fontIconString(name: "lock")
            lockIcon.textColor = .white
            lockIcon.font = UIFont(name: "icomoon", size: 40)
        }
    }
    @IBOutlet private var writeCodeView: UIView!
    @IBOutlet private var camerView: UIView!
    @IBOutlet private var scanButton: UIButton!
    @IBOutlet private var torchButton: UIButton!
    @IBOutlet private var getIDtextfield: UITextField!
    @IBOutlet private var manualCodeButton: UIButton!
    @IBOutlet private var inCameraTorchButton: UIButton!
    lazy var reader: QRCodeReader = QRCodeReader()
    
    @IBOutlet weak var qrCode: QRCodeReaderView! {
        didSet {
            qrCode.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = true
                $0.rectOfInterest         = CGRect(x: 0.2, y: 0.4, width: 0.6, height: 0.3)
            })
        }
    }
    
    fileprivate var cuddleLock: CuddleLock!
    fileprivate var rentViewModel: RentViewModel!
    fileprivate var tryLockSDK = 0
    fileprivate var strollerLockId: String!
    fileprivate var mallViewModel: MallViewModel!
    fileprivate var isBluetoothOn = false
    fileprivate var rentModel: InProgressRentModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }
    func initView() {
        hideKeyboardWhenTappedAround()
        writeCodeView.isHidden = true
        camerView.isHidden = false
        rentViewModel = RentViewModel()
        mallViewModel = MallViewModel()

        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let leftVC = mainSB.instantiateViewController(withIdentifier: "LeftMenuViewController")
        let rentalViewController = AppStoryboard.RentalState.viewController(viewControllerClass: RentalStateViewController.self)
        sideMenu = SSASideMenu(contentViewController: rentalViewController, leftMenuViewController: leftVC)
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = self

        initIntro()
    }

    fileprivate func initIntro() {
        if UserDefaults.standard.bool(forKey: ScanLockViewController.ScanLockIntroKey) {
            return
        }

        UserDefaults.standard.setValue(true, forKey: ScanLockViewController.ScanLockIntroKey)

        introText = []
//        introText.append("intro-scan-lock".getString())
        introText.append("intro-scan-manual".getString())

        introView = []
//        introView.append(self.view)
        introView.append(manualCodeButton)

        isDisplayOverlay = []
//        isDisplayOverlay.append(true)
        isDisplayOverlay.append(false)

        introCount = 1

        showIntro()
    }

    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.barStyle = .default
        reader.stopScanning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.barStyle = .black
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.barTintColor = UIColor(named: "darkestRed")
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "BalsamiqSans-Bold", size: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    override func viewDidAppear(_ animated: Bool) {
        startScan()
    }

    @objc func didTapOnCloseButton(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction private func cancelAction(_ sender: Any) {
        if let _ = navigationController?.popViewController(animated: true) {

        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction private func scanAction(_ sender: Any) {
        writeCodeView.isHidden = true
        camerView.isHidden = false
        startScan()
    }

    func startScan() {
        reader.didFindCode = { [self] result in
            self.reader.stopScanning()
            self.getIDtextfield.text = result.value
            self.barcodeReaded(barcode: result.value)
        }
        reader.startScanning()
    }

    @IBAction private func torchAction(_ sender: Any) {
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        if let device = captureDevice, device.hasTorch {
            do {
                try device.lockForConfiguration()
                if device.torchMode == AVCaptureDevice.TorchMode.on {
                    device.torchMode = AVCaptureDevice.TorchMode.off
                } else {
                    do {
                        try device.setTorchModeOn(level: 1.0)
                    } catch {
                        print(error)
                    }
                }
                device.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }

    @IBAction private func manualCodeButtonAction(_ sender: Any) {
        writeCodeView.isHidden = false
        camerView.isHidden = true
        reader.stopScanning()
    }

    @IBAction private func lockUnlockButtonAction(_ sender: Any) {
        if let barcode = getIDtextfield.text {
            barcodeReaded(barcode: barcode)
        }
    }

    fileprivate func barcodeReaded(barcode: String) {
        if barcode.isEmpty {
            self.showErrorDialog(message: "barcode-empty-message".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            return
        }
        Utility.showHudLoading()
        tryLockSDK = 0
        cuddleLock = nil
        cuddleLock = CuddleLock(commandDelegate: self)
        strollerLockId = barcode.lowercased()
        AnalyticManager.logEvent(name: "B\(strollerLockId ?? "")", params: ["barcode": strollerLockId as Any])
        getMallInfo()
    }

    fileprivate func getMallInfo() {
        mallViewModel.getMallDetailByStrollerCode(strollerCode: strollerLockId) { (info, error) in
            Utility.hideHudLoading()
            if error == nil, let mallAndStroller = info, let mall = mallAndStroller.mall, let stroller = mallAndStroller.stroller {
                if stroller.currentState == StrollerDto.CurrentState.unlocked {
                    self.showErrorDialog(title: "hint".getString(), message: "stroller-is-used".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil, okButtonCompletion: nil, cancelButtonCompletion: nil)
                    self.startScan()
                    return
                }
                self.showConfrimPriceAlert(mallInfo: mall)
                return
            }

            self.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                 okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString()) {
                Utility.showHudLoading()
                self.getMallInfo()
            } cancelButtonCompletion: {
                self.startScan()
            }
        }
    }

    fileprivate func showConfrimPriceAlert(mallInfo: GetMallDto) {
        let confirmPriceAlert = AppStoryboard.ScanLock.viewController(viewControllerClass: ConfirmPriceAlertViewController.self)
        confirmPriceAlert.providesPresentationContextTransitionStyle = true
        confirmPriceAlert.definesPresentationContext = true
        confirmPriceAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        confirmPriceAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        confirmPriceAlert.alertActionDelegate = self
        confirmPriceAlert.mallInfo = mallInfo
        self.present(confirmPriceAlert, animated: true, completion: nil)
    }

    fileprivate func searchStroller() {
        cuddleLock.useOldSDK = tryLockSDK == 1
        cuddleLock.lockId = strollerLockId
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.cuddleLock.findDevice(password: self.cuddleLock.useOldSDK ? CuddleLockUtility.defaultPasswordOld:CuddleLockUtility.defaultPassword)
        }
    }

    fileprivate func checkLockState() {
        self.cuddleLock.useOldSDK = self.tryLockSDK == 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.cuddleLock.queryLockState(password: self.cuddleLock.useOldSDK ? CuddleLockUtility.defaultPasswordOld:CuddleLockUtility.defaultPassword)
        }
    }

    fileprivate func rentStroller() {
        Utility.showHudLoading()

        rentViewModel.rentStroller(strollerId: strollerLockId) { [weak self] (data, error) in
            if data != nil {
                self?.rentModel = InProgressRentModel(balance: 0, costPerHour: data?.costPerHour, finalCost: 0, initialFee: data?.initialFee, rentDurationMilli: 0, rentId: "", status: .rented, strollerCode: self?.strollerLockId)
                self?.rentModel.costPerHour = data?.costPerHour
                self?.rentModel.initialFee = data?.initialFee
                self?.rentModel.rentDurationMilli = 0
                self?.rentModel.strollerCode = self?.strollerLockId
                self?.rentModel.finalCost = (self?.rentModel.costPerHour ?? 0) + (self?.rentModel.initialFee ?? 0)
                self?.unlockStroller()
                return
            }
            Utility.hideHudLoading()
            self?.startScan()
            AnalyticManager.logEvent(name: "FailedRent", params: ["barcode": self?.strollerLockId as Any, "ErrorMessage": error?.message ?? ""])
            if error?.code == ServerErrorCode.preconditionRequired.rawValue {
                self?.showErrorDialog(title: "hint".getString(), message: error?.message ?? "server-unreachable".getString(),
                                      okButtonTitle: "add-card".getString(), cancelButtonTitle: "cancel".getString(),
                                      okButtonCompletion: {
                                        self?.showAddCardView()
                                      }, cancelButtonCompletion: nil)
                return
            }
            self?.showErrorDialog(message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
        }
    }

    fileprivate func showRentTimer() {
        (sideMenu.contentViewController as! RentalStateViewController).rentModel = rentModel
        gotoRentalTime()
        AnalyticManager.logEvent(name: "SuccessRent", params: ["barcode": self.strollerLockId as Any])
    }

    fileprivate func showAddCardView() {
        let addCardVC = AppStoryboard.BankCard.viewController(viewControllerClass: ADDEditCreditCardViewController.self)
        addCardVC.parentVC = self
        self.navigationController?.pushViewController(addCardVC, animated: true)
    }

    fileprivate func unlockStroller() {
        cuddleLock.useOldSDK = tryLockSDK == 1
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.cuddleLock.unlock(password: self.cuddleLock.useOldSDK ? CuddleLockUtility.defaultPasswordOld:CuddleLockUtility.defaultPassword)
        }
    }

    fileprivate func gotoRentalTime() {
        cuddleLock = nil
        Utility.hideSuccessHudLoading()
        self.navigationController?.pushViewController(self.sideMenu ?? UIViewController(), animated: true)
    }
}

extension ScanLockViewController: QRCodeReaderViewControllerDelegate {
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        qrCode.isHidden = true
    }
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        qrCode.isHidden = true
    }
}

extension ScanLockViewController: LockResponseCommandProtocol {
    func isBluetoothOn(isOn: Bool) {
        AnalyticManager.logEvent(name: "Bluetooth\(isOn ? "On":"Off")", params: ["IsOn": isOn])
        if isOn {
            isBluetoothOn = true
        } else {
            isBluetoothOn = false
        }
    }

    func isLockNearby(isSuccess: Bool, error: String) {
        AnalyticManager.logEvent(name: "FoundStroller\(isSuccess)", params: ["barcode": strollerLockId as Any, "Founded": isSuccess])
        if isSuccess {
            checkLockState()
        } else {
            if tryLockSDK == 0 && isBluetoothOn {
                tryLockSDK = 1
                searchStroller()
                return
            }

            if isBluetoothOn {
                showErrorDialog(message: error, okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            } else {
                showErrorDialog(message: "bluetooth-is-off".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }
            Utility.hideHudLoading()
            startScan()
        }
    }

    func isUnlocked(isSuccess: Bool, error: String) {
        AnalyticManager.logEvent(name: "StrolUnLocked\(isSuccess)", params: ["barcode": self.strollerLockId as Any, "Unlocked": isSuccess])
        if isSuccess {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                self.cuddleLock.disconnect()
//                self.rentStroller()
                self.showRentTimer()
            })
        } else {
            if self.tryLockSDK == 0 && isBluetoothOn {
                self.tryLockSDK = 1
                self.unlockStroller()
                return
            }

            if isBluetoothOn {
                showErrorDialog(message: error, okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            } else {
                showErrorDialog(message: "bluetooth-is-off".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }
            Utility.hideHudLoading()
            self.startScan()
        }
    }

    func isInLockState(isLocked: Bool, error: String) {
        AnalyticManager.logEvent(name: "StrolLocked\(isLocked)", params: ["barcode": strollerLockId as Any, "IsLocked": isLocked])
        if isLocked {
//            self.unlockStroller()
            self.rentStroller()
        } else {
            if self.tryLockSDK == 0 && isBluetoothOn {
                self.tryLockSDK = 1
                self.checkLockState()
                return
            }

            if isBluetoothOn {
                self.showErrorDialog(title: "hint-client".getString(), message: "stroller-is-used".getString(),
                                     okButtonTitle: "ok".getString(), cancelButtonTitle: nil, okButtonCompletion: nil, cancelButtonCompletion: nil)
            } else {
                showErrorDialog(message: "bluetooth-is-off".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }

            Utility.hideHudLoading()
            self.startScan()
        }
    }

    func lockBatteryReport(isSuccess: Bool, battery: Int, error: String) {
        print(battery)
    }
}

extension ScanLockViewController: AlertActionDelegate {
    func onConfirmClick() {
        if isBluetoothOn {
            Utility.showHudLoading()
            searchStroller()
        } else {
            showErrorDialog(message: "bluetooth-is-off".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            startScan()
        }
    }

    func onCancelClick() {
        self.startScan()
    }
}
