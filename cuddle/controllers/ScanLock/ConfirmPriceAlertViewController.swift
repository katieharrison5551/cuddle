//
//  ConfirmPriceAlertViewController.swift
//  cuddle
//
//  Created by Saeed on 11/14/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

protocol AlertActionDelegate {
    func onConfirmClick()
    func onCancelClick()
}

class ConfirmPriceAlertViewController: UIViewController {

    @IBOutlet private var lblMall: UILabel!
    @IBOutlet private var lblInitialFee: UILabel!
    @IBOutlet private var lblCostPerHour: UILabel!
    @IBOutlet private var btnConfirm: UIButton!

    var alertActionDelegate: AlertActionDelegate!
    var mallInfo: GetMallDto!

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }

    override func viewDidAppear(_ animated: Bool) {
        btnConfirm.setGradientsBackground(.lightRed, .darkRed, 8)
    }

    fileprivate func initView() {
        if mallInfo == nil {
            alertActionDelegate.onCancelClick()
            return
        }
        lblMall.text = mallInfo.title?.fields[0]
        lblInitialFee.text = "$\(mallInfo.initialFee ?? 0)"
        lblCostPerHour.text = "$\(mallInfo.costPerHour ?? 0)"
    }

    @IBAction func onConfirmButtonClick(_ sender: Any) {
        alertActionDelegate.onConfirmClick()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onCancelButtonClick(_ sender: Any) {
        alertActionDelegate.onCancelClick()
        dismiss(animated: true, completion: nil)
    }
}
