//
//  LeftMenuViewController.swift
//  SSASideMenuExample
//
//  Created by Sebastian Andersen on 20/10/14.
//  Copyright (c) 2015 Sebastian Andersen. All rights reserved.
//

import Foundation
import UIKit

class MenuViewController: UIViewController {
    var ridesStackView: UIStackView!
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.frame = CGRect(x: 20, y: (self.view.frame.size.height - 54 * 5) / 2.0, width: self.view.frame.size.width/1.5, height: 54 * 5)
        tableView.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.isOpaque = false
        tableView.backgroundColor = .clear
        tableView.backgroundView = nil
        tableView.bounces = false
        return tableView
    }()
    let upperLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "border")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let lowerLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "border")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let ridesIcon: UIImageView = {
        let ride = UIImageView(image: UIImage(named: "ride"))
        ride.contentMode = .scaleAspectFit
        ride.translatesAutoresizingMaskIntoConstraints = false
        return ride
    }()
    lazy var ridesCount: UILabel = {
        let theLabel = UILabel()
        theLabel.text = "24"
        theLabel.textColor = .white
        theLabel.font = UIFont(name: CustomFont.Regular, size: 20)
        theLabel.translatesAutoresizingMaskIntoConstraints = false
        return theLabel
    }()
    lazy var ridesTitle: UILabel = {
        let rideLb = UILabel()
        rideLb.text = "Rides"
        rideLb.textColor = .cmiddleGray
        rideLb.font = UIFont(name: CustomFont.Regular, size: 12)
        rideLb.translatesAutoresizingMaskIntoConstraints = false
        return rideLb
    }()
    let referralIcon: UIImageView = {
        let theImageView = UIImageView(image: UIImage(named: "referral"))
        theImageView.contentMode = .scaleAspectFit
        theImageView.isUserInteractionEnabled = true
        theImageView.translatesAutoresizingMaskIntoConstraints = false
        return theImageView
    }()
    let referralTitle: UILabel = {
        let theLabel = UILabel()
        theLabel.text = "Referral"
        theLabel.textColor = .white
        theLabel.isUserInteractionEnabled = true
        theLabel.font = UIFont(name: CustomFont.Regular, size: 14)
        theLabel.translatesAutoresizingMaskIntoConstraints = false
        return theLabel
    }()
    lazy var nameTitle: UILabel = {
        let theLabel = UILabel()
        theLabel.textColor = .white
        theLabel.text = "Hi, "
        theLabel.font = UIFont(name: CustomFont.Regular, size: 14)
        theLabel.numberOfLines = 2
        theLabel.lineBreakMode = .byCharWrapping
        theLabel.translatesAutoresizingMaskIntoConstraints = false
        return theLabel
    }()
    let logoutIcon: UIImageView = {
        let theImageView = UIImageView(image: UIImage(named: "logout"))
        theImageView.contentMode = .scaleAspectFit
        theImageView.translatesAutoresizingMaskIntoConstraints = false
        return theImageView
    }()
    let logoutButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.titleLabel?.font = UIFont(name: CustomFont.Regular, size: 18)
        btn.setTitle("logout".getString(), for: .normal)
        btn.setTitle("logout".getString(), for: .selected)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = .clear
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(onLogOutButtonClick), for: .touchUpInside)
        return btn
    }()

    fileprivate var profileViewModel: ProfileViewModel!
    fileprivate var rideHistoryViewModel: RideHistoryViewModel!

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "darkestRed")
        view.addSubview(tableView)
        setupCustomLayout()
        initView()
    }
    func initView() {
        profileViewModel = ProfileViewModel()
        rideHistoryViewModel = RideHistoryViewModel()
        referralIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onReferralClick(_:))))
        referralTitle.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onReferralClick(_:))))
        loadUserName()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    fileprivate func loadUserName() {
        Utility.showHudLoading()
        profileViewModel.loadProfileUser { [weak self] (profile, error) in
            if error == nil {
                if let extraINfo = profile?.extraInfo {
                    if let name = extraINfo.fullName {
                        self?.nameTitle.text = "Hi, \(name)"
                    } else {
                        self?.nameTitle.text = "Hi, \(profile?.username ?? "")"
                    }
                } else {
                    self?.nameTitle.text = "Hi, \(profile?.username ?? "")"
                }
                self?.loadRideNumber()
                return
            }
            Utility.hideHudLoading()
            self?.nameTitle.text = ""
        }
    }

    fileprivate func loadRideNumber() {
        rideHistoryViewModel.getAllCuddleRideHistory(pageNumber: "0", pageSize: "10", sort: Sort_getAllCuddleTransactions.asc, sortKey: nil) { [weak self] (response, error) in
            Utility.hideHudLoading()
            if error == nil {
                self?.ridesCount.text = "\(response?.totalElements ?? 0)"
                return
            }

            self?.ridesCount.text = "0"
        }
    }

    @objc fileprivate func onReferralClick(_ sender: UITapGestureRecognizer) {
        let viewController = AppStoryboard.ReferalCode.initialViewController()
        present(viewController ?? UIViewController(), animated: true, completion: nil)
    }
    //swiftlint:disable all
    private func setupCustomLayout() {
        view.addSubview(upperLine)
        upperLine.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -16).isActive = true
        upperLine.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24).isActive = true
        upperLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        upperLine.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        view.addSubview(lowerLine)
        lowerLine.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 16).isActive = true
        lowerLine.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24).isActive = true
        lowerLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        lowerLine.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        ridesStackView = UIStackView(arrangedSubviews: [ridesIcon, ridesCount, ridesTitle])
        ridesStackView.axis = .vertical
        ridesStackView.alignment = .center
        ridesStackView.distribution = .fillEqually
        ridesStackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(ridesStackView)
        ridesStackView.bottomAnchor.constraint(equalTo: upperLine.topAnchor, constant: -12).isActive = true
        ridesStackView.leadingAnchor.constraint(equalTo: upperLine.leadingAnchor).isActive = true
        ridesStackView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        ridesStackView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        view.addSubview(referralIcon)
        referralIcon.topAnchor.constraint(equalTo: ridesStackView.topAnchor).isActive = true
        referralIcon.leadingAnchor.constraint(equalTo: ridesStackView.trailingAnchor, constant: 32).isActive = true
        referralIcon.heightAnchor.constraint(equalTo: ridesStackView.heightAnchor, multiplier: 0.5).isActive = true
        referralIcon.widthAnchor.constraint(equalTo: ridesStackView.heightAnchor, multiplier: 0.5).isActive = true
        view.addSubview(referralTitle)
        referralTitle.leadingAnchor.constraint(equalTo: referralIcon.leadingAnchor, constant: 0).isActive = true
        referralTitle.bottomAnchor.constraint(equalTo: ridesStackView.bottomAnchor, constant: -8).isActive = true
        view.addSubview(nameTitle)
        nameTitle.leadingAnchor.constraint(equalTo: ridesStackView.leadingAnchor, constant: 0).isActive = true
        nameTitle.bottomAnchor.constraint(equalTo: ridesStackView.topAnchor, constant: -16).isActive = true
        nameTitle.widthAnchor.constraint(equalToConstant: 200).isActive = true
        view.addSubview(logoutIcon)
        logoutIcon.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 16).isActive = true
        logoutIcon.topAnchor.constraint(equalTo: lowerLine.bottomAnchor, constant: 32).isActive = true
        logoutIcon.heightAnchor.constraint(equalToConstant: 18).isActive = true
        logoutIcon.widthAnchor.constraint(equalToConstant: 18).isActive = true
        view.addSubview(logoutButton)
        logoutButton.leadingAnchor.constraint(equalTo: logoutIcon.trailingAnchor, constant: 16).isActive = true
        logoutButton.centerYAnchor.constraint(equalTo: logoutIcon.centerYAnchor).isActive = true
        logoutButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        logoutButton.widthAnchor.constraint(equalToConstant: 70).isActive = true
    }

    @IBAction private func onLogOutButtonClick(sender: Any) {
        let logOutDialog = AppStoryboard.CustomDialog.viewController(viewControllerClass: LogOutDialogViewController.self)
        logOutDialog.logoutDelegate = self
        logOutDialog.providesPresentationContextTransitionStyle = true
        logOutDialog.definesPresentationContext = true
        logOutDialog.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        logOutDialog.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(logOutDialog, animated: true, completion: nil)
    }

    fileprivate func logOut() {
        UserInfoModel.shared.logoutUser()
        self.navigationController?.popToRootViewController(animated: false)
    }
}

extension MenuViewController: LogoutActionDelegate {
    func onYesClick() {
        logOut()
    }

    func onNoClick() {

    }
}

// MARK: TableViewDataSource & Delegate Methods
extension MenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MenuCell {
        let titles: [String] = ["Profile", "History", "Wallet", "Support", "Notifications"]
        let images: [String] = ["profile", "history", "wallet", "support", "notification"]
        cell.backgroundColor = UIColor.clear
        cell.menuIcon.image = UIImage(named: images[indexPath.row])
        cell.selectionStyle = .none
        cell.menuTitle.text = titles[indexPath.row]
        if indexPath.row == 4 {
            cell.menuSwitch.isHidden = false
        } else {
            cell.menuSwitch.isHidden = true
        }
        return cell
        }
        return UITableViewCell()
    }
    @objc fileprivate func switchChanged(sender: UISwitch!) {
          print("table row switch Changed \(sender.tag)")
          print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }
    func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(newSize)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        if let newImage = UIGraphicsGetImageFromCurrentImageContext() {
        UIGraphicsEndImageContext()
        return newImage.withRenderingMode(.alwaysOriginal)
        }
        return UIImage()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            let viewController = AppStoryboard.Profile.initialViewController()
            self.navigationController?.pushViewController(viewController!, animated: true)
        case 1:
            self.navigationController?.pushViewController(AppStoryboard.History.initialViewController() ?? UIViewController(), animated: true)
            sideMenuViewController?.hideMenuViewController()
        case 2:
            let controller = AppStoryboard.BankCard.viewController(viewControllerClass: CreditCardListViewController.self)
            controller.isFromMenu = true
            self.navigationController?.pushViewController(controller, animated: true)
            sideMenuViewController?.hideMenuViewController()
        case 3:
            let viewController = AppStoryboard.FAQ.viewController(viewControllerClass: FAQ_ViewController.self)
            self.navigationController?.pushViewController(viewController, animated: true)
            //self.performSegue(withIdentifier: "showSupport", sender: nil)
        case 4:
            break
        default:
            break
        }

        sideMenuViewController?.hideMenuViewController()

        if indexPath.row == 3 {
            //let supportVC = AppStoryboard.Support.initialViewController()
            //supportVC?.modalPresentationStyle = .fullScreen
            //self.navigationController?.pushViewController(supportVC!, animated: true)
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Support", bundle:nil)
//            let nextViewController = storyBoard.instantiateInitialViewController()
//            self.navigationController?.pushViewController(nextViewController!, animated: true)
//            self.performSegue(withIdentifier: "showSupport", sender: nil)
        }
    }
}
