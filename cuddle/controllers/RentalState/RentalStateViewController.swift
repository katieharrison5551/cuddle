import UIKit
import ConcentricProgressRingView

class RentalStateViewController: UIViewController {
    // MARK: - views
    @IBOutlet private var initialFeeAmount: UILabel!
    @IBOutlet private var costHoursAmount: UILabel!
    @IBOutlet private var totalAmount: UILabel!
    @IBOutlet private var endRentalAction: UILabel!
    @IBOutlet private var middleView: UIView!
    @IBOutlet private var menuBtn: MenuButton!
    @IBOutlet private var endRentalButton: UIView!
    @IBOutlet private var endRentalIcon: UILabel! {
        didSet {
            endRentalIcon.text = String.fontIconString(name: "cancel")
        }
    }
    @IBOutlet private var circularView: CircularProgressView!
    @IBOutlet private var timeLabel: UILabel!
    @IBOutlet private var roundAmountLabel: UILabel!
    @IBOutlet private var lblDate: UILabel!
    @IBOutlet private var lblWeekDay: UILabel!
    @IBOutlet private var vwDate: UIView!
    // MARK: - init var
    var timeCounter: Int64 = 0
    var serverTime: Int64 = 200
    var timer = Timer()
    var roundCounter: Int64 = 1
    let hourSeconds = 3600
    let oneMinute: Int64 = 60
    var costHours: Int64 = 2
    var initialFee: Int64 = 5
    var total: Int64 = 0
    var progressRingIsAnimating = false
    var rentModel: InProgressRentModel!
    var duration: TimeInterval!
    fileprivate var rentViewModel: RentViewModel!
    fileprivate var cuddleLock: CuddleLock!
    fileprivate var tryLockSDK = 0
    fileprivate var endRentPress = false
    fileprivate var isBluetoothOn = false
    // MARK: - methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initTestAction()
        duration = 60
    }
    func initView() {
        setRentInfo()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.update)), userInfo: nil, repeats: true)
        endRentalButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endRentalActionButton(_:))))
        menuBtn.setOnClick(onClick: #selector(SSASideMenu.presentLeftMenuViewController))
        NotificationCenter.default.addObserver(self, selector: #selector(updateWhenReopenApp), name: UIApplication.willEnterForegroundNotification, object: nil)
        rentViewModel = RentViewModel()
        NotificationCenter.default.addObserver(self, selector: #selector(refreshRentStatus), name:
                UIApplication.willEnterForegroundNotification, object: nil)
    }

    fileprivate func setRentInfo() {
        if rentModel != nil {
            costHours = rentModel.costPerHour ?? 0
            initialFee = rentModel.initialFee ?? 0
            serverTime = (rentModel.rentDurationMilli ?? 0) / 1000
            timeLabel.text = String(format: "%02d:%02d", serverTime / oneMinute, serverTime % oneMinute)
            if serverTime / (oneMinute * 60) > 0 {
                timeCounter = serverTime % (oneMinute * 60)
            }
            roundCounter = (serverTime / (oneMinute * 60)) + 1
            circularView.countinueAnimating(duration: TimeInterval(hourSeconds), showTime: TimeInterval(serverTime))
            roundAmountLabel.text = "\(roundCounter)"
            total = rentModel.finalCost ?? 0
        }

        totalAmount.text = String(total)
        costHoursAmount.text = "$\(costHours)"
        initialFeeAmount.text = "$\(initialFee)"
        lblDate.text = Date().getString()
        lblWeekDay.text = Date().getWeekDay()
    }

    fileprivate func initTestAction() {
        vwDate.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(queryBatteryLock(_:))))
    }

    @objc func endRentalActionButton(_ sender: UITapGestureRecognizer) {
        self.showActionSheet(title: NSLocalizedString("end-rent-question", comment: ""),
                             message: "", style: .actionSheet,
                             actions: [self.actionMessage(NSLocalizedString("end-rent", comment: ""), style: .default, {
                                Utility.showHudLoading()
                                self.tryLockSDK = 0
                                self.endRentPress = true
                                self.searchStroller()
                             }), self.actionMessageCancel()])
    }

    @objc func queryBatteryLock(_ sender: UITapGestureRecognizer) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.cuddleLock.query‌BatteryState(password: self.cuddleLock.useOldSDK ? CuddleLockUtility.defaultPasswordOld:CuddleLockUtility.defaultPassword)
        }
    }

    @objc func refreshRentStatus() {
        getUserRentalStatus()
    }

    override func viewDidAppear(_ animated: Bool) {
        endRentalButton.setGradientsBackground(.lightRed, .darkRed, 16)
    }

    @objc func update() {
        if timeCounter >= 0 && timeCounter != hourSeconds {
            timeCounter += 1
            let minutesCalc = timeCounter / oneMinute
            let secondCalc = timeCounter % oneMinute
            timeLabel.text = String(format: "%02d:%02d", minutesCalc, secondCalc)
        } else {
            timeCounter = 1
            roundCounter += 1
            let minutesCalc = timeCounter / oneMinute
            let secondCalc = timeCounter % oneMinute
            timeLabel.text = String(format: "%02d:%02d", minutesCalc, secondCalc)
            roundAmountLabel.text = String(roundCounter)
            total = initialFee + (costHours * roundCounter)
            totalAmount.text = String(total)
            circularView.resetFromZero(duration: duration)
        }
    }

    @objc func updateWhenReopenApp() {
        circularView.countinueAnimating(duration: TimeInterval(hourSeconds), showTime: TimeInterval(serverTime))
    }

    override func viewWillAppear(_ animated: Bool) {
        searchStroller()
    }

    fileprivate func getUserRentalStatus() {
        rentViewModel = RentViewModel()
        rentViewModel.getActiveRentList { (data, _) in
            guard let response = data, !response.isEmpty else {
                return
            }

            if !response.isEmpty {
                switch response[0].status {
                case .rented:
                    self.rentModel = response[0]
                    self.setRentInfo()
                default:
                    break
                }
            }
        }
    }

    fileprivate func searchStroller() {
        cuddleLock = nil
        cuddleLock = CuddleLock(commandDelegate: self)
        cuddleLock.useOldSDK = tryLockSDK == 1
        cuddleLock.lockId = self.rentModel.strollerCode
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.cuddleLock.findDevice(password: self.cuddleLock.useOldSDK ? CuddleLockUtility.defaultPasswordOld:CuddleLockUtility.defaultPassword)
        }
    }

    fileprivate func checkLockState() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.cuddleLock.useOldSDK = self.tryLockSDK == 1
            self.cuddleLock.queryLockState(password: self.cuddleLock.useOldSDK ? CuddleLockUtility.defaultPasswordOld:CuddleLockUtility.defaultPassword)
        }
    }

    fileprivate func endRent() {
        Utility.showHudLoading()
        rentViewModel.endRentStroller(strollerIdKey: self.rentModel.strollerCode) { (response, error) in
            if error == nil {
                if let factor = response {
                    self.showPayment(factor: factor)
                    Utility.hideSuccessHudLoading()
                    AnalyticManager.logEvent(name: "SuccessEndRent", params: ["barcode": self.rentModel.strollerCode as Any, "RentId": factor.rentId as Any])
                    return
                }
                AnalyticManager.logEvent(name: "FailedEndRent", params: ["barcode": self.rentModel.strollerCode as Any])
                self.showErrorDialog(title: "error".getString(), message: "somthing-wrong".getString(),
                                     okButtonTitle: "retry".getString(), cancelButtonTitle: nil, okButtonCompletion: {
                                        self.endRent()
                                     }, cancelButtonCompletion: nil)
                return
            }
            AnalyticManager.logEvent(name: "FailedEndRent2", params: ["barcode": self.rentModel.strollerCode as Any])
            Utility.hideHudLoading()

            self.showErrorDialog(title: "error".getString(), message: "server-unreachable".getString(),
                                 okButtonTitle: "retry".getString(), cancelButtonTitle: nil, okButtonCompletion: {
                                    self.endRent()
                                 }, cancelButtonCompletion: nil)
        }
    }

    fileprivate func showPayment(factor: InProgressRentModel) {
        let payment = AppStoryboard.Factor.viewController(viewControllerClass: FactorViewController.self)
        payment.factorModel = factor
        FactorModel.modelInfo = factor
        self.navigationController?.pushViewController(payment, animated: true)
    }
}

extension RentalStateViewController: LockResponseCommandProtocol {
    func isBluetoothOn(isOn: Bool) {
        AnalyticManager.logEvent(name: "Bluetooth\(isOn ? "On":"Off")", params: ["IsOn": isOn])
        if isOn {
            print("bluetooth on")
            if !isBluetoothOn {
                searchStroller()
            }
            isBluetoothOn = true
        } else {
            print("bluetooth off")
            isBluetoothOn = false
        }
    }

    func isLockNearby(isSuccess: Bool, error: String) {
        AnalyticManager.logEvent(name: "FoundStroller\(isSuccess)", params: ["barcode": self.rentModel.strollerCode as Any, "Founded": isSuccess])
        if isSuccess {
            checkLockState()
        } else {
            if tryLockSDK == 0 {
                tryLockSDK = 1
                searchStroller()
                return
            }

            if isBluetoothOn {
                if endRentPress {
                    showErrorDialog(message: error, okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
                    Utility.hideHudLoading()
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        self.searchStroller()
                    }
                }
            } else {
                showErrorDialog(message: "bluetooth-is-off".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }
        }
    }

    func isUnlocked(isSuccess: Bool, error: String) {

    }

    func isInLockState(isLocked: Bool, error: String) {
        AnalyticManager.logEvent(name: "StrolLocked\(isLocked)", params: ["barcode": self.rentModel.strollerCode as Any, "IsLocked": isLocked])
        if isLocked {
            cuddleLock.disconnect()
            endRent()
        } else {
//            if self.tryLockSDK == 0 {
//                self.tryLockSDK = 1
//                self.checkLockState()
//                return
//            }

            if endRentPress {
                self.showErrorDialog(title: "hint-client".getString(), message: "lock-stroller".getString(),
                                     okButtonTitle: "ok".getString(), cancelButtonTitle: nil, okButtonCompletion: nil, cancelButtonCompletion: nil)
                endRentPress = false
                Utility.hideHudLoading()
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.checkLockState()
            }
        }
    }

    func lockBatteryReport(isSuccess: Bool, battery: Int, error: String) {
        AnalyticManager.logEvent(name: "StrolBattery\(battery)", params: ["barcode": self.rentModel.strollerCode as Any, "IsLocked": isSuccess, "Battery": battery])
        print(battery)
    }
}
