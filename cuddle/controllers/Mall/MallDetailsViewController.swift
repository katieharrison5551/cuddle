//
//  MallDetailsViewController.swift
//  cuddle
//
//  Created by MohammadReza on 9/2/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMaps

class MallDetailsViewController: BaseViewController, SSASideMenuDelegate {
    static let MallIntroKey = "MallDetailView"

    @IBOutlet private var bottomSheetView: UIView!
    @IBOutlet private var pageIndicator: UIView!
    @IBOutlet private var placeName: UILabel!
    @IBOutlet private var mCollectionView: UICollectionView!
    @IBOutlet private var pageControl: UIPageControl!
    @IBOutlet private var touchPlaceIndicator: UIView!
    @IBOutlet private var placeAddress: UILabel!
    @IBOutlet private var placeWorkTime: UILabel!
    @IBOutlet private var initialFeeAmount: UILabel!
    @IBOutlet private var coustHoursAmount: UILabel!
    @IBOutlet private var directionButton: CustomUIButton!
    @IBOutlet private var stationsButton: CustomUIButton!
    @IBOutlet private var scanButton: CustomUIButton!

    var mallId: String! = "5f7af67cf2a0ae228fb7caf7"
    var currentLocation: CLLocationCoordinate2D?
    var destinationLocaton: CLLocationCoordinate2D?
    var mallCount: Int?

    fileprivate var mallViewModel: MallViewModel!
    fileprivate var banners: [String]!
    var bottomSheetY: CGFloat?
    override func viewDidLoad() {
        super.viewDidLoad()
        initNotificationSwipe()
        mCollectionView.isPagingEnabled = true
        initView()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scanButton.setGradientsBackground(.lightRed, .darkRed, 16)
        getMallInfo()
    }
    fileprivate func initNotificationSwipe() {
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        downSwipe.direction = .down
        touchPlaceIndicator.addGestureRecognizer(downSwipe)
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomSheetY = self.bottomSheetView.frame.origin.y
            self.bottomSheetView.frame.origin.y = self.view.frame.height
        }, completion: { _ in
            self.dismiss(animated: true, completion: nil)
        })
    }

    fileprivate func initView() {
        mallViewModel = MallViewModel()
        navigationController?.setNavigationBarHidden(true, animated: false)
        // pageControl.hidesForSinglePage = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
        initIntro()
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    fileprivate func initIntro() {
        if UserDefaults.standard.bool(forKey: MallDetailsViewController.MallIntroKey) {
            return
        }

        UserDefaults.standard.setValue(true, forKey: MallDetailsViewController.MallIntroKey)

        introText = []
        //        introText.append("intro-mall-info".getString())
        introText.append("intro-direction".getString())
        introText.append("intro-scan".getString())

        introView = []
        //        introView.append(self.view)
        introView.append(directionButton)
        introView.append(scanButton)

        isDisplayOverlay = []
        //        isDisplayOverlay.append(true)
        isDisplayOverlay.append(false)
        isDisplayOverlay.append(false)

        introCount = 2

        showIntro()
    }

    fileprivate func getMallInfo() {
        Utility.showHudLoading(title: "", message: "")
        mallViewModel.getMallDetail(mallId: mallId) { [weak self] (mallInfo, error) in
            Utility.hideHudLoading()

            guard let mall = mallInfo else {
                self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                      okButtonTitle: "retry".getString(), cancelButtonTitle: "close".getString(),
                                      okButtonCompletion: {
                                        Utility.showHudLoading()
                                        self?.getMallInfo()
                                      }, cancelButtonCompletion: {
                                        self?.respondToSwipeGesture(gesture: UITapGestureRecognizer.init())
                                      })
                return
            }

            self?.showMallInfo(mallInfo: mall)
        }
    }

    fileprivate func showMallInfo(mallInfo: GetMallDto) {
        placeName.text = mallInfo.title?.fields[0]
        placeAddress.text = mallInfo.metaData?["address"]
        initialFeeAmount.text = "".showPrice(price: mallInfo.initialFee ?? 0)
        coustHoursAmount.text = "".showPrice(price: mallInfo.costPerHour ?? 0)
        placeWorkTime.text = "Openning Time: \(mallInfo.openingTime ?? "-") - Closing Time: \(mallInfo.closingTime ?? "-")"
        banners = mallInfo.banner?.fields
        mCollectionView.reloadData()
    }

    @IBAction fileprivate func directionButtonAction(_ sender: Any) {
        //code
        let viewController = AppStoryboard.Direction.viewController(viewControllerClass: DirectionViewController.self)
        viewController.currentLocation = self.currentLocation
        viewController.destinationLocaton = self.destinationLocaton
        viewController.mallCount = self.mallCount
        viewController.contentID = self.mallId

        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction fileprivate func stationsButtonAction(_ sender: Any) {
        let viewController = AppStoryboard.StationList.viewController(viewControllerClass: StationListViewController.self)
        viewController.contentID = self.mallId
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction fileprivate func scanButtonAction(_ sender: Any) {
        self.navigationController?.pushViewController(AppStoryboard.ScanLock.viewController(viewControllerClass: ScanLockViewController.self), animated: true)
    }
}
extension MallDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if banners == nil {
            pageControl.numberOfPages = 0
            return 0
        }

        pageControl.numberOfPages = banners.count
        return banners.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionView", for: indexPath) as? MallPicCollectionViewCell {
            cell.mallPicture.sd_setImage(with: URL(string: banners[indexPath.section]), placeholderImage: UIImage(named: "TopHeader"))
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.section
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width-24, height: 128)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
extension MallDetailsViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: bottomSheetView) == true {
            return false
        }
        return true
    }
}
