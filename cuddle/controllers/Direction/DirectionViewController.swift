//
//  DirectionViewController.swift
//  cuddle
//
//  Created by MohammadReza on 10/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

struct Distance: Decodable {
    var text: String
    var value: Int64
}

struct Duration: Decodable {
    var text: String
    var value: Int
}

struct RouteDetail: Decodable {
    var distance: Distance
    var duration: Duration
}

struct Legs: Decodable {
    var legs: [RouteDetail]
}

struct Routes: Decodable {
    var routes: [Legs]
}

class DirectionViewController: UIViewController {

    @IBOutlet private var mapView: GMSMapView!
    @IBOutlet private var backBtn: UIButton! {
        didSet {
            backBtn.setTitle(String.fontIconString(name: "back"), for: .normal)
            backBtn.setTitleColor(UIColor(named: "darkestRed"), for: .normal)
            backBtn.setTitleColor(UIColor(named: "darkestRed"), for: .selected)
        }
    }

    @IBOutlet weak var bottomSheetView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: CustomUILabel!
    @IBOutlet weak var scanButton: CustomUIButton!


    //init var
    var currentLocation: CLLocationCoordinate2D?
    var destinationLocaton: CLLocationCoordinate2D?
    var test = CLLocationCoordinate2D(latitude: 35.689252, longitude: 51.3896)
    let directionApiKey = "AIzaSyC9EY26qa3s9eDV2fiUvmILtL7pdoWoEio"
    var zoomLevel: Float = 15.0
    var locationManagerViewModel: LocationManagerViewModel!
    var mallCount: Int?
    var contentID: String = ""
    let jsonDecoder = JSONDecoder.init()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()        
        fetchRoute(from: currentLocation!, to: destinationLocaton!)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scanButton.setGradientsBackground(.lightRed, .darkRed, 12)
    }

    func initView() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        locationManagerViewModel = LocationManagerViewModel(updateMapDelegate: self)
        mapView.isMyLocationEnabled = true
        let marker = CustomMarker(labelText: "\(mallCount ?? 0)")
        marker.position = CLLocationCoordinate2D(latitude: destinationLocaton?.latitude ?? 0.0, longitude: destinationLocaton?.longitude ?? 0.0)
        marker.map = mapView
    }
    @IBAction private func backbtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func fetchRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) {
        Utility.showHudLoading()
        let session = URLSession.shared

        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&key=\(directionApiKey)")!
        print("url : \(url)")
        let task = session.dataTask(with: url, completionHandler: { [self]
            (data, response, error) in

            guard error == nil else {
                showFailedFindRoute()
                print(error!.localizedDescription)
                return
            }

            var distance = ""
            var duration = ""
            var distanceValue: Int64 = 0

            do {
                if let mData = data {
                    let routeData = try jsonDecoder.decode(Routes.self, from: mData)
                    if !routeData.routes.isEmpty {
                        distance = routeData.routes[0].legs[0].distance.text
                        distanceValue = routeData.routes[0].legs[0].distance.value
                        duration = routeData.routes[0].legs[0].duration.text
                    }
                }
            } catch {
                showFailedFindRoute()
                print("error")
            }

            guard let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                showFailedFindRoute()
                print("error in JSONSerialization")
                return
            }

            guard let routes = jsonResponse["routes"] as? [Any] else {
                showFailedFindRoute()
                return
            }

            if routes.isEmpty {
                showFailedFindRoute()
                return
            }

            guard let route = routes[0] as? [String: Any] else {
                showFailedFindRoute()
                return
            }

            guard let overview_polyline = route["overview_polyline"] as? [String: Any] else {
                showFailedFindRoute()
                return
            }

            guard let polyLineString = overview_polyline["points"] as? String else {
                showFailedFindRoute()
                return
            }

            //Call this method to draw path on map
            self.drawPath(from: polyLineString, duration: duration, distance: distance, distanceValue: distanceValue)
        })
        task.resume()
    }

    fileprivate func showFailedFindRoute() {
        DispatchQueue.main.async {
            Utility.hideHudLoading()
            self.showErrorDialog(title: "error".getString(), message: "failed-find-route".getString(),
                                 okButtonTitle: "ok".getString(), cancelButtonTitle: nil, okButtonCompletion: {
                                    self.navigationController?.popToRootViewController(animated: true)
                                 }, cancelButtonCompletion: nil)
        }
    }

    func drawPath(from polyStr: String,duration: String, distance: String, distanceValue: Int64) {
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = mapView // Google MapView

        DispatchQueue.main.async {
            Utility.hideHudLoading()
            let source = CLLocationCoordinate2D(latitude: self.currentLocation!.latitude, longitude: self.currentLocation!.longitude)
            let destination = CLLocationCoordinate2D(latitude: self.destinationLocaton!.latitude, longitude: self.destinationLocaton!.longitude)
            let bounds = GMSCoordinateBounds(coordinate: source, coordinate: destination)
            let update = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
            self.mapView.moveCamera(update)
            self.showJourneyDetails(duration: duration, distance: distance)
        }
    }

    func showJourneyDetails(duration: String, distance: String) {
        durationLabel.text = duration
        distanceLabel.text = distance
    }

    @IBAction private func stationsButtonAction(_ sender: Any) {
        let viewController = AppStoryboard.StationList.viewController(viewControllerClass: StationListViewController.self)
        viewController.contentID = self.contentID
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction private func scanButtonActions(_ sender: Any) {
        self.navigationController?.pushViewController(AppStoryboard.ScanLock.viewController(viewControllerClass: ScanLockViewController.self), animated: true)
    }
}

extension DirectionViewController: UpdateMapLocation {
    func updateMap(location: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView.animate(to: camera)
    }
}
