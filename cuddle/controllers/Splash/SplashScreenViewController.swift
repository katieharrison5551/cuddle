//
//  SplashScreenViewController.swift
//  cuddle
//
//  Created by Saeed on 8/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController, SSASideMenuDelegate {
    var rentViewModel: RentViewModel!
    var sideMenu: SSASideMenu!
    var rentalViewController: RentalStateViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareSideMenu()
    }
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.init().bool(forKey: "isFirstLaunch") {
            UserDefaults.init().set(false, forKey: "isFirstLaunch")
            self.navigationController?.pushViewController(AppStoryboard.OnBoarding.initialViewController() ??  UIViewController(), animated: true)
        } else {
            checkLogin()
        }
    }
    fileprivate func checkLogin() {
        if UserInfoModel.shared.getUserToken() == nil {
            self.navigationController?.pushViewController(AppStoryboard.SigninSignupStoryboard.initialViewController() ??  UIViewController(), animated: true)
        } else {
            getUserRentalStatus { (hasRental, rentalStatusModel) in
                if hasRental {
                    switch rentalStatusModel?.status {
                    case .rented:
                        self.rentalViewController.rentModel = rentalStatusModel
                        self.navigationController?.pushViewController(self.sideMenu, animated: true)
                    case .free:
                        self.goToFactor(progressModel: rentalStatusModel!)
                    default:
                        self.performSegue(withIdentifier: "ShowMain", sender: self)
                    }
                } else {
                    self.performSegue(withIdentifier: "ShowMain", sender: self)
                }
            }
        }
    }

    fileprivate func goToFactor(progressModel: InProgressRentModel) {
        let factorVC = AppStoryboard.Factor.viewController(viewControllerClass: FactorViewController.self)
        FactorModel.modelInfo = progressModel
        factorVC.factorModel = progressModel
        self.navigationController?.pushViewController(factorVC, animated: true)
    }

    fileprivate func getUserRentalStatus(completion: @escaping (Bool, InProgressRentModel?) -> Void) {
        rentViewModel = RentViewModel()
        rentViewModel.getActiveRentList { (data, _) in
            guard let response = data, !response.isEmpty else {
                completion(false, nil)
                return
            }
            completion(true, response[0])
        }
    }
    fileprivate func prepareSideMenu(){
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let leftVC = mainSB.instantiateViewController(withIdentifier: "LeftMenuViewController")
        rentalViewController = AppStoryboard.RentalState.viewController(viewControllerClass: RentalStateViewController.self)
        sideMenu = SSASideMenu(contentViewController: rentalViewController, leftMenuViewController: leftVC)
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = self
    }
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
}
