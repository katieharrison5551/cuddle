import UIKit

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate {
    var userProfileInfo: SubscriberDto?
    var getProfileViewModel: ProfileViewModel!
    var updateViewModel: UpdateProfileViewModel!
    var isMaleIcon = true
    var imagePicker = UIImagePickerController()
    var imageUrl: URL!
    var imageURLString: String?
    @IBOutlet private var profileImageView: UIImageView!
    @IBOutlet private var cameraImageView: UIImageView!
    @IBOutlet private var backgroundGender: UIView!
    @IBOutlet private var genderIcon: UIImageView!
    @IBOutlet private var backButton: UILabel! {
        didSet {
            backButton.text = String.fontIconString(name: "back")
        }
    }
    @IBOutlet weak var backView: UIView!
    @IBOutlet private var genderLabel: CustomUILabel!
    @IBOutlet private var phoneTextField: FloatingCustomTxtFld! {
        didSet {
            phoneTextField.delegate = self
        }
    }
    @IBOutlet private var nameTextField: FloatingCustomTxtFld!
    @IBOutlet private var emailTextField: FloatingCustomTxtFld!
    @IBOutlet private var saveButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        initView()
        loadUserProfile()
    }
    func initView() {
        getProfileViewModel = ProfileViewModel()
        updateViewModel = UpdateProfileViewModel()
        backButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backButtonAction(_:))))
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backButtonAction(_:))))
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getImageProfileUser(_:))))
        cameraImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getImageProfileUser(_:))))
        backgroundGender.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveGenderIcon(_:))))
    }
    func loadUserProfile() {
        Utility.showHudLoading()
        getProfileViewModel.loadProfileUser { [self] (data, errorModelResponse) in
            if errorModelResponse == nil {
                nameTextField.text = data?.extraInfo?.fullName ?? ""
                phoneTextField.disabledColor = UIColor.darkGray
                emailTextField.disabledColor = UIColor.darkGray
                phoneTextField.text = data?.mobile ?? ""
                emailTextField.text = data?.email ?? ""

                genderLabel.text = data?.extraInfo?.gender?.rawValue ?? "MALE"
                if genderLabel.text == "MALE" {
                    isMaleIcon = true
                    self.genderIcon.layer.frame.origin.x = 100
                    self.genderIcon.image = UIImage(named: "maleIcon")
                    self.genderLabel.text = "MALE"
                } else {
                    isMaleIcon = false
                    self.genderIcon.layer.frame.origin.x = 0
                    self.genderIcon.image = UIImage(named: "femaleIcon")
                    self.genderLabel.text = "FEMALE"
                }
                if data?.extraInfo?.avatarImageUrl != nil {
                    profileImageView.sd_imageIndicator?.startAnimatingIndicator()
                    profileImageView.sd_setImage(with: URL.init(string: (data?.extraInfo?.avatarImageUrl) ?? "")) { (_, _, _, _) in
                        profileImageView.sd_imageIndicator?.stopAnimatingIndicator()
                    }
                    self.profileImageView.cornerRadius = self.profileImageView.layer.frame.height / 2
                    self.profileImageView.clipsToBounds = true
                }

                Utility.hideSuccessHudLoading()
            } else {
                Utility.hideHudLoading()
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        saveButton.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    override func viewWillDisappear(_ animated: Bool) {
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.barStyle = .default
    }
    @objc func backButtonAction(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func getImageProfileUser(_ sender: UITapGestureRecognizer) {
        let camera = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.getImageFromCamera()
        }
        let library = UIAlertAction(title: "Library", style: .default) { (_) in
            self.getImageFromLibrary()
        }
        self.showActionSheet(title: "Select Photo", message: "choose a way", style: .actionSheet, actions: [camera, library, actionMessageCancel()])
    }
    func getImageFromLibrary() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    func getImageFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            imagePicker.cameraDevice = .front
            imagePicker.showsCameraControls = true
            present(imagePicker, animated: true, completion: nil)
        }
    }
    @objc func moveGenderIcon(_ sender: UITapGestureRecognizer) {
        //code
        if isMaleIcon {
            isMaleIcon = false
            UIView.animate(withDuration: 0.4) {
                self.genderIcon.layer.frame.origin.x = 0
                self.genderIcon.image = UIImage(named: "femaleIcon")
                self.genderLabel.text = "FEMALE"
            }
        } else {
            isMaleIcon = true
            UIView.animate(withDuration: 0.4) {
                self.genderIcon.layer.frame.origin.x = 100
                self.genderIcon.image = UIImage(named: "maleIcon")
                self.genderLabel.text = "MALE"
            }
        }
    }
    @IBAction fileprivate func saveButtonAction(_ sender: Any) {
        Utility.showHudLoading()
        updateViewModel.updateProfileUser(name: nameTextField.text, phone: phoneTextField.text, email: emailTextField.text, gender: isMaleIcon ? "MALE": "FEMALE", imageURL: self.imageURLString) { (_, error) in
            if error.isSuccess {
//                Utility.hideSuccessHudLoading()
                self.loadUserProfile()
            } else {
                Utility.hideHudLoading()
                self.showErrorDialog(message: error.message, okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            //
        })
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        self.profileImageView.image = image
        uploadPic()
        self.profileImageView.cornerRadius = self.profileImageView.layer.frame.height / 2
        self.profileImageView.clipsToBounds = true
    }
    fileprivate func uploadPic() {
         Utility.showHudLoading()
        guard let imageData = self.profileImageView.image?.jpegData(compressionQuality: 0.5) else {
            showErrorDialog(message: "no-image".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            return
        }
        updateViewModel.uploadProfilePicture(file: imageData) { [weak self]  (data, error) in
            Utility.hideHudLoading()
            if data != nil {
                self?.imageURLString = data?.fileId
                return
            }
            self?.showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            let maxLength = 13
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
}
