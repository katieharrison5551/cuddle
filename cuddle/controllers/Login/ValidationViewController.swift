//
//  validationViewController.swift
//  cuddle
//
//  Created by MohammadReza on 9/1/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import OTPFieldView

class ValidationViewController: UIViewController {
    let initialTimerCount = 120

    var timerCount: Int!
    var timer: Timer!
    @IBOutlet private var logoIcon: UIButton! {
        didSet {
            logoIcon.setTitle(String.fontIconString(name: "logo"), for: .normal)
        }
    }
    @IBOutlet private var enterOtpTitle: UILabel!
    @IBOutlet private var otpTextField: OTPFieldView!
    @IBOutlet private var resendOtpButton: UIButton!
    @IBOutlet private var mobileNumberLabel: UILabel!
    @IBOutlet private var counterLabel: UILabel!
    @IBOutlet private var verifyButton: UIButton!
    @IBOutlet private var btnChangeNumber: UIButton!
    var mobile: String!
    var activateMobileViewModel: ActivateMobileEmailViewModel!
    var enteredOtp: String!
    fileprivate var tokenViewModel: GetTokenViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    fileprivate func initView() {
        activateMobileViewModel = ActivateMobileEmailViewModel()
        tokenViewModel = GetTokenViewModel()
        mobileNumberLabel.text = mobile
        setupOTPTimer()
        setupOtpView()
        self.hideKeyboardWhenTappedAround()
    }
    fileprivate func setupOTPTimer() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }

        resendOtpButton.isEnabled = false
        btnChangeNumber.isEnabled = false
        timerCount = initialTimerCount
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(self.update)), userInfo: nil, repeats: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        verifyButton.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barStyle = .black
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    fileprivate func setupOtpView() {
        self.otpTextField.fieldsCount = 5
        if let font = UIFont(name: CustomFont.Regular, size: 18) {
            self.otpTextField.fieldFont = font
        }
        self.otpTextField.fieldBorderWidth = 1
        self.otpTextField.defaultBorderColor = .cmiddleGray
        self.otpTextField.filledBorderColor = .cmiddleGray
        self.otpTextField.cursorColor = UIColor.red
        self.otpTextField.displayType = .underlinedBottom
        self.otpTextField.fieldSize = 40
        self.otpTextField.separatorSpace = 20
        self.otpTextField.shouldAllowIntermediateEditing = false
        self.otpTextField.delegate = self
        self.otpTextField.initializeUI()
    }
    @objc fileprivate func update() {
        if timerCount == 0 {
            resendOtpButton.titleLabel?.textColor = .lightRed
            resendOtpButton.isEnabled = true

            btnChangeNumber.titleLabel?.textColor = .lightRed
            btnChangeNumber.isEnabled = true
            return
        }

        timerCount -= 1
        let minutesCalc = timerCount / 60 % 60
        let secondCalc = timerCount % 60

        counterLabel.text = String(format: "%02d:%02d", minutesCalc, secondCalc)
    }
    @IBAction fileprivate func resendOtpButtonAction(_ sender: Any) {
        Utility.showHudLoading(title: "", message: "")
        SignupSigninViewModel().registerUser(withMobile: mobile) { [weak self] (response) in
            print(response.message)
            if response.isSuccess {
                Utility.hideSuccessHudLoading()
                self?.setupOTPTimer()
            } else {
                Utility.hideHudLoading()
                self?.showErrorDialog(message: response.message, okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }
    @IBAction fileprivate func verifyButtonAction(_ sender: Any) {
        if  enteredOtp != nil {
            verifyMobileNumber()
        } else {
            showErrorDialog(message: "empty-otp".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
        }
    }
    fileprivate func verifyMobileNumber() {
        AnalyticManager.logEvent(name: "Login", params: ["userName": mobile as Any])
        Utility.showHudLoading(title: "", message: "")
        activateMobileViewModel.activate(withMobile: mobile, code: enteredOtp) { [weak self] (response) in
            if response.isSuccess {
                self?.getAccessToken()
            } else {
                Utility.hideHudLoading()
                self?.showErrorDialog(message: response.message, okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }
        }
    }

    fileprivate func getAccessToken() {
        tokenViewModel.authenticate(withUser: mobile, andPass: enteredOtp) { [weak self] (response) in
            if response.isSuccess {
                Utility.hideHudLoading()
                self?.navigationController?.popToRootViewController(animated: false)
            } else {
                Utility.hideHudLoading()
                self?.showErrorDialog(message: response.message, okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }
        }
    }

    @IBAction private func onButtonChangeNumberClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension ValidationViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    func enteredOTP(otp otpString: String) {
        enteredOtp = otpString
        verifyMobileNumber()
    }
}
