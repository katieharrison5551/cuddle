import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKLoginKit
import SafariServices
import PhoneNumberKit
import CountryPickerView

class SigninSignupViewController: UIViewController, SigninWithAppleProtocol, SigninWithFBProtocol, SSASideMenuDelegate, SFSafariViewControllerDelegate {

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var logoIcon: UIButton! {
        didSet {
            logoIcon.setTitle(String.fontIconString(name: "logo"), for: .normal)
        }
    }
    @IBOutlet private var phoneNumberTextField: PhoneNumberTextField!
    @IBOutlet private var countryPicker: CountryPickerView!
    @IBOutlet private var sendButton: UIButton!
    @IBOutlet private var termsAndConditionsButton: UIButton! {
        didSet {
            let attributedString = NSMutableAttributedString(string: "I agree with terms and conditions")
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length))
            termsAndConditionsButton.titleLabel?.attributedText = attributedString
        }
    }
    @IBOutlet  private var signinWithGoogle: UIView!
    @IBOutlet  private var googleIcon: UILabel! {
        didSet {
            googleIcon.text = String.fontIconString(name: "google")
        }
    }
    @IBOutlet private var loginProviderStackView: UIStackView!
    @IBOutlet  private var fbView: UIView!
    @IBOutlet  private var fbIcon: UILabel! {
        didSet {
            fbIcon.text = String.fontIconString(name: "facebook")
        }
    }
    var appleLoginViewModel: AppleSSOViewModel!
    var tokenViewModel: GetTokenViewModel!
    var facebookSSOViewModel: FacebookSSOViewModel!
    var sideMenu: SSASideMenu!
    var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        initPhoneNumberTextField()
        initView()
    }
    func initPhoneNumberTextField() {
        phoneNumberTextField.withPrefix = true
        phoneNumberTextField.withFlag = false
        phoneNumberTextField.withExamplePlaceholder = true
        countryPicker.showPhoneCodeInView = false
        countryPicker.showCountryCodeInView = false
        countryPicker.delegate = self
        if let phoneNumber = phoneNumberTextField.phoneNumber {
            countryPicker.setCountryByCode("\(phoneNumber.countryCode)")
        }
    }
    func initView() {
        hideKeyboardWhenTappedAround()
        setTapListener()
        setNotificationListener()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        appleLoginViewModel = AppleSSOViewModel(signinHandler: self)
        facebookSSOViewModel = FacebookSSOViewModel(fbSigninHandler: self)
        tokenViewModel = GetTokenViewModel()
        termsAndConditionsButton.addTarget(self, action: #selector(onTermsAndConditionsButtonClick(_:)), for: .touchUpInside)
        if #available(iOS 13.0, *) {
            let authorizationButton = appleLoginViewModel.setupProviderLoginView(self.loginProviderStackView)
            authorizationButton.layer.shadowRadius = 8
            authorizationButton.layer.shadowOffset = CGSize(width: 2, height: 4)
            authorizationButton.layer.shadowColor = UIColor(named: "cdarkGray")?.cgColor
            authorizationButton.layer.shadowOpacity = 6
            authorizationButton.addTarget(self, action: #selector(appleAuthorizationAction), for: .touchUpInside)
        }
        prepareToShowMainPage()
    }
    func prepareToShowMainPage() {
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let leftVC = mainSB.instantiateViewController(withIdentifier: "LeftMenuViewController")
        let viewController = AppStoryboard.Main.viewController(viewControllerClass: MapViewController.self)
        sideMenu = SSASideMenu(contentViewController: viewController, leftMenuViewController: leftVC)
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        sendButton.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    @available(iOS 13.0, *)
    @objc func appleAuthorizationAction() {
        appleLoginViewModel.handleAuthorizationAppleIDButtonPress()
    }
    fileprivate func getAccessToken(username: String, pass: String) {
        tokenViewModel.authenticate(withUser: username, andPass: pass) { [weak self] (response) in
            if response.isSuccess {
                Utility.hideHudLoading()
//                self.navigationController?.pushViewController(self.sideMenu ?? UIViewController(), animated: true)
                self?.navigationController?.popToRootViewController(animated: false)
            } else {
                Utility.hideHudLoading()
                self?.showErrorDialog(message: response.message ?? "server-unreachable".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
            }
        }
    }
    func signinCompleteion(userName: String?, password: String?, isSuccess: Bool, error: ErrorMessageModel?) {

        if isSuccess, let user = userName, let pass = password {
            Utility.showHudLoading()
            self.getAccessToken(username: user, pass: pass)
        } else {
            Utility.hideHudLoading()
            showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
        }
    }
    func fbSigninCompleteion(isSuccess: Bool, error: ErrorMessageModel?) {
              Utility.hideHudLoading()
                 if isSuccess {
                     UserDefaults.init().set(true, forKey: "IsLogin")
                    self.navigationController?.pushViewController(self.sideMenu ?? UIViewController(), animated: true)
                 } else {
                    showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
                 }
          }
    fileprivate func setTapListener() {
        let tapOnSigninWithGoogle = UITapGestureRecognizer.init(target: self, action: #selector(self.onSigninWithGoogletap(_:)))
        signinWithGoogle.addGestureRecognizer(tapOnSigninWithGoogle)
        let tapOnSigninWithFb = UITapGestureRecognizer.init(target: self, action: #selector(self.onSigninWithFBtap(_:)))
        fbView.addGestureRecognizer(tapOnSigninWithFb)

    }
    fileprivate func setNotificationListener() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(userDidSignInGoogle(_:)),
                                               name: .signInGoogleCompleted,
                                               object: nil)
    }
    @objc private func userDidSignInGoogle(_ notification: Notification) {
        Utility.showHudLoading()
        SigninWithGoogleViewModel().signinWithGoogle { [weak self](user, response) in
            if response.isSuccess {
                self?.getAccessToken(username: user?.profile.email ?? "", pass: user?.authentication.idToken ?? "")
            } else {
                Utility.hideHudLoading()
                self?.showErrorDialog(message: response.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }
    @objc func onSigninWithGoogletap(_ sender: UITapGestureRecognizer) {
        GIDSignIn.sharedInstance().signIn()
    }
    @objc func onSigninWithFBtap(_ sender: UITapGestureRecognizer) {
        facebookSSOViewModel.login(controller: self)
    }
    @objc private func onTermsAndConditionsButtonClick(_ sender: UITapGestureRecognizer) {
        if let url = NSURL(string: "http://www.cuddlecart.com/home") {
            let safariVC: SFSafariViewController = SFSafariViewController(url: url as URL)
            safariVC.delegate = self
            self.present(safariVC, animated: true, completion: nil)
        }
    }
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    // MARK: - sign in or sign up actions
    @IBAction private func signinSignupAction(_ sender: Any) {
        if phoneNumberTextField.isValidNumber, let phoneNumber = phoneNumberTextField.phoneNumber {
            Utility.showHudLoading(title: "", message: "")
            let phone = "+\(phoneNumber.countryCode)\(phoneNumber.nationalNumber)"
            SignupSigninViewModel().registerUser(withMobile: phone) { [weak self] (response) in
                print(response.message)
                if response.isSuccess {
                    Utility.hideSuccessHudLoading()
                    let controller = AppStoryboard.SigninSignupStoryboard.viewController(viewControllerClass: ValidationViewController.self)
                    controller.mobile = phone
                    self?.navigationController?.pushViewController(controller, animated: true)
                } else {
                    Utility.hideHudLoading()
                    self?.showErrorDialog(message: response.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
                }
            }
        } else {
            showErrorDialog(message: "invalid-mobile".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
        }
    }
}

extension SigninSignupViewController: CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        phoneNumberTextField.text = country.phoneCode
    }
}
