//
//  CreditCardListViewController.swift
//  cuddle
//
//  Created by Shima on 10/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import Stripe
class CreditCardListViewController: UIViewController {
    @IBOutlet private var chooseBtn: CustomUIButton!
    @IBOutlet private var heightConstraintChooseButton: NSLayoutConstraint!
    @IBOutlet private var mainTable: UITableView!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    var cardItems: [BankCardDto]?
    var selectedIndex: Int = 0
    var pageNum: Int = 0
    var allbanckGtCount: Int = 0
    var creditCardViewModel: CreditCardViewModel!
    var stripeViewModel: StripeViewModel!
    var bankGatewayViewModel: BankGatewayViewModel!
    var walletToken: String!
    var walletTokenID: String!
    var bankGetwayId: String!
    var isFromMenu: Bool = false
    fileprivate var selectedCard: BankCardDto!
    fileprivate var selectedCardId: String!
    static let loadingTitle = "Please wait"
    static let loadingMessage = "We will try to process your payment"

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.barStyle = .black
        initView()
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.barTintColor = UIColor(named: "darkRed")
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: CustomFont.fontBold, size: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        getAllCards()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        chooseBtn.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    fileprivate func initView() {
        creditCardViewModel = CreditCardViewModel()
        stripeViewModel = StripeViewModel()
        bankGatewayViewModel = BankGatewayViewModel()
        mainTable?.estimatedRowHeight = 100
        mainTable?.rowHeight = UITableView.automaticDimension
        mainTable?.register(HorizontalCollectionCardTableViewCell.nib, forCellReuseIdentifier: HorizontalCollectionCardTableViewCell.identifier)
        mainTable?.register(CreditCardTableViewCell.nib, forCellReuseIdentifier: CreditCardTableViewCell.identifier)
        self.navigationItem.rightBarButtonItem?.target = self
        self.navigationItem.rightBarButtonItem?.action = #selector(addTapped)
    }
    @objc fileprivate func addTapped() {
        self.navigationController?.pushViewController(AppStoryboard.BankCard.viewController(viewControllerClass: ADDEditCreditCardViewController.self), animated: true)
    }
    @IBAction fileprivate func backBtnAction(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.navigationBar.barStyle = .default
    }
    @IBAction fileprivate func chooseBtnAction(_ sender: Any) {
        if   let item = cardItems, !item.isEmpty {
            selectedCard = item[selectedIndex]
            createStripeCard()
        }
    }

    fileprivate func createStripeCard() {
        Utility.showHudLoading(title: CreditCardListViewController.loadingTitle, message: CreditCardListViewController.loadingMessage)
        let cardParams = STPCardParams()
        cardParams.number = selectedCard.number
        cardParams.expMonth = UInt(selectedCard.expirationMonth!)
        cardParams.expYear = UInt(selectedCard.expirationYear!)
        cardParams.cvc = selectedCard.cvv2
        let sourceParams = STPSourceParams.cardParams(withCard: cardParams)
        STPAPIClient.shared().createSource(with: sourceParams) { [weak self] (source, error: Error?) in
            if let stripeSource = source, stripeSource.flow == .none && stripeSource.status == .chargeable {
                debugPrint("success")
                debugPrint(stripeSource.stripeID)
                self?.selectedCardId = stripeSource.stripeID
                self?.getAllBank()
            } else {
                debugPrint("failed check vpn:)")
                Utility.hideHudLoading()
                self?.showErrorDialog(title: "error".getString(), message: error?.localizedDescription ?? "somthing-wrong".getString(),
                                      okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                      okButtonCompletion: {
                                        self?.createStripeCard()
                                        Utility.showHudLoading(title: CreditCardListViewController.loadingTitle, message: CreditCardListViewController.loadingMessage)
                                      }, cancelButtonCompletion: nil)
            }
        }
    }

    fileprivate func getAllBank() {
        Utility.showHudLoading(title: CreditCardListViewController.loadingTitle, message: "Getting bank gateway...")
        bankGatewayViewModel.getAllBankGateway(pageNumber: "\(allbanckGtCount)", pageSize: "10", sort: Sort_getAllBankGateway.asc, sortKey: nil) { [weak self] (data, error) in
            if !(data?.content?.isEmpty ?? false) {
                if let response = data?.content {
                    for getwayItem in response {
                        while getwayItem.type == BankGatewayDto.ModelType.stripe {
                            self?.bankGetwayId = getwayItem._id ?? ""
                            self?.getStripeIntent(bankGatewayId: getwayItem._id ?? "")
                            return
                        }
                    }
                }
            }
            Utility.hideHudLoading()
            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                  okButtonCompletion: {
                                    self?.getAllBank()
                                  }, cancelButtonCompletion: nil)
        }
    }

    fileprivate func getStripeIntent(bankGatewayId: String) {
        Utility.showHudLoading(title: CreditCardListViewController.loadingTitle, message: "We try to check and confirm card info")
        stripeViewModel.doStripeTransaction(amount: FactorModel.modelInfo.finalCost ?? 100,
                                            bankGatewayId: bankGatewayId,
                                            walletTransactionType: CreateSubscriberWalletTransactionDto.WalletTransactionType.deposit) { [weak self] (depositData, error) in
            if depositData != nil {
                let paymentLinks = depositData?.paymentLink?.components(separatedBy: "/")
                if let payments = paymentLinks {
                    self?.walletToken = payments[payments.count - 1]
                } else {
                    self?.walletToken = depositData?._id
                }
                self?.walletTokenID = depositData?._id
                self?.processPayment(cardId: self?.selectedCard._id ?? "")
                return
            }

            Utility.hideHudLoading()
            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                  okButtonCompletion: {
                                    self?.getStripeIntent(bankGatewayId: bankGatewayId)
                                  }, cancelButtonCompletion: nil)
        }
    }

    fileprivate func processPayment(cardId: String) {
        Utility.showHudLoading(title: CreditCardListViewController.loadingTitle, message: "We try to process your payment")
        stripeViewModel.processPayment(cardId: cardId, token: self.walletToken) { [weak self] (processData, error) in
            if error == nil {
                self?.payUsingIntent(clientSecret: processData?.clientSecret ?? "")
                return
            }

            Utility.hideHudLoading()
            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                  okButtonCompletion: {
                                    self?.processPayment(cardId: cardId)
                                  }, cancelButtonCompletion: nil)
        }
    }

    func payUsingIntent(clientSecret: String) {
        let paymentIntentParams = STPPaymentIntentParams(clientSecret: clientSecret)

        STPAPIClient.shared().confirmPaymentIntent(with: paymentIntentParams, completion: { [weak self] (paymentIntent, error) in
            if let error = error {
                print(error)
                Utility.hideHudLoading()
                self?.showErrorDialog(title: "error".getString(), message: error.localizedDescription,
                                      okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                      okButtonCompletion: {
                                        self?.payUsingIntent(clientSecret: clientSecret)
                                      }, cancelButtonCompletion: nil)
            } else if let paymentIntent = paymentIntent {
                if paymentIntent.status == .succeeded {
                    if let token = self?.walletTokenID {
                        let controller = AppStoryboard.BankCard.viewController(viewControllerClass: PaymentStatusViewController.self)
                        controller.token = token
                        controller.bankGatewayId =  self?.bankGetwayId
                        Utility.hideSuccessHudLoading()
                        self?.navigationController?.pushViewController(controller, animated: true)
                    }
                } else {
                    Utility.hideHudLoading()
                    self?.showErrorDialog(title: "error".getString(), message: "Try Again".getString(),
                                          okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                          okButtonCompletion: {
                                            self?.payUsingIntent(clientSecret: clientSecret)
                                            Utility.showHudLoading()
                                          }, cancelButtonCompletion: nil)
                }
            }
        })
    }
    @objc func moreBtnAction(sender: UIButton) {
        self.showActionSheet(title: NSLocalizedString("Credit Card", comment: ""),
                             message: NSLocalizedString("EditDeleteCard", comment: ""),
                             style: .actionSheet,
                             actions: [self.actionMessage( NSLocalizedString("Edit", comment: ""), style: .default, {
                                let controller = AppStoryboard.BankCard.viewController(viewControllerClass: ADDEditCreditCardViewController.self)
                                if let items = self.cardItems {
                                    controller.cardItem =  items[sender.tag]
                                }
                                self.navigationController?.pushViewController(controller, animated: true)
                             }), self.actionMessage(NSLocalizedString("Delete", comment: ""), style: .destructive, {
                                self.showActionSheet(title: NSLocalizedString("Delete", comment: ""),
                                                     message: NSLocalizedString("deleteCard", comment: ""),
                                                     style: .actionSheet,
                                                     actions: [self.actionMessage(NSLocalizedString("yes", comment: ""), style: .destructive, {
                                                        if let items = self.cardItems {
                                                            self.deleteCreditCard(cardId: items[sender.tag]._id)
                                                        }
                                                     }), self.actionMessageCancel()])
                             }), self.actionMessageCancel()])
    }
    fileprivate func deleteCreditCard(cardId: String?) {
        Utility.showHudLoading(title: "", message: "")
        creditCardViewModel.deleteCard(cardId: cardId) { (data, error) in
            if data ?? false {
                Utility.hideSuccessHudLoading()
                self.getAllCards()
            } else {
                Utility.hideHudLoading()
                self.showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }
    fileprivate func getAllCards() {
        Utility.showHudLoading(title: "", message: "")
        creditCardViewModel.getAllCards(pageNumber: "\(pageNum)", pageSize: "10", sort: Sort_getAll.asc, sortKey: nil) { (data, _) in
            if data != nil {
                Utility.hideHudLoading()
                self.cardItems = data
                self.selectedIndex = 0
                self.mainTable.reloadData()
            } else {
                Utility.hideHudLoading()
            }
        }
    }
}
extension CreditCardListViewController: ReturnSelectedIndex {
    func selectedIndex(index: IndexPath) {
        self.selectedIndex = index.section
        mainTable.reloadSections([1], with: .none)
    }
}
extension CreditCardListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let items = cardItems, !items.isEmpty  else {
            chooseBtn.isHidden = true
            self.mainTable.removeNoDataView()
            mainTable.showNoDataView(titleDesc: "noAddCard".getString(), viewFrame: CGRect(x: 20, y: 50, width: mainTable.frame.size.width-40, height: 80), color: .white)
            bottomConst.constant = 5
            return 0
        }
        chooseBtn.isHidden = isFromMenu ? true : false
        bottomConst.constant = isFromMenu ? 5 : 85
        self.mainTable.removeNoDataView()
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let items = cardItems, !items.isEmpty  else {
            return 0
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let items = cardItems, !items.isEmpty  else {
            return UITableViewCell()
        }
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: HorizontalCollectionCardTableViewCell.identifier, for: indexPath) as? HorizontalCollectionCardTableViewCell {
                cell.setupCell(items)
                cell.selectedIndexDelegate = self
                cell.mainCollection.reloadData()
                DispatchQueue.main.async {
                    cell.mainCollection.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
                }
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CreditCardTableViewCell.identifier, for: indexPath) as? CreditCardTableViewCell {
                cell.setupCell(cardItems: items[selectedIndex])
                cell.moreBtn.addTarget(self, action: #selector(self.moreBtnAction), for: .touchUpInside)
                cell.moreBtn.tag = selectedIndex
                return cell
            }
        default:
            break
        }
        UITableViewCell().selectionStyle = .none
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UIScreen.main.bounds.width / 1.6
        default:
            return 460.0
        }
    }
}
