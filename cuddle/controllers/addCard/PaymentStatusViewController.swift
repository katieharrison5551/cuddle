//
//  PaymentStatusViewController.swift
//  cuddle
//
//  Created by Shima on 10/20/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class PaymentStatusViewController: UIViewController, SSASideMenuDelegate {
    @IBOutlet private var payStatusLabel: UILabel!
    @IBOutlet private var mainBtn: CustomUIButton!
    @IBOutlet private var lblMessage: UILabel!

    var token: String!
    var bankGatewayId: String!
    var stripeViewModel: StripeViewModel!
    var sideMenu: SSASideMenu!
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setRightBarButton(nil, animated: false)
        self.navigationItem.setLeftBarButton(nil, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.title = "Payment Status".getString()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainBtn.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    fileprivate func initView() {
        payStatusLabel.text = PaymentStatus.waiting.rawValue
        Utility.showHudLoading(title: "Please wait", message: "We try to finalize your payment")
        stripeViewModel = StripeViewModel()
        if let walletToken = self.token {
            callTransaction(token: walletToken)
        }

        //prepare to back
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        let leftVC = mainSB.instantiateViewController(withIdentifier: "LeftMenuViewController")
        let mainPage = AppStoryboard.Main.viewController(viewControllerClass: MapViewController.self)
        sideMenu = SSASideMenu(contentViewController: mainPage, leftMenuViewController: leftVC)
        sideMenu.configure(SSASideMenu.MenuViewEffect(fade: true, scale: true, scaleBackground: false))
        sideMenu.configure(SSASideMenu.ContentViewEffect(alpha: 1.0, scale: 0.7))
        sideMenu.configure(SSASideMenu.ContentViewShadow(enabled: true, color: UIColor.black, opacity: 0.6, radius: 6.0))
        sideMenu.delegate = self
    }
    @IBAction fileprivate func goToHomeAction(_ sender: Any) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.navigationController?.popToRootViewController(animated: true)
    }
    fileprivate func callTransaction(token: String) {
        Utility.showHudLoading(title: "Please wait", message: "We try to finalize your payment")
        stripeViewModel.getWalletTransaction(walletId: token) {[weak self] (data, error) in
//            Utility.hideHudLoading()
            if data != nil {
                if data?.currentState == .done {
                    if let bankID = self?.bankGatewayId {
//                        Utility.showHudLoading()
                        self?.doDebitAction(bankGetwayId: bankID)
                    }
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        self?.payStatusLabel.text = "\(self?.payStatusLabel.text ?? "")."
                        self?.callTransaction(token: token)
                    }
                }
            } else {
                self?.payStatusLabel.text = PaymentStatus.failure.rawValue
                self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                      okButtonTitle: "retry".getString(), cancelButtonTitle: "close".getString(),
                                      okButtonCompletion: {
                                        self?.callTransaction(token: self?.token ?? "")
                                      }, cancelButtonCompletion: nil)
            }
        }
    }
    fileprivate func doDebitAction(bankGetwayId: String) {
        stripeViewModel.doStripeTransaction(amount: FactorModel.modelInfo.finalCost ?? 100,
                                            bankGatewayId: bankGetwayId,
                                            walletTransactionType: CreateSubscriberWalletTransactionDto.WalletTransactionType.debit) { [weak self] (data, error) in

            if data != nil {
                self?.payStatusLabel.text = PaymentStatus.success.rawValue
                self?.lblMessage.isHidden = false
                Utility.hideSuccessHudLoading()
                return
            }
            Utility.hideHudLoading()
            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "close".getString(),
                                  okButtonCompletion: {
                                    Utility.showHudLoading()
                                    self?.doDebitAction(bankGetwayId: self?.bankGatewayId ?? "")
                                  }, cancelButtonCompletion: nil)
        }
    }
}
