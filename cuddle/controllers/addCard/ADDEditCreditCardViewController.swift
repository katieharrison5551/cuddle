//
//  ADDEditCreditCardViewController.swift
//  cuddle
//
//  Created by Shima on 9/30/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class ADDEditCreditCardViewController: UIViewController, UITextFieldDelegate {
    var creditCardViewModel: CreditCardViewModel!
    @IBOutlet private var firstUnitCardNum: CustomTextfield!
    @IBOutlet private var secUnitCardNum: CustomTextfield!
    @IBOutlet private var thirdUnitCardNum: CustomTextfield!
    @IBOutlet private var lastUnitCardNum: CustomTextfield!
    @IBOutlet private var expMonth: CustomTextfield!
    @IBOutlet private var expYear: CustomTextfield!
    @IBOutlet private var cvv2: CustomTextfield!
    @IBOutlet private var name: FloatingCustomTxtFld!
    @IBOutlet private var address: FloatingCustomTxtFld!
    @IBOutlet private var zipCode: FloatingCustomTxtFld!
    @IBOutlet private var country: FloatingCustomTxtFld!
    @IBOutlet private var city: FloatingCustomTxtFld!
    @IBOutlet private var state: FloatingCustomTxtFld!
    @IBOutlet private var saveBtn: CustomUIButton!
    @IBOutlet private var mainsclView: UIScrollView!{
        didSet {
            mainsclView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    var cardTextFieldArray: [CustomTextfield] {
        return [firstUnitCardNum, secUnitCardNum, thirdUnitCardNum, lastUnitCardNum]
    }
    var floatingTxtfld: [FloatingCustomTxtFld] {
        return [name, address, zipCode, country, city, state]
    }
    var cardItem: BankCardDto!
    var parentVC: UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    override func viewDidAppear(_ animated: Bool) {
        saveBtn.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    fileprivate func initView() {
        self.hideKeyboardWhenTappedAround()
        creditCardViewModel = CreditCardViewModel()
        [firstUnitCardNum, secUnitCardNum, thirdUnitCardNum, lastUnitCardNum, expMonth, expYear, cvv2, name, address, zipCode, country, city, state].forEach { textField in
            textField?.delegate = self
            textField?.addTarget(self, action: #selector(ADDEditCreditCardViewController.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
            textField?.clearButtonMode = .never

        }
        if let item = cardItem {
            getCardItem(item: item)
        }
    }

    fileprivate func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = UIColor(named: "darkRed")
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: CustomFont.fontBold, size: 20), NSAttributedString.Key.foregroundColor : UIColor.white]
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationItem.rightBarButtonItem = nil
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationBar()
        setNeedsStatusBarAppearanceUpdate()
    }
    fileprivate func getCardItem(item: BankCardDto) {
        if let exInfo = item.extraInfo, let add = exInfo.address, let cardNumber = item.number {
            let cardNumArray = cardNumber.components(withMaxLength: 4)
            firstUnitCardNum.text = cardNumArray[0]
            secUnitCardNum.text = cardNumArray[1]
            thirdUnitCardNum.text = cardNumArray[2]
            lastUnitCardNum.text = cardNumArray[3]
            expYear.text = "\(String(describing: item.expirationYear!))"
            expMonth.text = "\(String(describing: item.expirationMonth!))"
            cvv2.text = item.cvv2
            name.text = add.fullName
            address.text = add.address
            zipCode.text = add.zipCode
            country.text = add.country
            state.text = add.state
            city.text = add.city
            saveBtn.setTitle(NSLocalizedString("edit", comment: ""), for: .normal)
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case firstUnitCardNum:
            if textField.text?.count ?? 0 >= 4 {
                secUnitCardNum?.becomeFirstResponder()
            }
        case secUnitCardNum:
            if textField.text?.count ?? 0 >= 4 {
                thirdUnitCardNum?.becomeFirstResponder()
            }
        case thirdUnitCardNum:
            if textField.text?.count ?? 0 >= 4 {
                lastUnitCardNum?.becomeFirstResponder()
            }
        case lastUnitCardNum:
            if textField.text?.count ?? 0 >= 4 {
                expMonth?.becomeFirstResponder()
            }
        case expMonth:
            if textField.text?.count ?? 0 >= 2 {
                expYear?.becomeFirstResponder()
            }
        case expYear:
            if textField.text?.count ?? 0 >= 2 {
                cvv2?.becomeFirstResponder()
            }
        case cvv2:
            if textField.text?.count ?? 0 >= 3 {
                name?.becomeFirstResponder()
            }
        default:
            break
        }
    }
    fileprivate func setErrorOnTxtFld(textfieldArray: [CustomTextfield?], theColor: UIColor) {
        textfieldArray.forEach { textfield in
            textfield?.layer.masksToBounds = true
            textfield?.layer.borderColor = theColor.cgColor
            textfield?.layer.borderWidth = 1.0
        }
    }
    fileprivate func setErrorOnFloatingTxtFld(txtfld: [FloatingCustomTxtFld?], theError: String) {
        txtfld.forEach { textfield in
            textfield?.errorMessage = theError
            textfield?.titleErrorColor =  .red
            textfield?.textErrorColor = .black
        }
    }
    @IBAction fileprivate func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction fileprivate func gotoCardList(_ sender: Any) {
        self.goToParent()
    }
    @IBAction fileprivate func saveCreditCardAction(_ sender: Any) {
        Utility.showHudLoading(title: "", message: "")
        let creditNum = firstUnitCardNum.text! + secUnitCardNum.text!  + thirdUnitCardNum.text!  + lastUnitCardNum.text!
        if let item = cardItem {
            creditCardViewModel.createUpdateCard(address: address.text, city: city.text, country: country.text, fullName: name.text, state: state.text, zipCode: zipCode.text, number: creditNum, cvv2: cvv2.text, expirationYear: expYear.text, expirationMonth: expMonth.text, _id: item._id ) { [weak self] (data, error) in
                if error == nil {
                    Utility.hideSuccessHudLoading()
                    self?.goToParent()
                } else {
                    Utility.hideHudLoading()
                    self?.handleErrorCompletion(error: error)
                }
            }
        } else {
            creditCardViewModel.createUpdateCard(address: address.text, city: city.text, country: country.text, fullName: name.text, state: state.text, zipCode: zipCode.text, number: creditNum, cvv2: cvv2.text, expirationYear: expYear.text, expirationMonth: expMonth.text, _id: nil ) { [weak self] (data, error) in
                if data != nil {
                    Utility.hideSuccessHudLoading()
                    self?.goToParent()
                } else {
                    Utility.hideHudLoading()
                    self?.handleErrorCompletion(error: error)
                }
            }
        }
    }

    fileprivate func goToParent() {
        self.navigationController?.popViewController(animated: true)
    }

    fileprivate func handleErrorCompletion(error: ErrorMessageModel?) {
        switch error?.code {
        case CardValidationErrorEnum.cardNum.rawValue:
            setErrorOnTxtFld(textfieldArray: cardTextFieldArray, theColor: UIColor(named: "darkRed") ?? .red)
        case CardValidationErrorEnum.expMonth.rawValue:
            setErrorOnTxtFld(textfieldArray: [expMonth], theColor: UIColor(named: "darkRed") ?? .red)
        case CardValidationErrorEnum.expYear.rawValue:
            setErrorOnTxtFld(textfieldArray: [expYear], theColor: UIColor(named: "darkRed") ?? .red)
        case CardValidationErrorEnum.cvv2.rawValue:
            setErrorOnTxtFld(textfieldArray: [cvv2], theColor: UIColor(named: "darkRed") ?? .red)
        case CardValidationErrorEnum.name.rawValue:
            setErrorOnFloatingTxtFld(txtfld: [name], theError: CardValidationErrorMessageFieldEnum.name.rawValue)
        case CardValidationErrorEnum.address.rawValue:
            setErrorOnFloatingTxtFld(txtfld: [address], theError: CardValidationErrorMessageFieldEnum.address.rawValue)
        case CardValidationErrorEnum.zipcode.rawValue:
            setErrorOnFloatingTxtFld(txtfld: [zipCode], theError: CardValidationErrorMessageFieldEnum.zipcode.rawValue)
        case CardValidationErrorEnum.country.rawValue:
            setErrorOnFloatingTxtFld(txtfld: [country], theError: CardValidationErrorMessageFieldEnum.country.rawValue)
        case CardValidationErrorEnum.city.rawValue:
            setErrorOnFloatingTxtFld(txtfld: [city], theError: CardValidationErrorMessageFieldEnum.city.rawValue)
        case CardValidationErrorEnum.state.rawValue:
            setErrorOnFloatingTxtFld(txtfld: [state], theError: CardValidationErrorMessageFieldEnum.state.rawValue)
        default:
            showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "ok".getString(), cancelButtonTitle: nil)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
         } else {
            textField.resignFirstResponder()
         }
         return false
      }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        switch textField.tag {
        case 0:
            return newLength <= 4
        case 1:
            return newLength <= 4
        case 2:
            return newLength <= 4
        case 3:
            return newLength <= 4
        case 4:
            return newLength <= 2
        case 5:
            return newLength <= 2
        case 6:
            return newLength <= 4
        default:
            break
        }
        return true
    }
}
///
