//
//  CardSpecsCollectionViewCell.swift
//  cuddle
//
//  Created by Shima on 10/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class CardSpecsCollectionViewCell: UICollectionViewCell {
    @IBOutlet private var mainView: UIView!
    @IBOutlet private var fullName: UILabel!
    @IBOutlet private var cvv2: UILabel!
    @IBOutlet private var cardNum: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(cardItems: BankCardDto?) {
        fullName.text = cardItems?.extraInfo?.address?.fullName
        cvv2.text = cardItems?.cvv2
        cardNum.text = separated(theText: cardItems?.number ?? "")
    }
    fileprivate func separated(theText: String, separator: String = "   ", stride: Int = 4) -> String {
    return  theText.enumerated().map { $0.isMultiple(of: stride) && ($0 != 0) ? "\(separator)\($1)" : String($1) }.joined()
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
}
