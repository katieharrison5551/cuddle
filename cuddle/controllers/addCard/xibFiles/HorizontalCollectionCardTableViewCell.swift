//
//  HorizontalCollectionCardTableViewCell.swift
//  cuddle
//
//  Created by Shima on 10/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

protocol ReturnSelectedIndex {
    func selectedIndex(index: IndexPath)
}
class HorizontalCollectionCardTableViewCell: UITableViewCell {
    @IBOutlet var mainCollection: UICollectionView!
    @IBOutlet private var pageCtrl: UIPageControl!
    var cardItems: [BankCardDto]?
    var selectedIndexDelegate : ReturnSelectedIndex!
    static var nib: UINib {
           return UINib(nibName: identifier, bundle: nil)
       }
       static var identifier: String {
           return String(describing: self)
       }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainCollection.dataSource = self
        self.mainCollection.delegate = self
        self.mainCollection.register(CardSpecsCollectionViewCell.nib, forCellWithReuseIdentifier: CardSpecsCollectionViewCell.identifier)
        pageCtrl.hidesForSinglePage = true
    }
    override func layoutSubviews() {
       mainCollection.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    func  setupCell(_ items: [BankCardDto]?) {
        self.cardItems = items
    }
}

extension HorizontalCollectionCardTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let items = cardItems  else {
            return 0
        }
        pageCtrl.numberOfPages = items.count
        return items.count
      
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let items = cardItems  else {
            return UICollectionViewCell()
        }
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardSpecsCollectionViewCell.identifier, for: indexPath ) as? CardSpecsCollectionViewCell {
            cell.setupCell(cardItems: items[indexPath.section])
            snapToNearestCell(scrollView: self.mainCollection)
            return cell
        }
        return UICollectionViewCell()
    }

}
extension HorizontalCollectionCardTableViewCell: UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: mainCollection.contentOffset, size: mainCollection.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = mainCollection.indexPathForItem(at: visiblePoint) {
            self.pageCtrl.currentPage = visibleIndexPath.section
        }
    }
    func snapToNearestCell(scrollView: UIScrollView) {
         let middlePoint = Int(scrollView.contentOffset.x + UIScreen.main.bounds.width / 2)
         if let indexPath = self.mainCollection.indexPathForItem(at: CGPoint(x: middlePoint, y: 0)) {
            pageCtrl.currentPage = indexPath.section
              self.mainCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
         } else {
            pageCtrl.currentPage = 0
            self.mainCollection.scrollToItem(at: IndexPath(item: 0, section: 0 ), at: .centeredHorizontally, animated: true)
         }

    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        self.snapToNearestCell(scrollView: scrollView)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.snapToNearestCell(scrollView: scrollView)
        let middlePoint = Int(scrollView.contentOffset.x + UIScreen.main.bounds.width / 2)
        if let indexPath = self.mainCollection.indexPathForItem(at: CGPoint(x: middlePoint, y: 0)) {
        self.selectedIndexDelegate?.selectedIndex(index: indexPath)
        } else {
            self.selectedIndexDelegate?.selectedIndex(index: IndexPath(item: 0, section: 0))

        }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: self.contentView.frame.size.width - 50, height: self.frame.size.height - 40 )
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
