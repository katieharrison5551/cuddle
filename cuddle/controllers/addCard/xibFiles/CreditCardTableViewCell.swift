//
//  CreditCardTableViewCell.swift
//  cuddle
//
//  Created by Shima on 10/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class CreditCardTableViewCell: UITableViewCell {
    @IBOutlet var moreBtn: UIButton! {
           didSet {
            moreBtn.setTitle(String.fontIconString(name: "more"), for: .normal)
           }
       }
    @IBOutlet private var name: UILabel!
    @IBOutlet private var address: UILabel!
    @IBOutlet private var zipCode: UILabel!
    @IBOutlet private var country: UILabel!
    @IBOutlet private var state: UILabel!
    @IBOutlet private var city: UILabel!
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    static var identifier: String {
        return String(describing: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setupCell(cardItems: BankCardDto?) {
        if let item = cardItems, let exInfo = item.extraInfo, let add = exInfo.address {

            name.attributedText = createAttributedLabel(fullString: "   Name: " + add.fullName!, firstIndex: 0, txtLength:9)
            address.attributedText = createAttributedLabel(fullString: "   Address: " + add.address!, firstIndex: 0, txtLength: 12)
            zipCode.attributedText = createAttributedLabel(fullString: "   Zip Code: " + add.zipCode!, firstIndex: 0, txtLength: 13)
            country.attributedText = createAttributedLabel(fullString: "   Country: " + add.country!, firstIndex: 0, txtLength: 12)
            state.attributedText = createAttributedLabel(fullString: "   State: " + add.state!, firstIndex: 0, txtLength: 10)
            city.attributedText = createAttributedLabel(fullString: "   City: " + add.city!, firstIndex: 0, txtLength: 9)
        }
    }
    fileprivate func createAttributedLabel(fullString: String, firstIndex: Int, txtLength: Int) -> NSMutableAttributedString {

        let attrTxt = NSMutableAttributedString.init(string: fullString)
        attrTxt.setAttributes([NSAttributedString.Key.font: UIFont(name: CustomFont.Regular, size: 18.0),
                             NSAttributedString.Key.foregroundColor: UIColor.gray],
                            range: NSMakeRange(firstIndex, txtLength))
        return attrTxt
    }
}
