//
//  ReferralCodeViewController.swift
//  cuddle
//
//  Created by MohammadReza on 10/21/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ReferralCodeViewController: UIViewController {

    // MARK: - views
    @IBOutlet private var mainView: UIView!
    @IBOutlet private var topView: UIView! {
        didSet {
            topView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet private var closeLabel: UILabel! {
        didSet {
            closeLabel.text = String.fontIconString(name: "cancel")
        }
    }
    @IBOutlet private var shareIcon: UIImageView!
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var referralCodeLabel: UILabel!
    @IBOutlet private var inputCodeTextfield: UITextField!
    @IBOutlet private var referNowButton: UIButton!
    @IBOutlet private var bottomView: UIView! {
        didSet {
            bottomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }
    }
    @IBOutlet private var userReferralCodeToShareView: UIView!
    // MARK: - init var
    var referralCodeViewModel: SubscriberLogViewModel!
    var campaignViewModel: CampaignViewModel!
    var campaignList: PageCampaignOutDto!
    var userInviterCodeViewModel: UserInviterReferralCodeViewModel!
    var referralCodeList: ReferralUserDto!

    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func viewDidAppear(_ animated: Bool) {
        referNowButton.setGradientsBackground(.lightRed, .darkRed, 12)
    }

    // MARK: - other methods
    func initView() {
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "done"
        shareIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnShareUserReferralCode(_:))))
        closeLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnCloseButton(_:))))
        userReferralCodeToShareView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnShareUserReferralCode(_:))))
        referralCodeViewModel = SubscriberLogViewModel()
        campaignViewModel = CampaignViewModel()
        userInviterCodeViewModel = UserInviterReferralCodeViewModel()
        getUserInviterCode()
    }

    func getUserInviterCode() {
        Utility.showHudLoading(title: "", message: "")
        userInviterCodeViewModel.getUserReferralList { (data, error) in
            if error == nil {
                Utility.hideHudLoading()
                self.referralCodeList = data
                self.showUserInviterReferralCode(code: self.referralCodeList.subscriberCode ?? "No Code")
            } else {
                Utility.hideHudLoading()
                self.showErrorDialog(message: ((error?.errors == nil ? error?.message : error?.errors[0].message)) ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }

    func showUserInviterReferralCode(code: String) {
        referralCodeLabel.text = code
    }

    func getCampaignID() {
        Utility.showHudLoading(title: "", message: "")
        campaignViewModel.getCampaignList { (data, error) in
            if error == nil {
                self.campaignList = data
                guard let contents = self.campaignList.content, !contents.isEmpty else {
                    self.showErrorDialog(message: "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
                    return
                }
                self.sendReferralCode(campaignId: contents[0]._id ?? "No Code")
            } else {
                Utility.hideHudLoading()
                self.showErrorDialog(message: "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }

    func sendReferralCode(campaignId: String) {
        referralCodeViewModel.createSubscriberLog(campaignId: campaignId, subscriberCode: inputCodeTextfield.text) { (_, error) in
            if error == nil {
                Utility.hideSuccessHudLoading()
                self.dismiss(animated: true, completion: nil)
            } else {
                Utility.hideHudLoading()
                self.showErrorDialog(message: ((error?.errors == nil ? error?.message : error?.errors[0].message)) ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }

    func shareYourReferralCode(clientCode: String) {
        let activityViewController = UIActivityViewController(activityItems: [clientCode], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }

    // MARK: - Actions
    @IBAction private func onClickReferNowButton(_ sender: Any) {
        getCampaignID()
    }
    @objc fileprivate func didTapOnCloseButton(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func didTapOnShareUserReferralCode(_ sender: UITapGestureRecognizer) {
        guard let referalList = referralCodeList else {
            return
        }

        guard let subscriberCode = referalList.subscriberCode else {
            return
        }
        shareYourReferralCode(clientCode: descriptionLabel.text! + " '\(subscriberCode)' ")
    }
}
