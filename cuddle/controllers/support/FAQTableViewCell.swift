//
//  FAQTableViewCell.swift
//  cuddle
//
//  Created by MohammadReza on 10/20/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {


    @IBOutlet weak var messageTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
