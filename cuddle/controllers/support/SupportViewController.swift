import UIKit
import GrowingTextView
import IQKeyboardManagerSwift

class SupportViewController: UIViewController {
    // MARK: - implement view modules
    let pageSize = "100"

    @IBOutlet private var bottomCnsBackgroundview: NSLayoutConstraint!
    @IBOutlet private var bottomCnsTextView: NSLayoutConstraint!
    @IBOutlet private var sendButton: CustomUIButton! {
        didSet {
            sendButton.setTitle(String.fontIconString(name: "sendButton"), for: .normal)
        }
    }
    @IBOutlet private var backgroundView: UIView! {
        didSet {
            backgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet private var messageTextview: GrowingTextView!
    @IBOutlet private var chatTableView: UITableView!
    @IBOutlet private var backButton: BackButton!
    @IBOutlet private var supportImageProfile: UIBarButtonItem!
    // MARK: - init variables
    var activeField: UITextView?
    var messages: [TicketMessageDto]!
    fileprivate var supportViewModel: SupportViewModel!
    fileprivate var pageNumber: Int!
    fileprivate var totalPages: Int!
    private let refreshControl = UIRefreshControl()
    fileprivate var isLoading: Bool = false
    fileprivate var subscriberId: String!

    // MARK: - override funcs
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    func initView() {
        activeField = UITextView()
        messageTextview.trimWhiteSpaceWhenEndEditing = true
        IQKeyboardManager.shared.enable = false
        hideKeyboardWhenTappedAround()
        setupNavigationView()
        supportViewModel = SupportViewModel()
        pageNumber = 0
        messages = []
        initialTicket()
    }
    fileprivate func initialTicket() {
        supportViewModel.initializedTicket { [weak self] (isSuccess, subId, error) in
            if isSuccess {
                self?.subscriberId = subId
                self?.loadMessage()
                return
            }

            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                  okButtonCompletion: {
                                    self?.initialTicket()
                                  }, cancelButtonCompletion: {
                                    self?.dismiss(animated: true, completion: nil)
                                  })
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        setupTableView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    // MARK: - regular funcs
    @objc func keyboardWillShow(notification: Notification) {
        guard let keyboardInfo = notification.userInfo else {return}
        if let keyboardSize = (keyboardInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            let keyboardHeight = keyboardSize.height
            let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
            let window = UIApplication.shared.keyWindow
            if  let bottomPadding = window?.safeAreaInsets.bottom {
            var viewRect = self.backgroundView.frame
            viewRect.size.height -= keyboardHeight
            bottomCnsBackgroundview.constant = contentInsets.bottom - bottomPadding
            bottomCnsTextView.constant = bottomCnsBackgroundview.constant + 10.0
            }
        }
    }
    @objc func keyboardWillHide(notification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        bottomCnsBackgroundview.constant = contentInsets.bottom
        bottomCnsTextView.constant = bottomCnsBackgroundview.constant + 16
    }
    func setupNavigationView() {
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        supportImageProfile.image = UIImage(named: "profileImage")
        supportImageProfile.isEnabled = false
    }
    @IBAction private func onTapBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func setupTableView() {
        chatTableView.keyboardDismissMode = .onDrag
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.register(UINib(nibName: "SupportSenderCell", bundle: nil), forCellReuseIdentifier: "sender")
        chatTableView.register(UINib(nibName: "SupportRecieverCell", bundle: nil), forCellReuseIdentifier: "reciever")
        chatTableView?.estimatedRowHeight = 100
        chatTableView?.rowHeight = UITableView.automaticDimension
        refreshControl.addTarget(self, action: #selector(SupportViewController.loadMore), for: .valueChanged)
        chatTableView.refreshControl = refreshControl
    }
    @IBAction private func onSendMessageClick(_ sender: Any) {
        guard let message = messageTextview.text, !message.isEmpty else {
            return
        }
        sendMessage(message: message)
    }
    @IBAction private func onBackButtonClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func loadMore(_ sender: Any) {
        isLoading = true
        guard let allPages = self.totalPages else {
            pageNumber = 0
            loadMessage()
            return
        }
        if allPages > pageNumber {
        pageNumber += 1
        loadMessage()
        }
    }
    func loadMessage() {
   if pageNumber == 0 {
    Utility.showHudLoading()
   }
        supportViewModel.loadMessage(pageNumber: "\(pageNumber ?? 0)", pageSize: pageSize) { [weak self] (response, error) in
            Utility.hideHudLoading()
            if error == nil {
                guard let responseMessages = response else {
                    return
                }
                self?.totalPages = responseMessages.totalPages
                self?.showAllMessage(pageMessage: responseMessages)
                return
            }

            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                  okButtonCompletion: {
                                    self?.loadMessage()
                                  }, cancelButtonCompletion: {
                                    self?.dismiss(animated: true, completion: nil)
                                  })
        }
    }
    fileprivate func showAllMessage(pageMessage: PageTicketMessageDto) {
        if pageNumber != 0 {
            addOldMessage(oldMessage: pageMessage.content ?? [])
        } else {
            addNewMessage(newMessage: pageMessage.content ?? [])
        }
        let scrollIndex = chatTableView.indexPathsForVisibleRows?.first
        chatTableView.reloadData()
        if let scroll = scrollIndex {
            chatTableView.scrollToRow(at: scroll, at: .top, animated: false)
        } else {
            chatTableView.scrollToBottom()
        }
        refreshControl.endRefreshing()
    }
    fileprivate func addNewMessage(newMessage: [TicketMessageDto]) {
        messages.removeAll()
        messages.append(contentsOf: newMessage)
    }
    fileprivate func addOldMessage(oldMessage: [TicketMessageDto]) {
        messages.append(contentsOf: oldMessage)
        messages = removeDuplicateElements(messages: messages)
    }
    func removeDuplicateElements(messages: [TicketMessageDto]) -> [TicketMessageDto] {
        var uniqueMessages = [TicketMessageDto]()
        for message in messages {
            if !uniqueMessages.contains(where: {$0._id == message._id }) {
                uniqueMessages.append(message)
            }
        }
        return uniqueMessages
    }
    func sendMessage(message: String) {
        supportViewModel.sendMessage(message: message) { [weak self] (response, error) in
            if error == nil, let sentMessage = response {
                self?.messages.insert(sentMessage, at: 0)
                self?.messageTextview.text = ""
                self?.messageTextview.becomeFirstResponder()
                self?.chatTableView.reloadData()
                self?.chatTableView.scrollToBottom()
                return
            }

            self?.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                  okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString(),
                                  okButtonCompletion: {
                                    self?.sendMessage(message: message)
                                  }, cancelButtonCompletion: {
                                    self?.dismiss(animated: true, completion: nil)
                                  })
        }
    }
}
extension SupportViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        self.activeField = nil
    }
}
extension SupportViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if messages == nil || messages.isEmpty {
            self.chatTableView.removeNoDataView()
            chatTableView.showNoDataView(titleDesc: "noConversation".getString(), viewFrame: CGRect(x: 20, y: 50, width: chatTableView.frame.size.width-40, height: 40), color: .black)
            return 0
        }
        self.chatTableView.removeNoDataView()
        return messages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[messages.count-indexPath.row-1]
        if message.subscriberId == subscriberId {
            if let cell = Bundle.main.loadNibNamed("SupportSenderCell", owner: self, options: nil)?[0] as? SupportSenderCell {
                cell.message.text = message.content
                cell.time.text = message.createdDateMilli?.toDate() ?? ""
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01, execute: {
                    cell.vwGradient.setGradientsBackground(.lightRed, .darkRed, 8)
                })
                return cell
            }
        } else {
            if let cell = Bundle.main.loadNibNamed("SupportRecieverCell", owner: self, options: nil)?[0] as? SupportRecieverCell {
                cell.message.text = message.content
                cell.time.text = message.createdDateMilli?.toDate() ?? ""
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension UITableView {
    func scrollToBottom() {
        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection: self.numberOfSections-1) - 1,
                section: self.numberOfSections - 1)
            if self.hasRowAtIndexPath(indexPath: indexPath) {
                self.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    func hasRowAtIndexPath(indexPath: IndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section) && indexPath.row >= 0
    }
}
extension SupportViewController: GrowingTextViewDelegate {
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}
