//
//  FAQ_ViewController.swift
//  cuddle
//
//  Created by MohammadReza on 10/20/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class FAQ_ViewController: UIViewController {

    // MARK: - views
    @IBOutlet private var backgroundView: UIView! {
        didSet {
            backgroundView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet private var bottomView: UIView! {
        didSet {
            bottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet private var insideBottomView: UIView! {
        didSet {
            insideBottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }
    }
    @IBOutlet private var supportButton: UIButton!
    @IBOutlet private var table: UITableView!

    // MARK: - init var
    struct TableData {
        var isExpanded: Bool
        var title: String
        var message: String
    }
    var dataForTable = [TableData]()
    let cellIdentifier = "faqCell"
    var faqData: PageQueAnsDto?
    var faqViewModel: FAQviewModel?

    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func viewDidAppear(_ animated: Bool) {
        supportButton.setGradientsBackground(.lightRed, .darkRed, 12)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "BalsamiqSans-Bold", size: 20) ?? UIFont(), NSAttributedString.Key.foregroundColor: UIColor(named: "darkestRed") as Any]
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }

    // MARK: - Actions
    @IBAction private func onBackButtonClick(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    @IBAction private func onTapSupportButton(_ sender: Any) {
        let viewController = AppStoryboard.Support.viewController(viewControllerClass: SupportViewController.self)
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    // MARK: - other methods
    func initView() {
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        table.register(UINib(nibName: "FAQTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        faqViewModel = FAQviewModel()
        getAllFAQdata()
    }

    func getAllFAQdata() {
        Utility.showHudLoading()
        faqViewModel?.getAllFAQ(completion: { [self] (data, error) in
            if error == nil {
                Utility.hideHudLoading()
                faqData = data
                for faq in faqData?.content ?? [] {
                    if let question = faq.question, let answer = faq.answer {
                        dataForTable.append(TableData(isExpanded: false, title: question, message: answer))
                    }
                }
                table.reloadData()
            } else {
                Utility.hideHudLoading()
                showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        })
    }

} // end of class

// MARK: - tableView delegates
extension FAQ_ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if dataForTable.isEmpty {
            self.table.removeNoDataView()
            table.showNoDataView(titleDesc: "noData".getString(), viewFrame: CGRect(x: 20, y: 50, width: table.frame.size.width-40, height: 60), color: .black)
        }
        table.removeNoDataView()
        return dataForTable.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !dataForTable[section].isExpanded {
            return 0
        }
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? FAQTableViewCell {
            cell.messageTextView.text = dataForTable[indexPath.section].message
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 70))
        view.backgroundColor = .clear
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnHeader(_:))))
        view.tag = section

        let title = UILabel()
        title.frame = CGRect.init(x: 60, y: 5, width: view.frame.width-10, height: view.frame.height-10)
        title.text = dataForTable[section].title
        title.textColor = .black
        title.font = UIFont(name: "BalsamiqSans-Regular", size: 16)
        view.addSubview(title)

        let image = UIImageView.init(frame: CGRect.init(x: 1, y: 10, width: 60, height: 60))
        image.image = dataForTable[section].isExpanded ? UIImage(named: "close") : UIImage(named: "open")
        view.addSubview(image)

        return view
    }
    @objc func didTapOnHeader(_ sender: UITapGestureRecognizer) {
        let section = sender.view?.tag
        let indexPaths = IndexPath(row: 0, section: section ?? 0)
        let isExpanded = dataForTable[section ?? 0].isExpanded
        dataForTable[section ?? 0].isExpanded = !isExpanded

        if isExpanded {
            table.deleteRows(at: [indexPaths], with: .fade)
            table.reloadData()
        } else {
            table.insertRows(at: [indexPaths], with: .fade)
            table.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70.0
    }

}
