//
//  SecOnBoardingViewController.swift
//  cuddle
//
//  Created by Shima on 9/29/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import Lottie

class SecOnBoardingViewController: UIViewController, PageObservation {
    @IBOutlet private var mainAnimationView: UIView!
    @IBOutlet private var startBtn: CustomUIButton!
    @IBOutlet private var backBtn: UIButton! {
        didSet {
            backBtn.setTitle(String.fontIconString(name: "back"), for: .normal)
        }
    }
    var parentPageViewController: TourSliderPageViewController!
    let animationView = AnimationView()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let animation = Animation.named("9squares-AlBoardman")
        animationView.frame = self.mainAnimationView.bounds
        animationView.animation = animation
        animationView.contentMode = .scaleAspectFit
        mainAnimationView.addSubview(animationView)
        animationView.loopMode = .loop
        animationView.play()
        startBtn.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    @IBAction fileprivate func startBtnAction(_ sender: Any) {
        self.navigationController?.pushViewController(AppStoryboard.SigninSignupStoryboard.initialViewController() ?? UIViewController(), animated: true)
    }
    @IBAction fileprivate func goToPrePageAction(_ sender: Any) {
        parentPageViewController.scrollToViewController(index: 0)
    }
    func getParentPageViewController(parentRef: TourSliderPageViewController) {
        parentPageViewController = parentRef
    }
}
