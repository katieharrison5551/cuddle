//
//  FirstOnBoardingViewController.swift
//  cuddle
//
//  Created by Shima on 9/29/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
import Lottie

class FirstOnBoardingViewController: UIViewController, PageObservation {
    @IBOutlet private var mainAnimationView: UIView!
    @IBOutlet private var forwardBtn: UIButton!{
        didSet {
        forwardBtn.setTitle(String.fontIconString(name: "forward"), for: .normal)
        }
    }
    var parentPageViewController: TourSliderPageViewController!
    let animationView = AnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    override func viewWillAppear(_ animated: Bool) {
          animationView.play()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let animation = Animation.named("LottieLogo1")
               animationView.frame = self.mainAnimationView.bounds
               animationView.animation = animation
               animationView.contentMode = .scaleAspectFit
               mainAnimationView.addSubview(animationView)
               animationView.loopMode = .loop
               animationView.play()
    }
    @IBAction fileprivate func gotoSecondPage(_ sender: Any) {
        parentPageViewController.scrollToViewController(index: 1)
    }
    @IBAction fileprivate func skipBtnAction(_ sender: Any) {
        self.navigationController?.pushViewController(AppStoryboard.SigninSignupStoryboard.initialViewController() ?? UIViewController(), animated: true)
    }
    func getParentPageViewController(parentRef: TourSliderPageViewController) {
        parentPageViewController = parentRef
    }
}
