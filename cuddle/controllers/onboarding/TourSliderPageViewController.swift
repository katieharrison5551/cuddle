//
//  TourSliderPageViewController.swift
//  cuddle
//
//  Created by Shima on 9/29/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
protocol PageObservation: class {
       func getParentPageViewController(parentRef: TourSliderPageViewController)
   }
class TourSliderPageViewController: UIPageViewController {

    weak var tutorialDelegate: TutorialPageViewControllerDelegate?
    var orderedViewControllers: [UIViewController] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        let child1ViewController =  UIStoryboard(name: "OnBoarding", bundle: nil) .
        instantiateViewController(withIdentifier: "FirstOnBoardingViewController")
        let child1WithParent = child1ViewController as! PageObservation
        child1WithParent.getParentPageViewController(parentRef: self)
        orderedViewControllers.append(child1ViewController)
        let child2ViewController =  UIStoryboard(name: "OnBoarding", bundle: nil) .
        instantiateViewController(withIdentifier: "SecOnBoardingViewController")
        let child2WithParent = child2ViewController as! PageObservation
        child2WithParent.getParentPageViewController(parentRef: self)
        orderedViewControllers.append(child2ViewController)
        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(viewController: initialViewController)
        }
        tutorialDelegate?.tourSliderPageViewController(tutorialPageViewController: self, didUpdatePageCount: orderedViewControllers.count)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            scrollToViewController(viewController: nextViewController)
        }
    }
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.firstIndex(of: firstViewController) {
            let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(viewController: nextViewController, direction: direction)
        }
    }
    private func newColoredViewController(color: String) -> UIViewController {
        return UIStoryboard(name: "OnBoarding", bundle: nil) .
            instantiateViewController(withIdentifier: "\(color)ViewController")
    }
    private func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewController.NavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (_) -> Void in
                            self.notifyTutorialDelegateOfNewIndex()
        })
    }
    private func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.firstIndex(of: firstViewController) {
            tutorialDelegate?.tourSliderPageViewController(tutorialPageViewController: self, didUpdatePageIndex: index)
        }
    }
}
// MARK: UIPageViewControllerDataSource
extension TourSliderPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else {
            return nil
        }
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        return orderedViewControllers[previousIndex]
    }
        func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController)
            else {
                return nil
        }
        let nextIndex = viewControllerIndex + 1
        guard nextIndex < orderedViewControllers.count else {
            return nil
        }
        guard orderedViewControllers.count > nextIndex else {
            return nil
        }
        return orderedViewControllers[nextIndex]
    }
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
    }
}
extension TourSliderPageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        notifyTutorialDelegateOfNewIndex()
    }
}
protocol TutorialPageViewControllerDelegate: class {

    func tourSliderPageViewController(tutorialPageViewController: TourSliderPageViewController,
                                      didUpdatePageCount count: Int)
    func tourSliderPageViewController(tutorialPageViewController: TourSliderPageViewController,
                                      didUpdatePageIndex index: Int)
}
