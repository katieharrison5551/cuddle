//
//  TourSliderViewController.swift
//  cuddle
//
//  Created by Shima on 9/29/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//
import UIKit

class TourSliderViewController: UIViewController {
    @IBOutlet private var containerView: UIView!
    var tutorialPageViewController: TourSliderPageViewController? {
        didSet {
            tutorialPageViewController?.tutorialDelegate = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? TourSliderPageViewController {
            self.tutorialPageViewController = tutorialPageViewController
        }
    }
}
extension TourSliderViewController: TutorialPageViewControllerDelegate {
    func tourSliderPageViewController(tutorialPageViewController: TourSliderPageViewController, didUpdatePageCount count: Int) {
    }

    func tourSliderPageViewController(tutorialPageViewController: TourSliderPageViewController, didUpdatePageIndex index: Int) {
    }
}
