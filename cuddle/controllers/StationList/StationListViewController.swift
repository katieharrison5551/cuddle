//
//  StationListViewController.swift
//  cuddle
//
//  Created by MohammadReza on 9/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class StationListViewController: UIViewController {
    @IBOutlet private var mTableView: UITableView!
    @IBOutlet private var backButton: UIBarButtonItem!
    var stationListViewModel = StationListViewModel()
    var contentID : String = ""
    var stations = [GetContentDto]()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationView()
        mTableView.register(UINib(nibName: "StationListCell", bundle: nil), forCellReuseIdentifier: "mycell")
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        getStatinList()
    }
    func setupNavigationView() {
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }
    @IBAction fileprivate func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "BalsamiqSans-Bold", size: 20) ?? UIFont(), NSAttributedString.Key.foregroundColor: UIColor(named: "darkestRed")]
    }
    func getStatinList() {
        Utility.showHudLoading()
        stationListViewModel.getStationList(contentId: self.contentID, byChildren: true) { (stationList, error) in
            Utility.hideHudLoading()
            guard let stations = stationList else {
                self.showErrorDialog(title: "error".getString(), message: error?.message ?? "server-unreachable".getString(),
                                     okButtonTitle: "retry".getString(), cancelButtonTitle: "cancel".getString()) {
                    self.getStatinList()
                } cancelButtonCompletion: {
                    self.navigationController?.popViewController(animated: true)
                }
                return
            }
            self.stations = stations.children ?? [GetContentDto]()
            self.mTableView.reloadData()
        }
    }
}
extension StationListViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if !stations.isEmpty {
            mTableView.removeNoDataView()
            return stations.count
        }
        self.mTableView.removeNoDataView()
        mTableView.showNoDataView(titleDesc: "noStation".getString(), viewFrame: CGRect(x: 20, y: 50, width: mTableView.frame.size.width-40, height: 60), color: .black)
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as? StationListCell {
            cell.delegate = self
            cell.stationname.text = stations[indexPath.section].title?.fields[0]
            cell.stationImage.sd_setImage(with: URL.init(string: (stations[indexPath.section].banner?.fields[0]) ?? ""), placeholderImage: UIImage(named: "TopHeader"))
            cell.stationAddress.text = stations[indexPath.section].summary?.fields[0]
        return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}

extension StationListViewController: StationDetail {
    func getStationDetail(cell: StationListCell) {
//        let indexPath = mTableView.indexPath(for: cell)
//        let nextViewController = AppStoryboard.BottomSheet.viewController(viewControllerClass: StationDetailViewController.self)
//        nextViewController.stationInfo = stations[indexPath!.section]
//        nextViewController.modalPresentationStyle = .overCurrentContext
//        present(nextViewController, animated: true, completion: nil)
    }
}
