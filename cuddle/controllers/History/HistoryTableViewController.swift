//
//  HistoryTableViewController.swift
//  cuddle
//
//  Created by MohammadReza on 9/7/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class HistoryTableViewController: UITableViewController {
    @IBOutlet var mainTable: UITableView!
    var rideHistoryViewModel: RideHistoryViewModel!
    var pageCount: Int = 0
    var allRideHistory: [CuddleTransactionDto]?
    override func viewDidLoad() {
        super.viewDidLoad()
        rideHistoryViewModel = RideHistoryViewModel()
        self.tableView.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "historyCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        setupNavigationBar()
        getAllRideHistory()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    fileprivate func setupNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: CustomFont.fontBold, size: 20), NSAttributedString.Key.foregroundColor : UIColor(named: "darkRed")]
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.title = "History".getString()
        let backButton = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backBtnAction))
        backButton.title = String.fontIconString(name: "back")
        let backButtonAttributes = [NSAttributedString.Key.font: UIFont(name: "icomoon", size: 20) ?? UIFont(), NSAttributedString.Key.foregroundColor: UIColor(named: "darkRed")]
        backButton.setTitleTextAttributes(backButtonAttributes, for: .normal)
        backButton.setTitleTextAttributes(backButtonAttributes, for: .selected)
        self.navigationItem.leftBarButtonItem = backButton
    }
    @objc fileprivate func backBtnAction() {
        self.navigationController?.popViewController(animated: true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    // MARK: - Table view data source
    fileprivate func getAllRideHistory() {
        Utility.showHudLoading()
        rideHistoryViewModel.getAllCuddleRideHistory(pageNumber: "\(pageCount)", pageSize: "500", sort: Sort_getAllCuddleTransactions.desc, sortKey: ["createdDate"]) { [weak self] (data, error) in
            Utility.hideHudLoading()
            if data != nil {
            if !(data?.content?.isEmpty ?? false) {
                if let response = data?.content {
                    self?.allRideHistory = response
                    self?.mainTable.reloadData()
                            return
                }
            }
            } else {
                self?.showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let items = allRideHistory, !items.isEmpty  else {
            self.mainTable.removeNoDataView()
            mainTable.showNoDataView(titleDesc: "noHistory".getString(), viewFrame: CGRect(x: 20, y: 50, width: mainTable.frame.size.width-40, height: 80), color: .black)
            return 0
        }
        self.mainTable.removeNoDataView()
        return items.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let items = allRideHistory, !items.isEmpty  else {
            return UITableViewCell()
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell", for: indexPath) as? HistoryTableViewCell {
            cell.setupCell(historyItems: items[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 340.0
    }

}
