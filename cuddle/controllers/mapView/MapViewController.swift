//
//  FirstViewController.swift
//  AutoLayoutTest
//
//  Created by MohammadReza on 8/4/20.
//  Copyright © 2020 MohammadReza. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import IQKeyboardManagerSwift
import Instructions

class MapViewController: BaseViewController {
    static let MapIntroKey = "MapView"

    @IBOutlet private var lblLogo: UILabel!
    @IBOutlet private var vwUnlock: UIView!
    @IBOutlet private var btnUnlock: UIButton!
    @IBOutlet private var btnMenu: MenuButton!
    @IBOutlet private var btnMyLocation: CustomIconButton!
    @IBOutlet private var btnRefresh: CustomIconButton!
    @IBOutlet private var vwNotificationHandle: UIView!
    @IBOutlet private var vwNotificationHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var mapView: GMSMapView!
    @IBOutlet private var searchTxtfld: UITextField!
    @IBOutlet private var vwSearch: UIView!
    var zoomLevel: Float = 14.0
    var locationManagerViewModel: LocationManagerViewModel!
    var geoServiceViewModel: GeoServiceViewModel!
    var searchMallViewModel: SearchMallViewModel!
    var allMalls: [GetContentLocationDto]!
    var distance: CGFloat = 200.0
    var isLocUpdated: Bool = false
    fileprivate var keyword: String?
    fileprivate var zoomStep: Float = 70
    fileprivate var isSearchMall = false

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        title = "Home"
        initView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".getString()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .default
    }
    fileprivate func initView() {
        locationManagerViewModel = LocationManagerViewModel(updateMapDelegate: self)
        geoServiceViewModel = GeoServiceViewModel()
        searchMallViewModel = SearchMallViewModel()
        btnMenu.setOnClick(onClick: #selector(SSASideMenu.presentLeftMenuViewController))
        btnMyLocation.setOnClick(onClick: #selector(onFindMyLocationClick(_:)))
        btnRefresh.setOnClick(onClick: #selector(onRefreshButtonClick(_:)))
        btnMyLocation.setFontTitle(title: "mylocation", size: 24)
        btnRefresh.setFontTitle(title: "refresh", size: 24)
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Search".getString()
        searchTxtfld.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(searchBtnAction))
        initNotificationSwipe()
        initMap()
        getNearByMalls(location: locationManagerViewModel.getLocation())

        initIntro()
    }

    fileprivate func initIntro() {
        if UserDefaults.standard.bool(forKey: MapViewController.MapIntroKey) {
            return
        }

        UserDefaults.standard.setValue(true, forKey: MapViewController.MapIntroKey)

        introText = []
//        introText.append("intro-main".getString())
        introText.append("intro-search".getString())
        introText.append("intro-mylocation".getString())
        introText.append("intro-refresh".getString())
        introText.append("intro-scan".getString())

        introView = []
//        introView.append(self.view)
        introView.append(vwSearch)
        introView.append(btnMyLocation)
        introView.append(btnRefresh)
        introView.append(btnUnlock)

        isDisplayOverlay = []
//        isDisplayOverlay.append(true)
        isDisplayOverlay.append(false)
        isDisplayOverlay.append(false)
        isDisplayOverlay.append(false)
        isDisplayOverlay.append(false)

        introCount = 4

        showIntro()
    }

    @objc fileprivate func searchBtnAction() {
        searchMall()
    }

    fileprivate func searchMall() {
        isSearchMall = true
        guard let serachKeyword = searchTxtfld.text , !serachKeyword.isEmpty else {
            return
        }
        keyword = serachKeyword
        distance = 3000
        Utility.showHudLoading()
        getNearByMalls(location: locationManagerViewModel.getLocation())
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnUnlock.setGradientsBackground(.lightRed, .darkRed, 8)
    }
    fileprivate func initNotificationSwipe() {
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        upSwipe.direction = .up
        downSwipe.direction = .down
        vwNotificationHandle.addGestureRecognizer(upSwipe)
        vwNotificationHandle.addGestureRecognizer(downSwipe)
    }
    fileprivate func getNearByMalls(location: CLLocation) {
        distance = CGFloat((18 - mapView.camera.zoom) * zoomStep)
        var cameraMoved = false
        geoServiceViewModel.getAllMallsList(byChildren: "true", distance: "\(distance)", lat: "\(location.coordinate.latitude)", lng: "\(location.coordinate.longitude)", tagTitle: "", type: "mall", keyword: keyword) { [weak self] (data, error) in
            Utility.hideHudLoading()
            if error == nil {
                self?.mapView.clear()
                if let allData = data {
                    self?.allMalls = allData
                    for (index, mall) in allData.enumerated() {
                        DispatchQueue.main.async {
                            let marker = CustomMarker(labelText: "\(mall.children?.count ?? 0)")
                            marker.position = CLLocationCoordinate2D(latitude: mall.lat ?? 0.0, longitude: mall.lng ?? 0.0)
                            marker.map = self?.mapView
                            marker.userData = index

                            if !cameraMoved && self?.isSearchMall ?? false {
                                self?.isSearchMall = false
                                self?.moveMapCamera(to: CLLocation(latitude: mall.lat ?? 0.0, longitude: mall.lng ?? 0.0))
                                cameraMoved = true
                            }
                        }
                    }
                }
            } else {
                self?.showErrorDialog(message: error?.message ?? "server-unreachable".getString(), okButtonTitle: "close".getString(), cancelButtonTitle: nil)
            }
        }
    }
    fileprivate func initMap() {
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        self.onFindMyLocationClick(nil)
        // self.updateMap(location: CLLocation(latitude: -50.0, longitude: 102))
    }
    @objc func handleSwipes(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == .up {
            UIView.animate(withDuration: 0.5) {
                self.vwNotificationHeightConstraint.constant = 40
                self.view.layoutIfNeeded()
            }
        }
        if sender.direction == .down {
            UIView.animate(withDuration: 0.5) {
                self.vwNotificationHeightConstraint.constant = 100
                self.view.layoutIfNeeded()
            }
        }
    }
    @objc fileprivate func onFindMyLocationClick(_ sender: UIButton?) {
        let location = locationManagerViewModel.getLocation()
        moveMapCamera(to: location)
    }

    fileprivate func moveMapCamera(to position: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: position.coordinate.latitude,
                                              longitude: position.coordinate.longitude,
                                              zoom: zoomLevel)
        mapView.animate(to: camera)
    }

    @objc fileprivate func onRefreshButtonClick(_ sender: Any) {
        let mapCenter: CLLocation =  CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        Utility.showHudLoading()
        getNearByMalls(location: mapCenter)
    }
    @IBAction fileprivate func unlockBtnAction(_ sender: Any) {
        self.navigationController?.pushViewController(AppStoryboard.ScanLock.viewController(viewControllerClass: ScanLockViewController.self), animated: true)
    }
    @IBAction fileprivate func onSearchButtonClick(_ sender: Any) {
        self.view.endEditing(true)
        searchMall()
    }
}
extension MapViewController: UpdateMapLocation {
    func updateMap(location: CLLocation) {
        if !isLocUpdated {
            let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                                  longitude: location.coordinate.longitude,
                                                  zoom: zoomLevel)
            mapView.animate(to: camera)

            isLocUpdated = true
            getNearByMalls(location: location)
        }
    }
}
extension MapViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let mallIndex = marker.userData as? Int {
            let mallDetail = AppStoryboard.MallDetails.viewController(viewControllerClass: MallDetailsViewController.self)
            mallDetail.mallId = self.allMalls[mallIndex]._id
            if let currentLoc = locationManagerViewModel.currentLocation {
                mallDetail.currentLocation = currentLoc.coordinate
            }
            mallDetail.destinationLocaton = marker.position
            mallDetail.mallCount = self.allMalls[mallIndex].children?.count
            let navController = UINavigationController(rootViewController: mallDetail)
            navController.modalPresentationStyle = .overCurrentContext
            present(navController, animated: true, completion: nil)
            return true
        }
        return false
    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print(position.target)
        getNearByMalls(location: CLLocation(latitude: position.target.latitude, longitude: position.target.longitude))
    }
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.view.endEditing(true)
     }
}
