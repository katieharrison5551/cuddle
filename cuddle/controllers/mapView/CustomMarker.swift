//
//  CustomMarker.swift
//  cuddle
//
//  Created by Shima on 10/7/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class CustomMarker: GMSMarker {
    var label: UILabel!
    init(labelText: String) {
        super.init()
        let iconView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 40, height: 40)))
        iconView.backgroundColor = UIColor(named: "darkestRed")
        iconView.layer.masksToBounds = true
        iconView.layer.borderColor = UIColor(named: "cdarkGray")?.cgColor
        iconView.layer.borderWidth = 5.0
        iconView.layer.cornerRadius = iconView.frame.size.width / 2
        label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: iconView.bounds.width, height: iconView.bounds.width)))
        label.text = labelText
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont(name: CustomFont.fontBold, size: 16.0)
        iconView.addSubview(label)
        self.iconView = iconView
    }
}
