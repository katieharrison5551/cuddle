//
//  LockCommandEnum.swift
//  cuddle
//
//  Created by Saeed on 10/20/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

enum LockCommandEnum {
    case searching
    case getToken
    case unlock
    case queryLock
    case queryBattery
}
