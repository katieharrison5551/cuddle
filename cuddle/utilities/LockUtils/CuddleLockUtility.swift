//
//  CuddleLockUtility.swift
//  cuddle
//
//  Created by Saeed on 10/4/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

class CuddleLockUtility {
    public static let defaultPassword = "0,0,0,0,0,0"
    public static let defaultPasswordOld = "1,2,3,4,5,6"
    public static let shared = CuddleLockUtility()
    
    fileprivate  let defaultKey: [UInt8] = [32, 87, 47, 82, 54, 75, 63, 71, 48, 80, 65, 88, 17, 99, 45, 43]
    
    func addCrcAndEnd(byteArrayOutputStream: Array<UInt8>) -> Array<UInt8> {
        var bArr = byteArrayOutputStream
        var randomSize = 16 - byteArrayOutputStream.count
        while randomSize > 0 {
            bArr.append(5)
            randomSize -= 1
        }
        
        return encrypt(bArr: bArr)
    }
    
    fileprivate func encrypt(bArr: Array<UInt8>) -> Array<UInt8> {
        do {
            let encData = NSData(bytes: bArr, length: bArr.count).aes128Encrypt(withKey: String(bytes: defaultKey, encoding: String.Encoding.ascii))
            return encData!.bytes
        } catch {
            return Array<UInt8>()
        }
    }

    func decrypt(bArr: Array<UInt8>) -> Array<UInt8>{
        do {
            let decData = NSData(bytes: bArr, length: bArr.count).aes128Decrypt(withKey: String(bytes: defaultKey, encoding: String.Encoding.ascii))
            return decData!.bytes
        } catch {
            return Array<UInt8>()
        }
    }
}
