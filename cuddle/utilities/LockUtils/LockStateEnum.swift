//
//  LockStateEnum.swift
//  cuddle
//
//  Created by Saeed on 10/4/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

enum LockStateEnum {
    case initialized
    case searching
    case connecting
    case unlocking
    case queryLockState
    case queryBattery
}
