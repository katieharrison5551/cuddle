//
//  CuddleLock.swift
//  cuddle
//
//  Created by Saeed on 10/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

typealias LockCommandCompletion = ((Bool, String) -> Void)

protocol LockResponseCommandProtocol {
    func isBluetoothOn(isOn: Bool)

    func isLockNearby(isSuccess: Bool, error: String)

    func isUnlocked(isSuccess: Bool, error: String)

    func isInLockState(isLocked: Bool, error: String)

    func lockBatteryReport(isSuccess: Bool, battery: Int, error: String)
}

protocol LockCommandProtocol {
    func unlock(password: String)

    func queryLockState(password: String)

    func findDevice(password: String)

    func query‌BatteryState(password: String)

    func disconnect()
}

class CuddleLock: NSObject {
    static let StrollerNotFoundMessage = "We can not found stroller around you, please scan again or scan another stroller"

    var lockId: String!
    var password: String!
    var passArray: [String]!

    fileprivate var lockCommandDelegate: LockResponseCommandProtocol!
    fileprivate let getTokenCommand: [UInt8] = [6, 1, 1, 1]
    fileprivate let getTokenCommandOld: [UInt8] = [6, 1, 6]
    fileprivate let unlockCommand: [UInt8] = [5, 1, 6]
    fileprivate let unlockCommandOld: [UInt8] = [5, 1, 1, 1]
    fileprivate let queryBatteryCommand: [UInt8] = [2, 1, 1, 1]
    fileprivate let queryLockStateCommand: [UInt8] = [5, 0xe, 1, 1]
    fileprivate let serviceCBUUID = CBUUID(string: "0000fee7-0000-1000-8000-00805f9b34fb")
    fileprivate let readCharacteristicUUID = "000036F6-0000-1000-8000-00805f9b34fb"
    fileprivate let writeCharacteristicUUID = "000036F5-0000-1000-8000-00805f9b34fb"
    fileprivate var centralManager: CBCentralManager?
    fileprivate var selectedDevice: CBPeripheral!
    fileprivate var writeCharacteristic: CBCharacteristic!
    fileprivate var token: [UInt8]!
    fileprivate var state: LockStateEnum! = LockStateEnum.initialized
    fileprivate var commandType: LockCommandEnum! = LockCommandEnum.getToken

    fileprivate var availableLock: [LockModel]!
    var useOldSDK = false

    init(commandDelegate: LockResponseCommandProtocol) {
        super.init()
        availableLock = []
        lockCommandDelegate = commandDelegate
        centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
    }

    fileprivate func connectToDevice(device: CBPeripheral) {
        selectedDevice = device
        selectedDevice.delegate = self
        centralManager?.connect(selectedDevice, options: [CBCentralManagerOptionShowPowerAlertKey: true])
    }

    fileprivate func getToken() -> Data {
        state = LockStateEnum.connecting
        var output: Array<UInt8> = Array()
        output.append(contentsOf: getTokenCommand)

        let encryptedData = CuddleLockUtility.shared.addCrcAndEnd(byteArrayOutputStream: output)
        return Data(bytes: encryptedData, count: encryptedData.count)
    }

    fileprivate func getTokenOld() -> Data {
        state = LockStateEnum.connecting
        var output: Array<UInt8> = Array()
        output.append(contentsOf: getTokenCommandOld)

        for pass in passArray {
            output.append(contentsOf: pass.bytes)
        }

        let encryptedData = CuddleLockUtility.shared.addCrcAndEnd(byteArrayOutputStream: output)
        return Data(bytes: encryptedData, count: encryptedData.count)
    }

    fileprivate func sendCommand() {
        if useOldSDK {
            sendCommandOld()
        } else {
            if commandType == LockCommandEnum.unlock {
                sendUnlockCommand()
            } else if commandType == LockCommandEnum.queryLock {
                sendQueryLockCommand()
            } else if commandType == LockCommandEnum.queryBattery {
                sendQueryBatteryCommand()
            }
        }
    }

    fileprivate func sendCommandOld() {
        if commandType == LockCommandEnum.unlock {
            sendUnlockCommandOld()
        } else if commandType == LockCommandEnum.queryLock {
            sendQueryLockCommand()
        } else if commandType == LockCommandEnum.queryBattery {
            sendQueryBatteryCommand()
        }
    }

    fileprivate func sendUnlockCommand() {
        state = LockStateEnum.unlocking
        selectedDevice.writeValue(unlock(), for: writeCharacteristic, type: .withResponse)
    }

    fileprivate func sendUnlockCommandOld() {
        state = LockStateEnum.unlocking
        selectedDevice.writeValue(unlockOld(), for: writeCharacteristic, type: .withResponse)
    }

    fileprivate func sendQueryLockCommand() {
        state = LockStateEnum.queryLockState
        selectedDevice.writeValue(queryLockState(), for: writeCharacteristic, type: .withResponse)
    }

    fileprivate func sendQueryBatteryCommand() {
        state = LockStateEnum.queryBattery
        selectedDevice.writeValue(queryBattery(), for: writeCharacteristic, type: .withResponse)
    }

    fileprivate func unlock() -> Data {
        var output: Array<UInt8> = Array()
        output.append(contentsOf: unlockCommand)
        for pass in passArray {
            output.append(contentsOf: pass.bytes)
        }
        output.append(token[3])
        output.append(token[4])
        output.append(token[5])
        output.append(token[6])
        let encData = CuddleLockUtility.shared.addCrcAndEnd(byteArrayOutputStream: output)
        return Data(bytes: encData, count: encData.count)
    }

    fileprivate func unlockOld() -> Data {
        var output: Array<UInt8> = Array()
        output.append(contentsOf: unlockCommandOld)

        output.append(token[3])
        output.append(token[4])
        output.append(token[5])
        output.append(token[6])
        let encData = CuddleLockUtility.shared.addCrcAndEnd(byteArrayOutputStream: output)
        return Data(bytes: encData, count: encData.count)
    }

    fileprivate func queryLockState() -> Data {
        var output: Array<UInt8> = Array()
        output.append(contentsOf: queryLockStateCommand)
        output.append(token[3])
        output.append(token[4])
        output.append(token[5])
        output.append(token[6])
        let encData = CuddleLockUtility.shared.addCrcAndEnd(byteArrayOutputStream: output)
        return Data(bytes: encData, count: encData.count)
    }

    fileprivate func queryBattery() -> Data {
        var output: Array<UInt8> = Array()
        output.append(contentsOf: queryBatteryCommand)
        output.append(token[3])
        output.append(token[4])
        output.append(token[5])
        output.append(token[6])
        let encData = CuddleLockUtility.shared.addCrcAndEnd(byteArrayOutputStream: output)
        return Data(bytes: encData, count: encData.count)
    }
}

extension CuddleLock: CBCentralManagerDelegate, CBPeripheralDelegate {

    func startScan() {
        for lock in availableLock {
         let manufactureData = lock.advertisementData["kCBAdvDataManufacturerData"].debugDescription
                var lockUUID = manufactureData.replacingOccurrences(of: "<", with: "")
                lockUUID = lockUUID.replacingOccurrences(of: ">", with: "")
                lockUUID = lockUUID.replacingOccurrences(of: " ", with: "")
            AnalyticManager.logEvent(name: "N\(manufactureData)", params: ["LockName": (lock.lock.name ?? "") as Any, "ManufacturerData": manufactureData as Any, "TargetLockId": lockId as Any])
                if lockUUID.uppercased().contains(lockId.uppercased()) {
                    connectToDevice(device: lock.lock)
                }
            }

        if selectedDevice == nil {
            lockCommandDelegate?.isLockNearby(isSuccess: false, error: CuddleLock.StrollerNotFoundMessage)
        }
    }
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == .poweredOn {
            lockCommandDelegate?.isBluetoothOn(isOn: true)
            self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
        } else {
            lockCommandDelegate?.isBluetoothOn(isOn: false)
        }
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
//        if peripheral.name?.contains("Buckle") ?? false {
            availableLock.append(LockModel(device: peripheral, data: advertisementData))
//        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        selectedDevice.discoverServices(nil)
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            peripheral.discoverCharacteristics([CBUUID(string: readCharacteristicUUID.uppercased()), CBUUID(string: writeCharacteristicUUID.uppercased())], for: service)
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            peripheral.setNotifyValue(true, for: characteristic)
            if characteristic.uuid.uuidString.contains("36F5") {
                if useOldSDK {
                    peripheral.writeValue(getTokenOld(), for: characteristic, type: .withResponse)
                } else {
                    peripheral.writeValue(getToken(), for: characteristic, type: .withResponse)
                }

                selectedDevice = peripheral
                writeCharacteristic = characteristic
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        let inputData = CuddleLockUtility.shared.decrypt(bArr: characteristic.value!.bytes)
        if inputData[0] == 6 {
            processToken(input: inputData)
        } else if inputData[0] == 5 {
            if inputData[1] == 2 {
                processUnlockCommand(input: inputData)
            } else if inputData[1] == 0xf {
                processLockStateCommand(input: inputData)
            }
        } else if inputData[0] == 2 {
            processBatteryCommand(input: inputData)
        }

        debugPrint("unlock response =\(inputData)")
    }

    fileprivate func processToken(input: [UInt8]) {
        if input[1] == 2 && input[2] == 8 {
            token = input
            lockCommandDelegate?.isLockNearby(isSuccess: true, error: "")
            AnalyticManager.logEvent(name: "D\(input)", params: ["barcode": lockId as Any, "InputData": "\(input)"])
        } else if input[1] == 1 && input[2] == 6 {

        } else if input[1] == 1 && input[2] == 1 && input[3] == 1 {

        } else {
            lockCommandDelegate?.isLockNearby(isSuccess: false, error: CuddleLock.StrollerNotFoundMessage)
        }
    }

    fileprivate func processUnlockCommand(input: [UInt8]) {
        if input[2] == 1 && input[3] == 0 {
            lockCommandDelegate.isUnlocked(isSuccess: true, error: "")
        } else if input[2] == 1 && input[3] == 1 {
            lockCommandDelegate.isUnlocked(isSuccess: false, error: "stroller-unlock-failed".getString())
        } else {
            // retry
        }
    }

    fileprivate func processLockStateCommand(input: [UInt8]) {
        if input[2] == 1 && input[3] == 1 {
            lockCommandDelegate.isInLockState(isLocked: true, error: "")
        } else if input[0] == 203 {

        } else if input[2] == 1 && input[3] == 0 {
            lockCommandDelegate.isInLockState(isLocked: false, error: "")
        }
    }

    fileprivate func processBatteryCommand(input: [UInt8]) {
        if input[1] == 2 && input[2] == 1 && input[3] != 0xff {
            lockCommandDelegate.lockBatteryReport(isSuccess: true, battery: Int(input[4]), error: "")
        } else if input[0] == 203 {

        } else if input[1] == 2 && input[2] == 1 && input[3] == 0xff {
            lockCommandDelegate.lockBatteryReport(isSuccess: false, battery: Int(input[4]), error: "")
        }
    }
}

extension CuddleLock: LockCommandProtocol {
    func findDevice(password: String) {
        self.password = password
        passArray = password.components(separatedBy: ",")
        state = LockStateEnum.searching
        commandType = LockCommandEnum.searching
        startScan()
    }

    func unlock(password: String) {
        self.password = password
        passArray = password.components(separatedBy: ",")
        state = LockStateEnum.unlocking
        commandType = LockCommandEnum.unlock
        if selectedDevice == nil || token == nil {
            startScan()
        } else {
            sendCommand()
        }
    }

    func queryLockState(password: String) {
        self.password = password
        passArray = password.components(separatedBy: ",")
        state = LockStateEnum.queryLockState
        commandType = LockCommandEnum.queryLock
        if selectedDevice == nil || token == nil {
            startScan()
        } else {
            sendCommand()
        }
    }

    func query‌BatteryState(password: String) {
        self.password = password
        passArray = password.components(separatedBy: ",")
        state = LockStateEnum.queryBattery
        commandType = LockCommandEnum.queryBattery
        if selectedDevice == nil || token == nil {
            startScan()
        } else {
            sendCommand()
        }
    }

    func disconnect() {
        centralManager?.cancelPeripheralConnection(selectedDevice)
    }
}
