//
//  AnalyticManager.swift
//  cuddle
//
//  Created by Saeed on 10/27/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class AnalyticManager {

    class func logEvent(name: String, params: [String: Any]) {
        Analytics.logEvent(name, parameters: params)
//        storeInDB(name: name, params: params)
    }

    class func storeInDB(name: String, params: [String: Any]) {
        let db = Firestore.firestore()
        var ref: DocumentReference? = nil
        ref = db.collection(UserInfoModel.shared.getUsername().isEmpty ? "debug":UserInfoModel.shared.getUsername()).addDocument(data: [
            "actionName": name,
            "params": params,
            "token": UserInfoModel.shared.getUserToken(),
            "date": Date().timeIntervalSince1970
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
}
