//
//  StationDetailViewController.swift
//  cuddle
//
//  Created by MohammadReza on 9/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class StationDetailViewController: UIViewController {
    // BottomSheet view
    @IBOutlet private var bottomSheetView: UIView!
    // Top view
    @IBOutlet private var touchView: UIView!
    @IBOutlet private var indicatorView: UIView!
    @IBOutlet private var mCollectionView: UICollectionView!
    @IBOutlet private var pageControl: UIPageControl!
    // Bottom view
    @IBOutlet private var stationName: UILabel!
    @IBOutlet private var stationAddress: UILabel!
    @IBOutlet private var stationWorkTime: UILabel!
    @IBOutlet private var remainingCapacity: UILabel!
    @IBOutlet private var remainingCapacityAmount: UILabel!
    @IBOutlet private var available: UILabel!
    @IBOutlet private var availableAmount: UILabel!
    @IBOutlet private var scanButtonIcon: UILabel! {
        didSet {
            scanButtonIcon.text = String.fontIconString(name: "lock")
        }
    }
    @IBOutlet private var scanButtonAction: UIView!
    @IBOutlet private var scanButton: UIView!
    var bottomSheetY: CGFloat?
    var stationInfo: GetContentDto?
    override func viewDidLoad() {
        super.viewDidLoad()
        initNotificationSwipe()
        mCollectionView.isPagingEnabled = true
        initViews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scanButton.setGradientsBackground(.lightRed, .darkRed, 12)
    }
    fileprivate func initNotificationSwipe() {
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        downSwipe.direction = .down
        touchView.addGestureRecognizer(downSwipe)
    }
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomSheetY = self.bottomSheetView.frame.origin.y
            self.bottomSheetView.frame.origin.y = self.view.frame.height
        }, completion: { _ in
            self.dismiss(animated: true, completion: nil)
        })
    }
    func initViews() {
        stationName.text = stationInfo?.title?.fields[0]
        stationAddress.text = stationInfo?.summary?.fields[0]
        scanButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnScanButton(_:))))
    }
    @objc fileprivate func didTapOnScanButton(_ sender: UITapGestureRecognizer) {
        let viewController = AppStoryboard.ScanLock.viewController(viewControllerClass: ScanLockViewController.self)
        let navController = UINavigationController(rootViewController: viewController)
        navController.setNavigationBarHidden(false, animated: false)
        navController.navigationBar.barTintColor = UIColor(named: "darkestRed")
        navController.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : UIFont(name: "BalsamiqSans-Bold", size: 20), NSAttributedString.Key.foregroundColor : UIColor.white]
        navController.modalPresentationStyle = .fullScreen
        present(navController, animated: true, completion: nil)
    }

}
extension StationDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if stationInfo == nil {
            pageControl.numberOfPages = 0
            return 0
        }
        pageControl.numberOfPages = (stationInfo?.banner?.fields.count)!
        return (stationInfo?.banner?.fields.count)!
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bottomSheetCollectionCell", for: indexPath) as? StationDetailCell {
            cell.stationImage.sd_setImage(with: URL.init(string: (stationInfo?.banner?.fields[0])!), completed: nil)            
        return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.section
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width-24, height: collectionView.frame.height)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
