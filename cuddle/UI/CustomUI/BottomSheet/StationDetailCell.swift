//
//  stationDetailCell.swift
//  cuddle
//
//  Created by MohammadReza on 9/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class StationDetailCell: UICollectionViewCell {
    //swiftlint:disable all
    @IBOutlet var stationImage: UIImageView!
}
