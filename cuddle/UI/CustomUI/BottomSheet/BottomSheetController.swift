//
//  BottomSheetController.swift
//  cuddle
//
//  Created by MohammadReza on 9/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    enum  BottomSheetType {
        case mall
        case station
    }
    func showBottomSheet(type: BottomSheetType) {
        switch type {
        case .mall:
            present(AppStoryboard.MallDetails.initialViewController() ?? UIViewController(), animated: true, completion: nil)
        case .station:
            present(AppStoryboard.BottomSheet.initialViewController() ?? UIViewController(), animated: true, completion: nil)
        }
    }
}
