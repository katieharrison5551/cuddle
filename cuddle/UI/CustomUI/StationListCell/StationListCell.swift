//
//  StationListCell.swift
//  cuddle
//
//  Created by MohammadReza on 9/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

protocol StationDetail {
    func getStationDetail(cell: StationListCell)
}

class StationListCell: UITableViewCell {
    //swiftlint:disable all
    var delegate : StationDetail?
    @IBOutlet var goToDetails: UIButton! {
        didSet {
            goToDetails.setTitle(String.fontIconString(name: "forward"), for: .normal)
            goToDetails.setTitle(String.fontIconString(name: "forward"), for: .selected)            
        }
    }
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stationImage: UIImageView!
    @IBOutlet weak var stationname: UILabel!
    @IBOutlet weak var stationAddress: UILabel!
    //swiftlint:enable all
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        setupView()
    }

    @IBAction fileprivate func goToNextPage(_ sender: Any) {
        delegate?.getStationDetail(cell: self)
    }

    func setupView() {
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnView(_:))))
    }

    @objc func didTapOnView(_ sender: UITapGestureRecognizer) {
        delegate?.getStationDetail(cell: self)
    }

}
