//
//  LogOutDialogViewController.swift
//  cuddle
//
//  Created by Saeed on 11/21/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

protocol LogoutActionDelegate {
    func onYesClick()
    func onNoClick()
}

class LogOutDialogViewController: UIViewController {

    @IBOutlet private var btnYes: UIButton!
    @IBOutlet private var btnNo: UIButton!
    @IBOutlet private var vwParent: UIView!

    var logoutDelegate: LogoutActionDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        vwParent.alpha = 0
    }

    override func viewDidAppear(_ animated: Bool) {
        btnNo.setGradientsBackground(.lightRed, .darkRed, 8)
        UIView.animate(withDuration: 0.1) {
            self.vwParent.alpha = 1
        }
    }

    @IBAction func onYesButtonClick(_ sender: Any) {
        logoutDelegate?.onYesClick()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onNoButtonClick(_ sender: Any) {
        logoutDelegate?.onNoClick()
        dismiss(animated: true, completion: nil)
    }
}
