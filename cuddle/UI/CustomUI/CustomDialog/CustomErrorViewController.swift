//
//  CustomErrorViewController.swift
//  cuddle
//
//  Created by Saeed on 11/23/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

typealias ButtonDelegate = (() -> Void)

class CustomErrorViewController: UIViewController {

    @IBOutlet private var lblTitle: UILabel!
    @IBOutlet private var lblDescription: UILabel!
    @IBOutlet private var btnOk: UIButton!
    @IBOutlet private var btnCancel: UIButton!

    fileprivate var okButtonDelegate: ButtonDelegate!
    fileprivate var cancelButtonDelegate: ButtonDelegate!

    fileprivate var dialogTitle: String!
    fileprivate var dialogDescription: String!
    fileprivate var okTitle: String!
    fileprivate var cancelTitle: String!

    func setupAlert(title: String, desc: String, okButtonTitle: String?, cancelButtonTitle: String?, okButtonCompletion: ButtonDelegate? = nil, cancelButtonCompletion: ButtonDelegate? = nil) {
        dialogTitle = title
        dialogDescription = desc
        okButtonDelegate = okButtonCompletion
        cancelButtonDelegate = cancelButtonCompletion

        okTitle = okButtonTitle
        cancelTitle = cancelButtonTitle


    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }

    fileprivate func initView() {
        lblTitle.text = dialogTitle
        lblDescription.text = dialogDescription

        btnOk.isHidden = true
        btnCancel.isHidden = true

        if okTitle != nil && !okTitle.isEmpty {
            btnOk.setTitle(okTitle, for: .normal)
            btnOk.isHidden = false
        }

        if cancelTitle != nil && !cancelTitle.isEmpty {
            btnCancel.setTitle(cancelTitle, for: .normal)
            btnCancel.isHidden = false
        }
    }

    @IBAction func onOkButtonClick(_ sender: Any) {
        okButtonDelegate?()
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onCancelButtonClick(_ sender: Any) {
        cancelButtonDelegate?()
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
