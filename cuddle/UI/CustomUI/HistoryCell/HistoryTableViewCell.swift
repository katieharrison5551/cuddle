//
//  HistoryTableViewCell.swift
//  cuddle
//
//  Created by MohammadReza on 9/7/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//
import UIKit
class HistoryTableViewCell: UITableViewCell {
    @IBOutlet private var dateLebel: UILabel!
    @IBOutlet private var timeLabel: UILabel!
    @IBOutlet private var mallName: UILabel!
    @IBOutlet private var transactionID: UILabel!
    @IBOutlet private var payedAmount: UILabel!
    @IBOutlet private var lblPaymentTime: UILabel!
    @IBOutlet private var lblStrollerCode: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func setupCell(historyItems: CuddleTransactionDto?) {
        if let item = historyItems {
            dateLebel.text = (item.rentCreatedDateMilli ?? 0).toDate()
            timeLabel.text = item.rentDuration?.calculateDuration()
            mallName.text = item.mallName
            transactionID.text = item.transactionId
            payedAmount.text = "".showPrice(price: item.amount ?? 0)
            lblStrollerCode.text = item.strollerAdditionalCode
            lblPaymentTime.text = (item.transactionCreatedDateMilli ?? 0).toDate()
        }
    }
}
