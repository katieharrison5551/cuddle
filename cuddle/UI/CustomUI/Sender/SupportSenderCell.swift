//
//  SupportSenderCell.swift
//  cuddle
//
//  Created by MohammadReza on 8/22/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
class SupportSenderCell: UITableViewCell {
    //swiftlint:disable all
    @IBOutlet var message: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var vwGradient: UIView!
    //swiftlint:enable all
    let gradientLayer = CAGradientLayer()
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
