//
//  MallPicCollectionViewCell.swift
//  cuddle
//
//  Created by MohammadReza on 9/2/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
class MallPicCollectionViewCell: UICollectionViewCell {
    //swiftlint:disable all
    @IBOutlet  var mallPicture: UIImageView!
    //swiftlint:enable all
}
