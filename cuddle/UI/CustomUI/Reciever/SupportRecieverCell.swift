//
//  SupportRecieverCell.swift
//  cuddle
//
//  Created by MohammadReza on 8/22/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit

class SupportRecieverCell: UITableViewCell {
    //swiftlint:disable all
    @IBOutlet var message: PaddingLabel!
    @IBOutlet var time: UILabel!
    //swiftlint:enable all
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        message.layer.masksToBounds = true
        message.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
