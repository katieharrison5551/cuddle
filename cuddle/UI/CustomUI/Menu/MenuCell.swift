//
//  MenuCell.swift
//  cuddle
//
//  Created by MohammadReza on 8/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import UIKit
class MenuCell: UITableViewCell {
    //swiftlint:disable all
    @IBOutlet var menuIcon: UIImageView!
    @IBOutlet var menuTitle: UILabel!
    @IBOutlet var menuSwitch: UISwitch!
    //swiftlint:enable all
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
