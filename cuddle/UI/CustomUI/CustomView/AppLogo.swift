//
//  AppLogo.swift
//  cuddle
//
//  Created by Saeed on 8/15/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class AppLogo: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initLogo()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initLogo()
    }
    fileprivate func initLogo() {
        self.font = Utility.getFontIcon(withSize: 45)
        self.text = String.fontIconString(name: "logo")
        self.textColor = UIColor(named: "darkestRed")
    }
}
