//
//  MenuButton.swift
//  cuddle
//
//  Created by Saeed on 8/15/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class MenuButton: CustomIconButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initMenuButton()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initMenuButton()
    }
    fileprivate func initMenuButton() {
        setFontTitle(title: "menu", size: 25)
        self.setTitleColor(UIColor(named: "lightRed"), for: .normal)
    }
}
