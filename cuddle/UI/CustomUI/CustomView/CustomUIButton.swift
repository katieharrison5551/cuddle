//
//  CustomButton.swift
//  cuddle
//
//  Created by Saeed on 8/17/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomUIButton: UIButton {
        @IBInspectable var fontSize: CGFloat {
        get {
            return 14.0
        } set {
            self.titleLabel?.font = UIFont(name: CustomFont.fontBold, size: newValue)
        }
    }
    @IBInspectable var isBackgroundGradient: Bool {
        get {
            return false
        } set {
            if newValue {
                self.setGradientsBackground(.lightRed, .darkRed, 12)
                self.setTitleColor(.white, for: .normal)
            } else {
                self.backgroundColor = .white
                self.cornerRadius = 12
                self.setTitleColor(.darkRed, for: .normal)
                self.borderColor = .lightRed
                self.borderWidth = 1.5
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    func setOnClick(onClick: Selector) {
        self.addTarget(onClick.self, action: onClick, for: .touchUpInside)
    }
}
