//
//  CustomTextfield.swift
//  cuddle
//
//  Created by Saeed on 8/17/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class CustomTextfield: UITextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    fileprivate func initTextfield() {
        self.font = Utility.getRegularFont(withSize: 12)
    }
}
