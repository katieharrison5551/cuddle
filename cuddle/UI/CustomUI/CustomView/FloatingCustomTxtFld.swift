//
//  FloatingCustomTxtFld.swift
//  cuddle
//
//  Created by Shima on 11/4/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//
 import SkyFloatingLabelTextField

import Foundation
class FloatingCustomTxtFld: SkyFloatingLabelTextField {

    private let leftPadding = CGFloat(8)
    private let rightPadding = CGFloat(6)

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {

        let rect = CGRect(
            x: leftPadding,
            y: titleHeight(),
            width: bounds.size.width - rightPadding,
            height: bounds.size.height - titleHeight() - selectedLineHeight
        )

        return rect

    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {

        let rect = CGRect(
            x: leftPadding,
            y: titleHeight(),
            width: bounds.size.width - rightPadding,
            height: bounds.size.height - titleHeight() - selectedLineHeight
        )

        return rect

    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {

        let rect = CGRect(
            x: leftPadding,
            y: titleHeight(),
            width: bounds.size.width - rightPadding,
            height: bounds.size.height - titleHeight() - selectedLineHeight
        )

        return rect

    }

    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {

        if editing {
            return CGRect(x: leftPadding, y: 5, width: bounds.size.width, height: titleHeight())
        }

        return CGRect(x: leftPadding, y: titleHeight(), width: bounds.size.width, height: titleHeight())
    }

}
