//
//  BackButton.swift
//  cuddle
//
//  Created by MohammadReza on 9/7/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class BackButton: UIBarButtonItem {
    override init() {
        super.init()
         createBackButton()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createBackButton()
    }
    func createBackButton() {
        self.title = String.fontIconString(name: "back")
        let backButtonAttributes = [NSAttributedString.Key.font: UIFont(name: CustomFont.FontIcon, size: 20) ?? UIFont(), NSAttributedString.Key.foregroundColor: tintColor]
        self.setTitleTextAttributes(backButtonAttributes, for: .normal)
        self.setTitleTextAttributes(backButtonAttributes, for: .selected)
    }
}
