//
//  CustomIconButton.swift
//  cuddle
//
//  Created by Saeed on 8/17/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class CustomIconButton: CustomUIButton {
    /// set button title with icomon
    /// @title: is key value of Cuddle.plsit
    func setFontTitle(title: String, size: CGFloat) {
        self.titleLabel?.font = Utility.getFontIcon(withSize: size)
        self.setTitle(String.fontIconString(name: title), for: .normal)
    }
}
