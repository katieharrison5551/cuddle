//
//  SearchButton.swift
//  cuddle
//
//  Created by Saeed on 8/15/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class SearchButton: CustomIconButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSearchButton()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initSearchButton()
    }
    fileprivate func initSearchButton() {
        setFontTitle(title: "search", size: 20)
        self.setTitleColor(UIColor.black, for: .normal)
    }
}
