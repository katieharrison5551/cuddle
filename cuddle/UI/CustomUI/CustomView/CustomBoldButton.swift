//
//  CustomBoldButton.swift
//  cuddle
//
//  Created by Saeed on 8/15/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class CustomBoldButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initBoldButton()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initBoldButton()
    }
    fileprivate func initBoldButton() {
        self.titleLabel?.font = Utility.getBoldFont(withSize: 14)
        self.setTitleColor(UIColor.white, for: .normal)
        self.backgroundColor = UIColor(named: "darkestRed")
    }
}
