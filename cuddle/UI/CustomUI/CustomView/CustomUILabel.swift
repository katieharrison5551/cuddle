//
//  CustomUILabel.swift
//  cuddle
//
//  Created by Saeed on 8/15/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

class CustomUILabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUILabel()
    }
    fileprivate func initUILabel() {
        self.font = Utility.getRegularFont(withSize: 14)
       // self.text = self.text?.uppercased()
        self.textColor = UIColor.black
    }
}
@IBDesignable class PaddingLabel: UILabel {
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    @IBInspectable
    var isRedGradient: Bool {
        get {
            return false
        } set {
            self.setRedGradient()
        }
    }
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
    override var bounds: CGRect {
        didSet {
            preferredMaxLayoutWidth = bounds.width - (leftInset + rightInset)
        }
    }
    /// set gradient backgrounf
    func setRedGradient() {
        self.setGradientsBackground(.lightRed, .darkRed, 8)
    }
}
