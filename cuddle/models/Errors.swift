//Created by Shima on 10/03/20.
//swiftlint:disable all
import Foundation
struct Errors: Codable {
	let field: String?
	let message: String?
	enum CodingKeys: String, CodingKey {
		case field = "field"
		case message = "message"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		field = try values.decodeIfPresent(String.self, forKey: .field)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}
}
