//
//  SigninModelResponse.swift
//  cuddle
//
//  Created by Saeed on 8/11/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

struct SigninModelResponse {
    var isSuccess: Bool
    var message: String
}

enum SigninModelResponseMessage: String {
    case notLogin = "Please sign in again"
    case successLogin = "Welcome"
    case failedLogin = "Something wrong happend"
}
