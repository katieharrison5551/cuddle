//
//  LockModel.swift
//  cuddle
//
//  Created by Saeed on 10/5/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import CoreBluetooth

class LockModel {
    var lock: CBPeripheral!
    var advertisementData: [String: Any]!

    init(device: CBPeripheral, data: [String: Any]) {
        self.lock = device
        self.advertisementData = data
    }
}
