//
//  RegisterModelResponse.swift
//  cuddle
//
//  Created by Saeed on 8/10/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

struct RegisterModelResponse {
    var isSuccess: Bool
    var message: String
}

enum RegisterModelResponseMessage: String {
    case invalidMobile = "Mobile number is not valid"
    case invalidEmail = "Email format is not valid"
    case successRegisteration = "Registration successfull"
    case failedRegisteration = "Registration failed"
}
