//
//  ActivateModelResponse.swift
//  cuddle
//
//  Created by Saeed on 8/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

struct ActivateModelResponse {
    var isSuccess: Bool
    var message: String
}

enum ActivateModelResponseMessage: String {
    case invalidMobile = "Mobile number is not valid"
    case invalidEmail = "Email format is not valid"
    case invalidVerificationCode = "Verification code is not valid"
    case successVerify = "Verify successfull"
    case failedVerify = "Verify failed"
}
