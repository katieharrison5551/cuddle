//
// Created by Saeed on 8/19/20.
//

import Foundation
import UIKit

enum InternalErrorCode: Int32 {
    case networkError = 10
}

enum ServerErrorCode: Int32 {
    case serverError = 500
    case preconditionRequired = 428
    case preconditionFailed = 412
}

struct ErrorMessageModel {
    var title: String
    var message: String
    var errors: [Errors]!
    var code: Int32!
}

enum ErrorMsgResp: String {
    case invalidUser = "Something went wrong! please try again"
    case notLogin = "Please sign in again"
}
