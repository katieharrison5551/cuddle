//Created by Shima on 10/03/20.
//swiftlint:disable all
import Foundation
struct ErrorModel: Codable {
	let errors: [Errors]?
	let message: String?
	let fingerPrint: String?
	enum CodingKeys: String, CodingKey {

		case errors = "errors"
		case message = "message"
		case fingerPrint = "fingerPrint"
	}
	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		errors = try values.decodeIfPresent([Errors].self, forKey: .errors)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		fingerPrint = try values.decodeIfPresent(String.self, forKey: .fingerPrint)
	}
}
