//
//  UserInfoModel.swift
//  cuddle
//
//  Created by Saeed on 10/3/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

protocol UserInfoProtocol {
    func setUserToken(accessToken: String?, accessTokenType: String?)
    func getUserToken() -> String?

    func setUsername(username: String)
    func getUsername() -> String
}

final public class UserInfoModel {
    static let UserTokenKey = "TokenId"
    static let UserTokenTypeKey = "TokenType"
    static let UserNameKey = "Username"
    public static let shared = UserInfoModel()
    private let concurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: .concurrent)
    private init() {
    }
}
extension UserInfoModel: UserInfoProtocol {
    public func setUserToken(accessToken: String?, accessTokenType: String?) {
        guard let token = accessToken, let tokenType = accessTokenType else {
            return
        }
        concurrentQueue.async( flags: .barrier ) {
            UserDefaults.standard.setValue(token, forKey: UserInfoModel.UserTokenKey)
            UserDefaults.standard.setValue(tokenType, forKey: UserInfoModel.UserTokenTypeKey)
        }
    }
    public func getUserToken() -> String? {
        var token: String?
        var tokenType: String?
        concurrentQueue.sync {
            token = UserDefaults.standard.string(forKey: UserInfoModel.UserTokenKey)
            tokenType = UserDefaults.standard.string(forKey: UserInfoModel.UserTokenTypeKey)
        }
        guard let accessToken = token, let accessTokenType = tokenType else {
            return nil
        }
        debugPrint("\(accessTokenType) \(accessToken)")
       // return "\(accessTokenType) \(accessToken)"
        return "Bearer \(accessToken)"
    }

    func setUsername(username: String) {
        concurrentQueue.async( flags: .barrier ) {
            UserDefaults.standard.setValue(username, forKey: UserInfoModel.UserNameKey)
        }
    }

    func getUsername() -> String {
        var username: String?
        concurrentQueue.sync {
            username = UserDefaults.standard.string(forKey: UserInfoModel.UserNameKey)
        }

        return username ?? ""
    }

    public func logoutUser() {
        concurrentQueue.async( flags: .barrier ) {
            UserDefaults.standard.removeObject(forKey: UserInfoModel.UserTokenKey)
            UserDefaults.standard.removeObject(forKey: UserInfoModel.UserTokenTypeKey)
        }
    }
}
