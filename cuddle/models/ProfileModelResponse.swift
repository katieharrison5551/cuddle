//
//  ProfileModel.swift
//  cuddle
//
//  Created by MohammadReza on 8/16/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import UIKit

struct ProfileModelResponse {
    var isSuccess: Bool
    var message: String
}
struct ProfileModel {
    var name: String
    var phone: String
    var email: String
    var profileImageURL: String
}
