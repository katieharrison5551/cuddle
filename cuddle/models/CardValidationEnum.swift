//
//  CardValidationEnum.swift
//  cuddle
//
//  Created by Shima on 9/30/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
enum CardValidationErrorEnum: Int32 {
    case cardNum = 11
    case expMonth = 12
    case expYear = 13
    case cvv2 = 14
    case name = 15
    case address = 16
    case zipcode = 17
    case country = 18
    case city = 19
    case state = 20
     case sheba = 21
}
enum CardValidationErrorMessageFieldEnum: String {
    case cardNum = "number"
    case expMonth = "expirationMonth"
    case expYear = "expirationYear"
    case cvv2 = "cvv2"
    case name = "fullName"
    case address = "address"
    case zipcode = "zipCode"
    case country = "country"
    case city = "city"
    case state = "state"
    case sheba = "sheba"
}
