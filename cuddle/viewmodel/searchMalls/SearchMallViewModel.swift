//
//  SearchMallViewModel.swift
//  cuddle
//
//  Created by Shima on 10/27/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias SerachMallCompletation = ((_ data: PageGetMallDto?,_ error: ErrorMessageModel?) -> Void)
protocol GetMallListByKeywordProtocol {
    func getMallListByKeyword(pageNumber: String?, pageSize: String?, sort: Sort_getMallList?, sortKey: [String]?, title: String?, completion: @escaping SerachMallCompletation)

}
class SearchMallViewModel: BaseViewModel {
    fileprivate var searchMallService: getMallListProtocol
    init(searchMallService: getMallListProtocol = CuddleMallControllerAPI()) {
        self.searchMallService = searchMallService
    }
}
extension SearchMallViewModel: GetMallListByKeywordProtocol {
    func getMallListByKeyword(pageNumber: String?, pageSize: String?, sort: Sort_getMallList?, sortKey: [String]?, title: String?, completion: @escaping SerachMallCompletation) {
        guard let searchTxt = title, !searchTxt.isEmpty, searchTxt.count > 2 else {
            completion(nil, ErrorMessageModel(title: "error".getString(), message: "searchKeyword".getString()))
            return
        }
        self.searchMallService.getMallList(pageNumber: pageNumber!, pageSize: pageSize!, sort: sort!, sortKey: sortKey!, title: title!) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
