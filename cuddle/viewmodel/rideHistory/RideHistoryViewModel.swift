//
//  RideHistoryViewModel.swift
//  cuddle
//
//  Created by Shima on 10/26/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias AllCuddleTransactionsCompletation = ((_ data: PageCuddleTransactionDto?,_ error: ErrorMessageModel?) -> Void)

protocol AllCuddleTransactionsProtocol {
    func getAllCuddleRideHistory(pageNumber: String?, pageSize: String?, sort: Sort_getAllCuddleTransactions?, sortKey: [String]?, completion: @escaping AllCuddleTransactionsCompletation)
}
class RideHistoryViewModel: BaseViewModel {
    fileprivate var historyService: getAllCuddleTransactionsProtocol
    init(historyService: getAllCuddleTransactionsProtocol = CuddleTransactionControllerAPI()) {
        self.historyService = historyService
    }
}
extension RideHistoryViewModel: AllCuddleTransactionsProtocol {
    func getAllCuddleRideHistory(pageNumber: String?, pageSize: String?, sort: Sort_getAllCuddleTransactions?, sortKey: [String]?, completion: @escaping AllCuddleTransactionsCompletation) {
        historyService.getAllCuddleTransactions(pageNumber: pageNumber ?? "", pageSize: pageSize ?? "" , sort: sort ?? Sort_getAllCuddleTransactions.asc , sortKey: sortKey ?? []) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: response.errorMessage))
                }
            }
        }
    }
}
