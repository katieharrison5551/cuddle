//
//  UpdateProfileViewModel.swift
//  cuddle
//
//  Created by MohammadReza on 10/18/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//
import Foundation

typealias UpdateProfileCompletion = ((SubscriberDto?, RegisterModelResponse) -> Void)
typealias UploadProfilePictureCompletation = ((_ data: UploadedFileResponseDto?,_ error: ErrorMessageModel?) -> Void)

protocol UpdateProfileUserProtocol {
    func updateProfileUser(name: String?, phone: String?, email: String?, gender: String?, imageURL: String?, completion : @escaping UpdateProfileCompletion)
}
protocol UploadProfilePictureProtocol {
    func uploadProfilePicture(file: Data, completion: @escaping UploadProfilePictureCompletation)
}
class UpdateProfileViewModel: BaseViewModel {
    fileprivate var updateProfileService: updateProfileUsingPATCH1Protocol
    fileprivate var uploadPicService: uploadProfilePictureProtocol

    init(updateProfileService: updateProfileUsingPATCH1Protocol = SubscriberControllerAPI(), uploadPicService: uploadProfilePictureProtocol = SubscriberControllerAPI()) {
        self.updateProfileService = updateProfileService
        self.uploadPicService = uploadPicService
    }
}
extension UpdateProfileViewModel: UploadProfilePictureProtocol {
    func uploadProfilePicture(file: Data, completion: @escaping UploadProfilePictureCompletation) {
        let uploadService = UploadProfilePicture()
        uploadService.uploadPicture(imageData: file) { (response) in
            if response != nil {
                do {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(UploadedFileResponseDto.self, from: response! as! Data)
                    completion(responseModel, nil)
                } catch {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: ""))
                }
                return
            }

            completion(nil, ErrorMessageModel(title: "error".getString(), message: ""))
        }
    }
}

extension UpdateProfileViewModel: UpdateProfileUserProtocol {
    func updateProfileUser(name: String?, phone: String?, email: String?, gender: String?, imageURL: String?, completion: @escaping UpdateProfileCompletion) {
        guard let fullName = name, !fullName.isEmpty else {
            completion(nil, RegisterModelResponse(isSuccess: false, message: "profile-empty-name".getString()))
            return
        }
        guard let mobileNumber = phone, !mobileNumber.isEmpty else {
            completion(nil, RegisterModelResponse(isSuccess: false, message: "profile-empty-mobile".getString()))
            return
        }

//        if !Utility().validatePhoneNumber(value: mobileNumber) {
//            completion(nil, RegisterModelResponse(isSuccess: false, message: "profile-incorrect-mobile".getString()))
//            return
//        }

        guard let emailAddress = email, !emailAddress.isEmpty else {
            completion(nil, RegisterModelResponse(isSuccess: false, message: "profile-empty-email".getString()))
            return
        }

        if !emailAddress.isValidEmail() {
            completion(nil, RegisterModelResponse(isSuccess: false, message: "profile-incorrect-email".getString()))
            return
        }

        var isMale = false

        if gender == "MALE" {
            isMale = true
        } else {
            isMale = false
        }

        let extraInfo = SubscriberExtraInfoDto(addressList: nil, avatarImageUrl: imageURL, birthDay: nil, economicCode: nil, education: nil, firstName: nil, fullName: fullName, gender: isMale ? .male : .female, lastName: nil, nationalCode: nil, tag: nil, workResume: nil)

        let sendData = SubscriberDto(email: emailAddress, extraInfo: extraInfo, _id: nil, locked: nil, mobile: mobileNumber, subscriberStatus: nil, tokens: nil, username: nil, password: nil, roles: nil)

        updateProfileService.updateProfileUsingPATCH1(subscriberDto: sendData) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (respnse) in
                if respnse.isSuccess {
                    completion(nil, RegisterModelResponse(isSuccess: true, message: ""))
                } else {
                    completion(nil, RegisterModelResponse(isSuccess: false, message: respnse.errorMessage))
                }
            }
        }
    }
}
