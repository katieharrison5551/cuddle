//
//  ReferalCodeViewModel.swift
//  cuddle
//
//  Created by MohammadReza on 10/24/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias CreateSubscriberLogCompletion = ((Void?, ErrorMessageModel?) -> Void)

protocol UserSubscriberLogProtocol {
    func createSubscriberLog(campaignId: String?, subscriberCode: String?, completion : @escaping CreateSubscriberLogCompletion)
}

class SubscriberLogViewModel: BaseViewModel {

    fileprivate var logService: createSubscriberLogProtocol
    init(referralService: createSubscriberLogProtocol = ReferralLogControllerAPI()) {
        self.logService = referralService
    }
}

extension SubscriberLogViewModel: UserSubscriberLogProtocol {
    func createSubscriberLog(campaignId: String?, subscriberCode: String?, completion: @escaping CreateSubscriberLogCompletion) {
        guard let campID = campaignId, !campID.isEmpty else {
            return
        }

        guard let code = subscriberCode, !code.isEmpty else {
            completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: "Field is empty"))
            return
        }

        logService.createSubscriberLog(referralLogInDto: ReferralLogInDto(campaignId: campID, subscriberCode: code)) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: response.errorMessage, errors: response.errors))
                }
            }
        }
    }
}
