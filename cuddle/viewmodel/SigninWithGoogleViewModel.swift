//
//  SigninWithGoogle.swift
//  cuddle
//
//  Created by Saeed on 8/11/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import GoogleSignIn

typealias SigninWithGoogleCompletation = ((_ user: GIDGoogleUser?, SigninModelResponse) -> Void)

protocol SigninWithGoogleProtocol {
    func signinWithGoogle(completation: @escaping SigninWithGoogleCompletation)
}

class SigninWithGoogleViewModel: BaseViewModel {
//    var signinService: googleRegisterByTokenProtocol
    var signinService: activateByCodeProtocol
    init(service: activateByCodeProtocol =  RegistrationControllerAPI()) {
        signinService = service
    }
}

extension SigninWithGoogleViewModel: SigninWithGoogleProtocol {
    func signinWithGoogle(completation: @escaping SigninWithGoogleCompletation) {
        if let user = GIDSignIn.sharedInstance()?.currentUser {
            // User signed in
             let activationModel = ActivationDto(activationCode: user.authentication.idToken, mobile: nil, email: nil, regType: ActivationDto.RegType.googleOneStep)
            signinService.activateByCode(activationDto: activationModel) { (response, error) in
                self.handleResponse(responseModel: response, error: error) { (response) in
                    if response.isSuccess {
                        completation(user, SigninModelResponse(isSuccess: true, message: SigninModelResponseMessage.successLogin.rawValue))
                    } else {
                        completation(nil, SigninModelResponse(isSuccess: false, message: SigninModelResponseMessage.failedLogin.rawValue))
                    }
                }
            }
        } else {
            // User signed out
            completation(nil, SigninModelResponse(isSuccess: true, message: SigninModelResponseMessage.notLogin.rawValue))
        }
    }
}
