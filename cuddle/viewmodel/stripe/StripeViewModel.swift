//
//  StripeViewModel.swift
//  cuddle
//
//  Created by Shima on 10/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
enum PaymentStatus: String {
    case success = "SUCCEEDED"
    case failure = "FAILED"
    case waiting = "Processing"
}
typealias StripeProcessPaymentCompletation = ((_ data: StripePaymentModel?, _ error: ErrorMessageModel?) -> Void)
typealias StripeDepositCompletation = ((_ data: WalletTransactionDto?, _ error: ErrorMessageModel?) -> Void)
typealias StripeGetWalletTransactionCompletation = ((_ data: WalletTransactionDto?, _ error: ErrorMessageModel?) -> Void)
protocol StripeDepositProtocol {
    func doStripeTransaction(amount: Int64, bankGatewayId: String, walletTransactionType: CreateSubscriberWalletTransactionDto.WalletTransactionType , completion: @escaping StripeDepositCompletation)
}
protocol StripeProcessPaymentProtocol {
    func processPayment(cardId: String, token: String, completion: @escaping StripeProcessPaymentCompletation)
}
protocol StripeWalletTransactionProtocol {
    func getWalletTransaction(walletId: String, completion: @escaping StripeGetWalletTransactionCompletation)
}
class StripeViewModel: BaseViewModel {
    fileprivate var stripeService: doSubscriberTransactionProtocol
    fileprivate var stripeProcess: processPaymentForStripe1Protocol
    fileprivate var stripeTransaction: getWalletTransactionProtocol
    init(stripeService: doSubscriberTransactionProtocol = WalletTransactionControllerAPI(),
         process: processPaymentForStripe1Protocol = PublicTransactionControllerAPI(),
         transaction: getWalletTransactionProtocol = WalletTransactionControllerAPI()) {
        self.stripeService = stripeService
        self.stripeProcess = process
        self.stripeTransaction = transaction
    }
}
extension StripeViewModel: StripeWalletTransactionProtocol {
    func getWalletTransaction(walletId: String, completion: @escaping StripeGetWalletTransactionCompletation) {
        self.stripeTransaction.getWalletTransaction(_id: walletId) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
extension StripeViewModel: StripeProcessPaymentProtocol {
    func processPayment(cardId: String, token: String, completion: @escaping StripeProcessPaymentCompletation) {
        self.stripeProcess.processPaymentForStripe1(dto: StripeTransactionProcessDto(cardId: cardId), token: token) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
extension StripeViewModel: StripeDepositProtocol {
    func doStripeTransaction(amount: Int64, bankGatewayId: String, walletTransactionType: CreateSubscriberWalletTransactionDto.WalletTransactionType, completion: @escaping StripeDepositCompletation) {

        let userWallet = CreateSubscriberWalletTransactionDto.init(amount: amount,
                                                                   bankGatewayId: bankGatewayId,
                                                                   clientCallbackUrl: nil,
                                                                   _description: nil,
                                                                   relatedId: FactorModel.modelInfo.rentId,
                                                                   walletTransactionType: walletTransactionType)
        
        stripeService.doSubscriberTransaction(createSubscriberWalletTransactionDto: userWallet) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
