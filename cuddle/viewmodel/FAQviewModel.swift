//
//  FAQviewModel.swift
//  cuddle
//
//  Created by MohammadReza on 10/20/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias GetAllFAQCompletation = ((PageQueAnsDto?, ErrorMessageModel?) -> Void)

protocol FAQprotocol {
    func getAllFAQ(completion: @escaping GetAllFAQCompletation)
}

class FAQviewModel: BaseViewModel {

    fileprivate var faqService: getAll1Protocol

    init(faqService: getAll1Protocol = QueAnsControllerAPI()) {
        self.faqService = faqService
    }

}

extension FAQviewModel: FAQprotocol {
    func getAllFAQ(completion: @escaping GetAllFAQCompletation) {
        faqService.getAll1(pageNumber: "0", pageSize: "100", sort: .asc, sortKey: [], targetWord: "") { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: response.errorMessage))
                }
            }
        }
    }
}
