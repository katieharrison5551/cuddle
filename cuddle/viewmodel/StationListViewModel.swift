//
//  StationListViewModel.swift
//  cuddle
//
//  Created by MohammadReza on 10/10/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias GetStationListCompletion = ((GetContentDto?, RegisterModelResponse?) -> Void)

protocol GetStationListServiceProtocol {
    func getStationList(contentId: String, byChildren: Bool, completion: @escaping GetStationListCompletion)
}

class StationListViewModel: BaseViewModel {
    fileprivate var stationListService: getContentProtocol!
    init(stationListService: getContentProtocol = ContentControllerAPI()) {
        self.stationListService = stationListService
    }
}

extension StationListViewModel: GetStationListServiceProtocol {
    func getStationList(contentId: String, byChildren: Bool, completion: @escaping GetStationListCompletion) {
        stationListService.getContent(contentId: contentId, byChildren: byChildren) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, RegisterModelResponse(isSuccess: true, message:
                            RegisterModelResponseMessage.successRegisteration.rawValue))
                } else {
                    completion(nil, RegisterModelResponse(isSuccess: false, message:
                    RegisterModelResponseMessage.failedRegisteration.rawValue))
                }
            }
        }
    }
}
