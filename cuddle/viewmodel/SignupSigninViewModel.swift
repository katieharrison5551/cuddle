//
//  SignupSigninViewModel.swift
//  cuddle
//
//  Created by Saeed on 8/10/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias RegisterCompletation = ((RegisterModelResponse) -> Void)

protocol RegisterUserProtocol {
    func registerUser(withMobile mobile: String?, completation: @escaping RegisterCompletation)
    func registerUser(withEmail email: String, completation: @escaping RegisterCompletation)
}

class SignupSigninViewModel: BaseViewModel {
    fileprivate var registerService: registerProtocol!
    init(registerService: registerProtocol = RegistrationControllerAPI()) {
        self.registerService = registerService
    }
}

extension SignupSigninViewModel: RegisterUserProtocol {
    func registerUser(withMobile mobile: String?, completation: @escaping RegisterCompletation) {
        guard let mobileNumber = mobile else {
            completation(RegisterModelResponse(isSuccess: false, message: RegisterModelResponseMessage.invalidMobile.rawValue))
            return
        }
        let registerModel = RegisterWithTypeDto.init(email: nil, mobile: mobileNumber, password: nil, registrationType: RegisterWithTypeDto.RegistrationType.smsOtp, username: nil)
        registerService.register(registerWithTypeDto: registerModel) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completation(RegisterModelResponse(isSuccess: true, message:
                            RegisterModelResponseMessage.successRegisteration.rawValue))
                } else {
                    completation(RegisterModelResponse(isSuccess: false, message:
                    RegisterModelResponseMessage.failedRegisteration.rawValue))
                }
            }
        }
    }
    fileprivate func isMobileValid(_ mobile: String) -> Bool {
        let phoneRegex = "^[0-9]{3}[0-9]{3}[0-9]{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: mobile)
    }
    ///Email
    func registerUser(withEmail email: String, completation: @escaping RegisterCompletation) {
        if !isEmailValid(email) {
            completation(RegisterModelResponse(isSuccess: false, message: RegisterModelResponseMessage.invalidEmail.rawValue))
            return
        }
        let registerModel = RegisterWithTypeDto.init(email: email, mobile: nil, password: nil, registrationType: RegisterWithTypeDto.RegistrationType.emailOtp, username: nil)
        registerService.register(registerWithTypeDto: registerModel) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completation(RegisterModelResponse(isSuccess: true, message:
                            RegisterModelResponseMessage.successRegisteration.rawValue))
                } else {
                    completation(RegisterModelResponse(isSuccess: false, message:
                    RegisterModelResponseMessage.failedRegisteration.rawValue))
                }
            }
        }
    }
    fileprivate func isEmailValid(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
