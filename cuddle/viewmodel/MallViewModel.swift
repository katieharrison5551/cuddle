//
//  MallViewModel.swift
//  cuddle
//
//  Created by Saeed on 10/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias GetMallInfoCompletion = ((GetMallDto?, ErrorMessageModel?) -> Void)
typealias GetMallStrollerCompletion = ((MallAndStrollerDto?, ErrorMessageModel?) -> Void)

protocol MallProtocol {
    func getMallDetail(mallId: String, completion: @escaping GetMallInfoCompletion)

    func getMallDetailByStrollerCode(service: getMallByStrollerCodeProtocol?, strollerCode: String, completion: @escaping GetMallStrollerCompletion)
}

class MallViewModel: BaseViewModel {

    fileprivate var mallDetailService: getMallProtocol!
    init(mallService: getMallProtocol = CuddleMallControllerAPI()) {
        self.mallDetailService = mallService
    }
}

extension MallViewModel: MallProtocol {
    func getMallDetail(mallId: String, completion: @escaping GetMallInfoCompletion) {
        mallDetailService.getMall(_id: mallId, byChildren: true) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }

    func getMallDetailByStrollerCode(service: getMallByStrollerCodeProtocol? = CuddleMallControllerAPI(), strollerCode: String, completion: @escaping GetMallStrollerCompletion) {
        service?.getMallByStrollerCode(strollerCode: strollerCode, completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        })
    }
}
