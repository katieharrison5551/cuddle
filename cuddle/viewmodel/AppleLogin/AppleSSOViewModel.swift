//  Created by Shima on 8/15/20.

import UIKit
import AuthenticationServices

@available(iOS 13.0, *)

protocol AppleSSOProtocol {
    func   setupProviderLoginView(_ stackView: UIStackView) -> ASAuthorizationAppleIDButton
    func  performExistingAccountSetupFlows()
    func handleAuthorizationAppleIDButtonPress()
    func signinWithAppleCallBack(token googleToken: String)
}
protocol SigninWithAppleProtocol {
    func signinCompleteion(userName: String?, password: String?, isSuccess: Bool, error: ErrorMessageModel?)
}
class AppleSSOViewModel: NSObject, APIHeaderProtocol, NetworkValidationProtocol {
    fileprivate var signinCallback: SigninWithAppleProtocol!
    var appleLoginService: activateByCodeProtocol =  RegistrationControllerAPI()
    init(signinHandler: SigninWithAppleProtocol) {
        signinCallback = signinHandler
    }
}
@available(iOS 13.0, *)
extension AppleSSOViewModel: AppleSSOProtocol {
    func signinWithAppleCallBack(token identityToken: String) {
        Utility.showHudLoading(title: "", message: "")
        let activationModel = ActivationDto(activationCode: identityToken, mobile: "", email: "", regType: ActivationDto.RegType.appleOneStep)
        appleLoginService.activateByCode(activationDto: activationModel) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    self.signinCallback.signinCompleteion(userName: response.responseModel?.username, password: identityToken, isSuccess: response.isSuccess, error: ErrorMessageModel(title: "", message: ""))
                } else {
                if  let error = response.errors {
                    self.signinCallback.signinCompleteion(userName: nil, password: nil, isSuccess: response.isSuccess, error: ErrorMessageModel(title: "", message: error[0].message ?? "", errors: error))
                } else {
                    self.signinCallback.signinCompleteion(userName: nil, password: nil, isSuccess: response.isSuccess, error: ErrorMessageModel(title: "", message: response.errorMessage))
                }
                }
            }
        }
    }
    func setupProviderLoginView(_ stackView: UIStackView) -> ASAuthorizationAppleIDButton {
        let authorizationButton = ASAuthorizationAppleIDButton()
        stackView.addArrangedSubview(authorizationButton)
        return authorizationButton
    }
    func performExistingAccountSetupFlows() {
        let requests = [ASAuthorizationAppleIDProvider().createRequest(),
                        ASAuthorizationPasswordProvider().createRequest()]
        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    func handleAuthorizationAppleIDButtonPress() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
}
@available(iOS 13.0, *)
extension AppleSSOViewModel: ASAuthorizationControllerDelegate {
    /// - Tag: did_complete_authorization
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            let userIdentifier = appleIDCredential.user
            let email = appleIDCredential.email
            Utility().savingData(inUserDefulat: email, keyOfUserDefault: "appleEmail")
            if let identityTokenData = appleIDCredential.identityToken {
                let identityToken = String(data: identityTokenData, encoding: .utf8)
                self.saveUserInKeychain(userIdentifier)
                self.signinWithAppleCallBack(token: identityToken ?? "")
            }
        case let passwordCredential as ASPasswordCredential:
            let username = passwordCredential.user
            let password = passwordCredential.password
            DispatchQueue.main.async {
                self.showPasswordCredentialAlert(username: username, password: password)
            }
        default:
            break
        }
    }
    private func saveUserInKeychain(_ userIdentifier: String) {
        if let bundleId = Bundle.main.bundleIdentifier {
            do {
                try KeychainItem(service: bundleId, account: "userIdentifier").saveItem(userIdentifier)
            } catch {
                print("Unable to save userIdentifier to keychain.")
            }
        }
    }
    private func showPasswordCredentialAlert(username: String, password: String) {
        let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
        let alertController = UIAlertController(title: "Keychain Credential Received",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    /// - Tag: did_complete_error
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
    }
}
@available(iOS 13.0, *)
extension AppleSSOViewModel: ASAuthorizationControllerPresentationContextProviding {
    /// - Tag: provide_presentation_anchor
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        if let rootView = UIApplication.shared.keyWindow?.rootViewController?.view.window {
            return rootView
        }
        return UIWindow()
    }
}
