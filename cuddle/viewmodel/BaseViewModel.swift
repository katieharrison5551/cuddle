//
//  BaseViewModel.swift
//  cuddle
//
//  Created by Saeed on 8/26/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

class BaseViewModel: APIHeaderProtocol, NetworkValidationProtocol {
    init() {
        SwaggerClientAPI.customHeaders = getDefaultHeader()
    }
}
