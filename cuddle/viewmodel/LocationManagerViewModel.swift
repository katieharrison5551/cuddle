//
//  LocationManagerViewModel.swift
//  cuddle
//
//  Created by Saeed on 8/17/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import GoogleMaps

protocol UpdateMapLocation: AnyObject {
    func updateMap(location: CLLocation)
}
class LocationManagerViewModel: NSObject {
    var locationManager: CLLocationManager!
    var currentLocation: CLLocation!

    let defaultLocation = CLLocation(latitude: -33.1447442, longitude: 157.6843112)
    weak var updateMapDelegate: UpdateMapLocation!
    init(updateMapDelegate: UpdateMapLocation) {
        super.init()
        self.updateMapDelegate = updateMapDelegate
        initLocationManager()
    }
    fileprivate func initLocationManager() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    }
}

extension LocationManagerViewModel: CLLocationManagerDelegate {
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location: CLLocation = locations.last {
            currentLocation = location
            updateMapDelegate.updateMap(location: location)
        }
    }
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways:
            print("Location access was granted.")
            fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        @unknown default:
            fatalError()
        }
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
    func getLocation() -> CLLocation {
        guard let location = currentLocation else {
            return defaultLocation
        }
        return location
    }
}
