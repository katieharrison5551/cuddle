//
//  GeoServiceViewModel.swift
//  cuddle
//
//  Created by Shima on 10/6/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//
//swiftlint:disable all
import Foundation

typealias GetMallsLocationsListCompletation = ((_ data: [GetContentLocationDto]?, _ error: ErrorMessageModel?) -> Void)

protocol GetMallsLocationsListProtocol {
    func getAllMallsList(byChildren: String, distance: String, lat: String, lng: String, tagTitle: String, type: String, keyword: String?, completion: @escaping GetMallsLocationsListCompletation)
}
class GeoServiceViewModel: BaseViewModel {
    fileprivate var geoService: getContentLocationsListProtocol
    init(geoService: getContentLocationsListProtocol = ContentControllerAPI()) {
        self.geoService = geoService
    }
}
extension GeoServiceViewModel: GetMallsLocationsListProtocol {
    func getAllMallsList(byChildren: String, distance: String, lat: String, lng: String, tagTitle: String, type: String, keyword: String?, completion: @escaping GetMallsLocationsListCompletation) {
        geoService.getContentLocationsList(byChildren: byChildren, content: "", distance: distance, keyword: keyword ?? "", lat: lat, lng: lng, summary: nil, tagTitle: nil, title: nil, type: type) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
