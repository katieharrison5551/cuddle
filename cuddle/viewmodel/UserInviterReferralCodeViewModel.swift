//
//  UserInviterReferralCode.swift
//  cuddle
//
//  Created by MohammadReza on 10/26/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias UserReferralListCompletion = ((ReferralUserDto?, ErrorMessageModel?) -> Void)

protocol UserReferralListProtocol {
    func getUserReferralList(completion: @escaping UserReferralListCompletion)
}
class UserInviterReferralCodeViewModel: BaseViewModel {
    fileprivate var referralListService: createReferralUserProtocol
    init(referralListService: createReferralUserProtocol = ReferralUserControllerAPI()) {
        self.referralListService = referralListService
    }
}
extension UserInviterReferralCodeViewModel: UserReferralListProtocol {
    func getUserReferralList(completion: @escaping UserReferralListCompletion) {
        referralListService.createReferralUser { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (responseData) in
                if responseData.isSuccess {
                    completion(responseData.responseModel, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: responseData.errorMessage))
                }
            }
        }
    }
}
