//
//  FileManagerViewModel.swift
//  cuddle
//
//  Created by Shima on 11/1/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias CreateFolderCompletation = ((_ data: FileOrFolder?,_ error: ErrorMessageModel?) -> Void)
typealias UploadFileCompletation = ((_ data: FileOrFolder?,_ error: ErrorMessageModel?) -> Void)

protocol CreateFolderInRootProtocol {
    func createFolderInRoot(name: String, completion: @escaping CreateFolderCompletation)
}
protocol UploadFileProtocol {
    func uploadfile(file: URL?, folderId: String?, name: String?, completion: @escaping UploadFileCompletation)
}
class FileManagerViewModel: BaseViewModel {
    fileprivate var createfolderService: createFolderProtocol
    fileprivate var uploadFileService: uploadProtocol

    init(createfolderService: createFolderProtocol = FileControllerAPI(), uploadFileService: uploadProtocol = FileControllerAPI()) {
        self.createfolderService = createfolderService
        self.uploadFileService = uploadFileService
    }
}
extension FileManagerViewModel: CreateFolderInRootProtocol {
    func createFolderInRoot(name: String, completion: @escaping CreateFolderCompletation) {
        createfolderService.createFolder(createFileRequest: CreateFileRequest(name: name, parentId: nil)) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: response.errorMessage))
                }
            }
        }
    }
}
extension FileManagerViewModel: UploadFileProtocol {
    func uploadfile(file: URL?, folderId: String?, name: String?, completion: @escaping UploadFileCompletation) {

        uploadFileService.upload(file: file, folderId: folderId, name: name) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: response.errorMessage))
                }
            }
        }
    }
}
