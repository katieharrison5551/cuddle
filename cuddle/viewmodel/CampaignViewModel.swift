//
//  CampaignViewModel.swift
//  cuddle
//
//  Created by MohammadReza on 10/24/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias CampaignListCompletation = ((PageCampaignOutDto?, ErrorMessageModel?) -> Void)

protocol CampaignListProtocol {
    func getCampaignList(completion: @escaping CampaignListCompletation)
}
class CampaignViewModel: BaseViewModel {
    fileprivate var campaignService: getListCampaignProtocol
    init(campaignService: getListCampaignProtocol = CampaignControllerAPI()) {
        self.campaignService = campaignService
    }
}
extension CampaignViewModel: CampaignListProtocol {
    func getCampaignList(completion: @escaping CampaignListCompletation) {
        campaignService.getListCampaign(pageNumber: "0", pageSize: "10", sort: Sort_getListCampaign(rawValue: "ASC")!, sortKey: [""]) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (responseData) in
                if responseData.isSuccess {
                    completion(responseData.responseModel, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: responseData.errorMessage))
                }
            }
        }
    }
}
