//
//  ActivateMobileEmailViewModel.swift
//  cuddle
//
//  Created by Saeed on 8/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias ActivateMobileCompletationHandler = (ActivateModelResponse) -> Void

protocol ActivateMobileProtocol {
    func activate(withMobile mobile: String?, code: String?, completionHandler: @escaping ActivateMobileCompletationHandler)
}

protocol ActivateEmailProtocol {
    func activate(withEmail email: String?, code: String?, completionHandler: @escaping ActivateMobileCompletationHandler)
}

class ActivateMobileEmailViewModel: BaseViewModel {

    var verifyService: activateByCodeProtocol!

    init(service: activateByCodeProtocol = RegistrationControllerAPI()) {
        verifyService = service
    }

    fileprivate func isActivationCodeValid(_ code: String?) -> Bool {

        guard let activationCode = code else {
            return false
        }
        if activationCode.isEmpty {
            return false
        }
        return true
    }
}

extension ActivateMobileEmailViewModel: ActivateMobileProtocol {
    func activate(withMobile mobile: String?, code: String?, completionHandler: @escaping ActivateMobileCompletationHandler) {
//        if !isMobileValid(mobile){
//            completionHandler(ActivateModelResponse(isSuccess: false, message: ActivateModelResponseMessage.InvalidMobile.rawValue))
//            return
//        }
        if !isActivationCodeValid(code) {
            completionHandler(ActivateModelResponse(isSuccess: false, message: ActivateModelResponseMessage.invalidVerificationCode.rawValue))
            return
        }
        let activationModel = ActivationDto.init(activationCode: code, mobile: mobile, email: nil, regType: .smsOtp)

        verifyService.activateByCode(activationDto: activationModel) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completionHandler(ActivateModelResponse(isSuccess: true, message: ActivateModelResponseMessage.successVerify.rawValue))
                } else {
                    completionHandler(ActivateModelResponse(isSuccess: false, message: ActivateModelResponseMessage.failedVerify.rawValue))
                }
            }
        }
    }

    fileprivate func isMobileValid(_ mobile: String?) -> Bool {

        guard let mobileNumber = mobile else {
            return false
        }

        if mobileNumber.isEmpty {
            return false
        }
        let phoneRegex = "^[0-9]{3}[0-9]{3}[0-9]{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: mobile)
    }
}

extension ActivateMobileEmailViewModel: ActivateEmailProtocol {
    func activate(withEmail email: String?, code: String?, completionHandler: @escaping ActivateMobileCompletationHandler) {
        if !isEmailValid(email) {
            completionHandler(ActivateModelResponse(isSuccess: false, message: ActivateModelResponseMessage.invalidEmail.rawValue))
            return
        }
        if !isActivationCodeValid(code) {
            completionHandler(ActivateModelResponse(isSuccess: false, message: ActivateModelResponseMessage.invalidVerificationCode.rawValue))
            return
        }

        let activationModel = ActivationDto.init(activationCode: code, mobile: nil, email: email, regType: .emailOtp)

        verifyService.activateByCode(activationDto: activationModel) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completionHandler(ActivateModelResponse(isSuccess: true, message: ActivateModelResponseMessage.successVerify.rawValue))
                } else {
                    completionHandler(ActivateModelResponse(isSuccess: false, message: ActivateModelResponseMessage.failedVerify.rawValue))
                }
            }
        }

    }

    fileprivate func isEmailValid(_ email: String?) -> Bool {

        guard let userEmail = email else {
            return false
        }

        if userEmail.isEmpty {
            return false
        }

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: userEmail)
    }
}
