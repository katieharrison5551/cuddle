//
//  BankGatewayViewModel.swift
//  cuddle
//
//  Created by Shima on 10/13/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
typealias AllBankGatewayCompletation = ((_ data: PageBankGatewayDto?, _ error: ErrorMessageModel?) -> Void)

protocol AllBankGatewayProtocol {
    func getAllBankGateway(pageNumber: String, pageSize: String, sort: Sort_getAllBankGateway, sortKey: [String]?, completion: @escaping AllBankGatewayCompletation)
}
class BankGatewayViewModel: BaseViewModel {
    fileprivate var allBankService: getAllBankGatewayProtocol
    init(allBankService: getAllBankGatewayProtocol = BankGatewayControllerAPI()) {
        self.allBankService = allBankService
    }
}
extension BankGatewayViewModel: AllBankGatewayProtocol {
    func getAllBankGateway(pageNumber: String, pageSize: String, sort: Sort_getAllBankGateway, sortKey: [String]?, completion: @escaping AllBankGatewayCompletation) {
        allBankService.getAllBankGateway(pageNumber: pageNumber, pageSize: pageSize, sort: sort, sortKey: sortKey ?? []) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
