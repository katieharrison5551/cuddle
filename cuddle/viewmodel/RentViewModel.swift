//
//  RentViewModel.swift
//  cuddle
//
//  Created by Saeed on 10/10/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias RentStrollerCompletion = ((GetMallDto?, ErrorMessageModel?) -> Void)
typealias EndRentStrollerCompletion = ((InProgressRentModel?, ErrorMessageModel?) -> Void)
typealias ActiveRentListCompletation = ((_ data: [InProgressRentModel]?, _ error: ErrorMessageModel?) -> Void)

protocol RentProtocol {
    func rentStroller(strollerId: String, completion: @escaping RentStrollerCompletion)
}
protocol EndRentProtocol {
    func endRentStroller(strollerIdKey: String?, rentService: endRentProtocol?, completion: @escaping EndRentStrollerCompletion)
}
protocol AllActiveRentListProtocol {
    func getActiveRentList(completion: @escaping ActiveRentListCompletation)
}
class RentViewModel: BaseViewModel {
    fileprivate var rentStrollerService: createRentProtocol!
    fileprivate var activeRentService: getInProgressRentListProtocol!
    init(rentService: createRentProtocol = RentControllerAPI(), activeRent: getInProgressRentListProtocol = RentControllerAPI()) {
        self.rentStrollerService = rentService
        self.activeRentService = activeRent
    }
}
extension RentViewModel: RentProtocol {
    func rentStroller(strollerId: String, completion: @escaping RentStrollerCompletion) {
        self.rentStrollerService.createRent(startRentDto: StartRentDto(strollerCode: strollerId.lowercased())) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: response.errorMessage, code: Int32(response.errorCode ?? 500)))
                }
            }
        }
    }
}
extension RentViewModel: EndRentProtocol {
    func endRentStroller(strollerIdKey: String?, rentService: endRentProtocol? = nil, completion: @escaping EndRentStrollerCompletion) {
        var service: endRentProtocol? = rentService

        if service == nil {
            service = RentControllerAPI()
        }

        guard let strollerId = strollerIdKey else {
            completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""),
                                              message: NSLocalizedString("strollerId-not-found", comment: "")))
            return
        }

        let stroller = StartRentDto(strollerCode: strollerId.lowercased())
        service?.endRent(startRentDto: stroller, completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                    return
                }
                completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
            }
        })
    }
}
extension RentViewModel: AllActiveRentListProtocol {
    func getActiveRentList(completion: @escaping ActiveRentListCompletation) {
        self.activeRentService.getInProgressRentList { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
