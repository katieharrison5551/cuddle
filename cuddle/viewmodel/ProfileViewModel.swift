//
//  ProfileViewModel.swift
//  cuddle
//
//  Created by MohammadReza on 8/17/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias ProfileCompletion = ((SubscriberDto?, ErrorMessageModel?) -> Void)

protocol ProfileUserProtocol {
    func loadProfileUser(completion : @escaping ProfileCompletion)
}
class ProfileViewModel: BaseViewModel {
    fileprivate var profileService: getProfileProtocol
    init(profileService: getProfileProtocol = SubscriberControllerAPI()) {
        self.profileService = profileService
    }
}
extension ProfileViewModel: ProfileUserProtocol {
    func loadProfileUser(completion: @escaping ProfileCompletion) {
        profileService.getProfile { (response, error) in
            self.handleResponse(responseModel: response, error: error) { (responseData) in
                if responseData.isSuccess {
                    completion(responseData.responseModel!, nil)
                } else {
                    completion(nil, ErrorMessageModel(title: "error".getString(), message: responseData.errorMessage))
                }
            }
        }
    }
}
