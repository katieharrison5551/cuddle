//
//  TicketMessageViewModel.swift
//  cuddle
//
//  Created by Saeed on 10/13/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias SendTicketMessageCompletion = ((TicketMessageDto?, ErrorMessageModel?) -> Void)
typealias GetTicketMessagesCompletion = ((PageTicketMessageDto?, ErrorMessageModel?) -> Void)
protocol SendTicketMessageProtocol {
    func sendTicketMessage(ticketService: createTicketMessageProtocol?, ticketId: String, content: String, completion: @escaping SendTicketMessageCompletion)
}
protocol GetTicketMessagesProtocol {
    func getTicketMessages(ticketService: getTicketMessageListByTicketIdProtocol?, ticketId: String, pageNumber: String, pageSize: String, completion: @escaping GetTicketMessagesCompletion)
}
class TicketMessageViewModel: BaseViewModel {
}
extension TicketMessageViewModel {
    func sendTicketMessage(ticketService: createTicketMessageProtocol? = nil, ticketId: String, content: String,  completion: @escaping SendTicketMessageCompletion) {
        var service: createTicketMessageProtocol? = ticketService
        if service == nil {
            service = TicketMessageControllerAPI()
        }
        let ticket = TicketMessageDto(ticketId: ticketId, content: content, subscriberId: nil, _id: nil, createdDateMilli: nil, fullName: UserInfoModel.shared.getUsername())
        service?.createTicketMessage(ticketMessageDto: ticket, completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                    return
                }
                completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""),
                                                  message: response.errorMessage, errors: response.errors))
            }
        })
    }
}
extension TicketMessageViewModel: GetTicketMessagesProtocol {
    func getTicketMessages(ticketService: getTicketMessageListByTicketIdProtocol? = nil, ticketId: String, pageNumber: String, pageSize: String, completion: @escaping GetTicketMessagesCompletion) {
        var service: getTicketMessageListByTicketIdProtocol? = ticketService
        if service == nil {
            service = TicketMessageControllerAPI()
        }
        service?.getTicketMessageListByTicketId(ticketId: ticketId, pageNumber: pageNumber, pageSize: pageSize, sort: .desc, sortKey: ["createdDate"], completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                    return
                }
                completion(nil, ErrorMessageModel(title: "", message: response.errorMessage, errors: response.errors))
            }
        })
    }
}
