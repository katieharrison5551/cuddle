//
//  SupportViewModel.swift
//  cuddle
//
//  Created by Saeed on 10/14/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias SendMessageCompletion = ((TicketMessageDto?, ErrorMessageModel?) -> Void)

typealias LoadMessageCompletion = ((PageTicketMessageDto?, ErrorMessageModel?) -> Void)

typealias InitializedTicketCompletion = ((Bool, String?, ErrorMessageModel?) -> Void)

protocol InitializedProtocol {
    func initializedTicket(completion: @escaping InitializedTicketCompletion)
}
protocol SendMessageProtocol {
    func sendMessage(message: String, completion: @escaping SendMessageCompletion)
}

protocol LoadMessageProtocol {
    func loadMessage(pageNumber: String, pageSize: String, completion: @escaping LoadMessageCompletion)
}

class SupportViewModel: BaseViewModel {

    fileprivate var categoryId: String!
    fileprivate var ticketId: String!
    fileprivate var supportCategoryViewModel: SupportCategoryViewModel!
    fileprivate var ticketViewModel: TicketViewModel!
    fileprivate var ticketMessageViewModel: TicketMessageViewModel!

    override init() {
        super.init()
        supportCategoryViewModel = SupportCategoryViewModel()
        ticketViewModel = TicketViewModel()
        ticketMessageViewModel = TicketMessageViewModel()
    }

    func getCategoryId(completion: @escaping InitializedTicketCompletion) {
        supportCategoryViewModel.getSupportCategory { (response, error) in
            if error == nil {
                self.categoryId = response?._id
                self.getTicketId(completion: completion)

                return
            }

            completion(false, nil, error)
        }
    }

    func getTicketId( completion: @escaping InitializedTicketCompletion) {
        if categoryId == nil {
            getCategoryId(completion: completion)
            return
        }

        ticketViewModel.openTicket(subject: "Support", content: "Support", priority: .regular, categoryId: categoryId) { (response, error) in
            if error == nil {
                self.ticketId = response?._id
                completion(true, response?.subscriberId, nil)
                return
            }

            completion(false, nil, error)
        }
    }
}

extension SupportViewModel: InitializedProtocol {
    func initializedTicket(completion: @escaping InitializedTicketCompletion) {
        getCategoryId(completion: completion)
    }
}

extension SupportViewModel: SendMessageProtocol {
    func sendMessage(message: String, completion: @escaping SendMessageCompletion) {
        if categoryId == nil {
            completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("unavailable-support-category", comment: "")))
            return
        }

        if ticketId == nil {
            completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("unavailable-support-ticket", comment: "")))
            return
        }

        ticketMessageViewModel.sendTicketMessage(ticketId: ticketId, content: message) { (response, error) in
            completion(response, error)
        }
    }
}

extension SupportViewModel: LoadMessageProtocol {
    func loadMessage(pageNumber: String, pageSize: String, completion: @escaping LoadMessageCompletion) {
        if categoryId == nil {
            completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("unavailable-support-category", comment: "")))
        }

        if ticketId == nil {
            completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: NSLocalizedString("unavailable-support-ticket", comment: "")))
        }

        ticketMessageViewModel.getTicketMessages(ticketId: ticketId, pageNumber: pageNumber, pageSize: pageSize) { (response, error) in
            if error == nil {
                completion(response, nil)
                return
            }

            completion(nil, error)
        }
    }
}
