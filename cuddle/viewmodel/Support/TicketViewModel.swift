//
//  TicketViewModel.swift
//  cuddle
//
//  Created by Saeed on 10/13/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias CreateTicketCompletion = ((TicketDto?, ErrorMessageModel?) -> Void)

typealias GetTicketListCompletion = ((TicketDto?, ErrorMessageModel?) -> Void)

typealias OpenTicketCompletion = ((TicketDto?, ErrorMessageModel?) -> Void)

protocol CreateTicketProtocol {
    func createTicket(ticketService: createTicketProtocol?, subject: String, content: String, priority: TicketDto.Priority, categoryId: String, completion: @escaping CreateTicketCompletion)
}

protocol GetTicketListProtocol {
    func getTicketList(ticketService: getSubscriberTicketsProtocol?, pageSize: String?, pageNumber: String?, completion: @escaping GetTicketListCompletion)
}

protocol OpenTicketProtocol {
    func openTicket(subject: String, content: String, priority: TicketDto.Priority, categoryId: String, completion: @escaping OpenTicketCompletion)
}

class TicketViewModel: BaseViewModel {
    let closedTicket: Int32 = 1001
}

extension TicketViewModel: OpenTicketProtocol {

    func openTicket(subject: String, content: String, priority: TicketDto.Priority, categoryId: String, completion: @escaping OpenTicketCompletion) {
        getTicketList { (ticket, error) in
            if error == nil {
                completion(ticket, nil)
                return
            } else if error?.code == self.closedTicket {
                self.createTicket(subject: subject, content: content, priority: priority, categoryId: categoryId) { (ticket, error) in
                    if error == nil {
                        completion(ticket, nil)
                        return
                    } else {
                        completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: error?.message ?? "server-unreachable".getString()))
                    }
                }
            } else {
                completion(nil, ErrorMessageModel(title: NSLocalizedString("error", comment: ""), message: error?.message ?? "server-unreachable".getString()))
            }
        }
    }
}

extension TicketViewModel: CreateTicketProtocol {
    func createTicket(ticketService: createTicketProtocol? = nil, subject: String, content: String, priority: TicketDto.Priority, categoryId: String, completion: @escaping CreateTicketCompletion) {
        var service: createTicketProtocol? = ticketService
        if service == nil {
            service = TicketControllerAPI()
        }
        let ticket = TicketDto(categoryName: "Support", createdDateMilli: nil, _id: nil, state: nil, subscriberId: nil, subject: subject, content: content, priority: priority, categoryId: categoryId)
        service?.createTicket(ticketDto: ticket, completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(response.responseModel, nil)
                    return
                }
                completion(nil, ErrorMessageModel(title: "", message: response.errorMessage, errors: response.errors))
            }
        })
    }
}
extension TicketViewModel: GetTicketListProtocol {
    func getTicketList(ticketService: getSubscriberTicketsProtocol? = nil,  pageSize: String? = nil, pageNumber: String? = nil, completion: @escaping GetTicketListCompletion) {
        var service: getSubscriberTicketsProtocol? = ticketService
        if service == nil {
            service = TicketControllerAPI()
        }
        service?.getSubscriberTickets(content: "", keyword: "", pageNumber: pageNumber ?? "0", pageSize: pageSize ?? "10", sort: Sort_getSubscriberTickets.asc, sortKey: [], subject: "", ticketStateEnum: TicketStateEnum_getSubscriberTickets.opened, completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess, let tickets = response.responseModel?.content {
                    for ticket in tickets {
                        if ticket.state?.current?.state == StateDtostring.State.opened {
                            completion(ticket, nil)
                            return
                        }
                    }
                    completion(nil, ErrorMessageModel(title: "", message: "", code: self.closedTicket))
                }
                completion(nil, ErrorMessageModel(title: "", message: response.errorMessage, errors: response.errors))
            }
        })
    }
}
