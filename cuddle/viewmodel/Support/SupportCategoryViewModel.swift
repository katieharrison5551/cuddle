//
//  SupportCategoryViewModel.swift
//  cuddle
//
//  Created by Saeed on 10/13/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias GetSupportCategoryCompletion = ((SupportCategoryDto?, ErrorMessageModel?) -> Void)

protocol SupportCategoryProtocol {
    func getSupportCategory(categoryService: getCategoryListProtocol?, completion: @escaping GetSupportCategoryCompletion)
}

class SupportCategoryViewModel: BaseViewModel {

}

extension SupportCategoryViewModel: SupportCategoryProtocol {
    func getSupportCategory(categoryService: getCategoryListProtocol? = nil, completion: @escaping GetSupportCategoryCompletion) {
        var service: getCategoryListProtocol? = categoryService

        if service == nil {
            service = SupportCategoryControllerAPI()
        }

        service?.getCategoryList(pageNumber: "0", pageSize: "10", sort: .asc, sortKey: [], completion: { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    if let category = response.responseModel?.content?[0] {
                        completion(category, nil)
                        return
                    }
                }

                completion(nil, ErrorMessageModel(title: "", message: response.errorMessage))
            }
        })
    }
}
