//
//  GetTokenViewModel.swift
//  cuddle
//
//  Created by Saeed on 9/30/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation

typealias AuthenticateCompletation = ((RegisterModelResponse) -> Void)

protocol AuthenticationProtocol {
    func authenticate(withUser mobile: String?, andPass pass: String?, completation: @escaping AuthenticateCompletation)
    func authenticate(withToken token: String, completation: @escaping AuthenticateCompletation)
}

class GetTokenViewModel: BaseViewModel {

}

extension GetTokenViewModel: AuthenticationProtocol {
    func authenticate(withUser mobile: String?, andPass pass: String?, completation: @escaping AuthenticateCompletation) {
        SwaggerClientAPI.customHeaders = getTokenHeader()
        AuthenticationAPI.postTokenRequest(username: mobile, password: pass) { (accessToken, error) in
            self.handleResponse(responseModel: accessToken, error: error) { (response) in
                if response.isSuccess {
                    UserInfoModel.shared.setUserToken(accessToken: response.responseModel?.access_token, accessTokenType: response.responseModel?.token_type)
                    UserInfoModel.shared.setUsername(username: mobile ?? "")

                    completation(RegisterModelResponse(isSuccess: true, message: ""))
                    return
                }

                completation(RegisterModelResponse(isSuccess: false, message:
                RegisterModelResponseMessage.failedRegisteration.rawValue))
            }
        }
    }

    func authenticate(withToken token: String, completation: @escaping AuthenticateCompletation) {

    }
}
