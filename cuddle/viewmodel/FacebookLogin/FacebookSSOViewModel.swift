//
//  FacebookSSOViewModel.swift
//  cuddle
//
//  Created by Shima on 10/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import Foundation
import FBSDKLoginKit

typealias FBLoginCompletion = ((_ data: Bool?, _ error: ErrorMessageModel?) -> Void)

protocol FacebookSSOProtocol {
    func loginWithFacebook(user: String, password: String)
    func login(controller: UIViewController)
}
protocol SigninWithFBProtocol {
    func fbSigninCompleteion(isSuccess: Bool, error: ErrorMessageModel?)
}
class FacebookSSOViewModel: BaseViewModel {
    fileprivate var fbSigninCallback: SigninWithFBProtocol!
    init(fbSigninHandler: SigninWithFBProtocol) {
        self.fbSigninCallback = fbSigninHandler
    }
}
extension FacebookSSOViewModel: FacebookSSOProtocol {
    func loginWithFacebook(user: String, password: String) {
        //call backend web service for get user access token
        self.fbSigninCallback.fbSigninCompleteion(isSuccess: true, error: nil)
    }
    func login(controller: UIViewController) {
        var userEmail: String = ""
        LoginManager().logIn(permissions: ["email"], from: controller) { (result, error) -> Void in
            if error == nil {
                if let fresult = result {
                    let fbloginresult: LoginManagerLoginResult = fresult
                    if fresult.isCancelled {
                        self.fbSigninCallback.fbSigninCompleteion(isSuccess: false, error: ErrorMessageModel(title: "", message: "User Tapped On Cancel Button."))
                        return
                    }
                    if fbloginresult.grantedPermissions.contains("email") {
                        if AccessToken.current != nil {
                            Utility.showHudLoading()
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (_, result, error) -> Void in
                                if error == nil {
                                    let userInfoDic = (result as? [String: AnyObject])
                                    if let username = userInfoDic?["email"] as? String {
                                    userEmail = username
                                    }
                                let fbAccessToken = AccessToken.current?.tokenString ?? ""
                                self.loginWithFacebook(user: userEmail, password: fbAccessToken)
                                } else {
                                     Utility.hideHudLoading()
                                }
                            })
                        }
                    }
                }
            } else {
                self.fbSigninCallback.fbSigninCompleteion(isSuccess: false, error: ErrorMessageModel(title: "", message: "something Wrong happened"))
            }
        }
    }
}
