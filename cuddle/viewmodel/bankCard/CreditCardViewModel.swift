//
//  CreditCardViewModel.swift
//  cuddle
//
//  Created by Shima on 9/30/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

// swiftlint:disable all
import Foundation

typealias CreateCardCompletation = ((_ data: BankCardDto?,_ error: ErrorMessageModel?) -> Void)
typealias GetAllCardsCompletation = ((_ data: [BankCardDto]?,_ error: ErrorMessageModel?) -> Void)
typealias DeleteCardCompletation = ((_ data: Bool?,_ error: ErrorMessageModel?) -> Void)

protocol CreateCreditCardProtocol {
    func createUpdateCard(address: String?, city: String?, country: String?, fullName: String?, state: String?, zipCode: String?,number:  String?, cvv2:  String?, expirationYear:  String?, expirationMonth:  String?, _id: String? , completion: @escaping CreateCardCompletation)
}
protocol GetAllCreditCardsProtocol {
    func getAllCards(pageNumber: String?, pageSize: String?, sort: Sort_getAll?, sortKey: [String]?, completion: @escaping GetAllCardsCompletation)
}
protocol DeleteCreditCardProtocol {
    func deleteCard(cardId: String?, completion: @escaping DeleteCardCompletation)
}
class CreditCardViewModel: BaseViewModel {
    fileprivate var creditCardGetAllService: getAllBankCardsProtocol!
    fileprivate var creditCardCreateService: createBankCardProtocol!
    fileprivate var creditCardDeleteService: deleteBankCardProtocol!
    fileprivate var creditCardUpdateService: updateBankCardProtocol!
    init(creditCardCreateService: createBankCardProtocol = BankCardControllerAPI(), creditCardGetAllService: getAllBankCardsProtocol = BankCardControllerAPI(), creditCardDeleteService: deleteBankCardProtocol = BankCardControllerAPI(), creditCardUpdateService: updateBankCardProtocol = BankCardControllerAPI() ) {
        self.creditCardCreateService = creditCardCreateService
        self.creditCardGetAllService = creditCardGetAllService
        self.creditCardDeleteService = creditCardDeleteService
        self.creditCardUpdateService = creditCardUpdateService
    }
}
extension CreditCardViewModel: DeleteCreditCardProtocol {
    func deleteCard(cardId: String?, completion: @escaping DeleteCardCompletation) {
        guard let id = cardId else{
            completion(nil, ErrorMessageModel(title: "", message: "Invalid Card"))
            return
        }
        creditCardDeleteService.deleteBankCard(_id: id) { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(true , nil)
                } else {
                    completion(nil , ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
extension CreditCardViewModel: GetAllCreditCardsProtocol {
    func getAllCards(pageNumber: String?, pageSize: String?, sort: Sort_getAll?, sortKey: [String]?, completion: @escaping GetAllCardsCompletation) {
        creditCardGetAllService.getAllBankCards() { (data, error) in
            self.handleResponse(responseModel: data, error: error) { (response) in
                if response.isSuccess {
                    completion(data , nil)
                } else {
                    completion(nil , ErrorMessageModel(title: "", message: response.errorMessage))
                }
            }
        }
    }
}
extension CreditCardViewModel: CreateCreditCardProtocol {
    
    func createUpdateCard(address: String?, city: String?, country: String?, fullName: String?, state: String?, zipCode: String?, number: String?, cvv2: String?, expirationYear: String?, expirationMonth: String?, _id : String?, completion: @escaping CreateCardCompletation) {
        guard let cardNum = number, !cardNum.isEmpty else{
            completion(nil, ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.cardNum.rawValue))
            return
        }
        if !self.isCreditNumValid(cardNum)
        {
            completion(nil, ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.cardNum.rawValue))
            return
        }
        guard let expMonth = expirationMonth, !expMonth.isEmpty else{
            completion(nil, ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.expMonth.rawValue))
            return
        }
        guard let expYear = expirationYear, !expYear.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.expYear.rawValue))
            return
        }
        guard let cvv = cvv2, !cvv.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.cvv2.rawValue))
            return
        }
        guard let userName = fullName, !userName.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.name.rawValue))
            return
        }
        guard let userAddress = address, !userAddress.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.address.rawValue))
            return
        }
        guard let userZipCode = zipCode, !userZipCode.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.zipcode.rawValue))
            return
        }
        guard let userCountry = country, !userCountry.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.country.rawValue))
            return
        }
        guard let userCity = city, !userCity.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.city.rawValue))
            return
        }
        guard let userState = state, !userState.isEmpty else{
            completion(nil,
                       ErrorMessageModel(title: "", message: "", code: CardValidationErrorEnum.state.rawValue))
            return
        }
        let userCompleteAdd = BankCardAddressDto(address: userAddress, city: userCity, country: userCountry, fullName: userName, state: userState, zipCode: userZipCode)
              let extraInfo = BankCardExtraInfoDto(address: userCompleteAdd)
        var userBankCard = BankCardDto(extraInfo: extraInfo, _id: nil, selected: true, number: cardNum, sheba: nil, cvv2: cvv, expirationYear: Int(expYear), expirationMonth: Int(expMonth))
        if let idString = _id, !idString.isEmpty {
            userBankCard = BankCardDto(extraInfo: extraInfo, _id: idString, selected: true, number: cardNum, sheba: nil, cvv2: cvv, expirationYear: Int(expYear), expirationMonth: Int(expMonth))
        creditCardUpdateService.updateBankCard(bankCardDto: userBankCard, _id: idString) { (data, error) in
                   self.handleResponse(responseModel: data, error: error) { (response) in
                       if response.isSuccess {
                           completion(nil , nil)
                       } else {
                        if  let errors = response.errors {
                           completion(nil, self.handleErrors(errors))
                        }
                    }
            }
            }
        } else {
            creditCardCreateService.createBankCard(bankCardDto: userBankCard) { (data, error) in
                self.handleResponse(responseModel: data, error: error) { (response) in
                    if response.isSuccess {
                        completion(data , nil)
                    } else {
                        if  let errors = response.errors {
                           completion(nil, self.handleErrors(errors))
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func isCreditNumValid(_ creditNum: String) -> Bool {
        if creditNum.count == 16
        {
            return true
        }
        return false
    }
    fileprivate func handleErrors(_ errors: [Errors]) -> ErrorMessageModel? {
         for i in 0...errors.count-1 {
                                    switch errors[i].field {
                                    case CardValidationErrorMessageFieldEnum.cardNum.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.cardNum.rawValue)
                                    case CardValidationErrorMessageFieldEnum.expMonth.rawValue:
                                       return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.expMonth.rawValue)
                                    case CardValidationErrorMessageFieldEnum.expYear.rawValue:
                                       return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.expYear.rawValue)
                                    case CardValidationErrorMessageFieldEnum.cvv2.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.cvv2.rawValue)
                                    case CardValidationErrorMessageFieldEnum.name.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.name.rawValue)
                                    case CardValidationErrorMessageFieldEnum.address.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.address.rawValue)
                                    case CardValidationErrorMessageFieldEnum.zipcode.rawValue:
                                       return ErrorMessageModel(title: "", message: errors[i].message!, code: CardValidationErrorEnum.zipcode.rawValue)
                                    case CardValidationErrorMessageFieldEnum.country.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.country.rawValue)
                                    case CardValidationErrorMessageFieldEnum.city.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.city.rawValue)
                                    case CardValidationErrorMessageFieldEnum.state.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.state.rawValue)
                                    case CardValidationErrorMessageFieldEnum.sheba.rawValue:
                                        return ErrorMessageModel(title: "", message: errors[i].field! + " " + errors[i].message!, code: CardValidationErrorEnum.sheba.rawValue)
                                    default:
                                        break
                                    }
                                }
        return ErrorMessageModel(title: "", message:"")
        
       }
}
