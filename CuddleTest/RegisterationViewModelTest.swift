//
//  RegisterationViewModelTest.swift
//  CuddleTests
//
//  Created by Saeed on 8/10/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import XCTest
import Cuddle

class RegisterationViewModelTest: XCTestCase {
    var signupSigninViewModel: SignupSigninViewModel!
    override func setUp() {
        signupSigninViewModel = SignupSigninViewModel(registerService: MockRegisterService())
    }
    func testMobileNumber() throws {
        signupSigninViewModel.registerUser(withMobile: "2679025237") { (response) in
            assert(response.isSuccess)
            assert(response.message == RegisterModelResponseMessage.SuccessRegisteration.rawValue)
        }
        signupSigninViewModel.registerUser(withMobile: nil) { (response) in
            assert(!response.isSuccess)
            assert(response.message == RegisterModelResponseMessage.InvalidMobile.rawValue)
        }
        signupSigninViewModel.registerUser(withMobile: "09195769712") { (response) in
            assert(!response.isSuccess)
            assert(response.message == RegisterModelResponseMessage.InvalidMobile.rawValue)
        }
        signupSigninViewModel.registerUser(withMobile: "") { (response) in
            assert(!response.isSuccess)
            assert(response.message == RegisterModelResponseMessage.InvalidMobile.rawValue)
        }
    }
    func testEmailFormat() throws {
        signupSigninViewModel.registerUser(withEmail: "s@s.com") { (response) in
            assert(response.isSuccess)
            assert(response.message == RegisterModelResponseMessage.SuccessRegisteration.rawValue)
        }
        signupSigninViewModel.registerUser(withEmail: "vasl.ir") { (response) in
            assert(!response.isSuccess)
        }
        signupSigninViewModel.registerUser(withEmail: "") { (response) in
            assert(!response.isSuccess)
        }
        signupSigninViewModel.registerUser(withEmail: "12john@home@") { (response) in
            assert(!response.isSuccess)
            assert(response.message == RegisterModelResponseMessage.InvalidEmail.rawValue)
        }
    }
    class MockRegisterService: registerUsingPOSTProtocol {
        func registerUsingPOST(registerWithTypeDto: RegisterWithTypeDto, completion: @escaping registerUsingPOSTCompletation) {
            completion(nil, nil)
        }
    }
}
