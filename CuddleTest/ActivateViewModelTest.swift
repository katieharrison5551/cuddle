//
//  ActivateViewModelTest.swift
//  CuddleTest
//
//  Created by Saeed on 8/12/20.
//  Copyright © 2020 Cuddle. All rights reserved.
//

import XCTest
import Cuddle

class ActivateViewModelTest: XCTestCase {

    var activateViewModel: ActivateMobileEmailViewModel!
    override func setUp() {
        activateViewModel = ActivateMobileEmailViewModel(service: MockActivateService())
    }
    func testMobileNumber() throws {
        activateViewModel.activate(withMobile: "2679025237", code: "123") { (response) in
            assert(response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.SuccessVerify.rawValue)
        }
        activateViewModel.activate(withMobile: nil, code: "123") { (response) in
            assert(!response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.InvalidMobile.rawValue)
        }
        activateViewModel.activate(withMobile: "09195769712", code: "123") { (response) in
            assert(!response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.InvalidMobile.rawValue)
        }
        activateViewModel.activate(withMobile: "", code: "123") { (response) in
            assert(!response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.InvalidMobile.rawValue)
        }
    }
    func testEmailFormat() throws {
        activateViewModel.activate(withEmail: "s@s.com", code: "123") { (response) in
            assert(response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.SuccessVerify.rawValue)
        }
        activateViewModel.activate(withEmail: "vasl.ir", code: "123") { (response) in
            assert(!response.isSuccess)
        }
        activateViewModel.activate(withEmail: "", code: "123") { (response) in
            assert(!response.isSuccess)
        }
        activateViewModel.activate(withEmail: "12john@home@", code: "123") { (response) in
            assert(!response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.InvalidEmail.rawValue)
        }
    }
    func testActivationCode() throws {
        activateViewModel.activate(withMobile: "2679025237", code: "123") { (response) in
            assert(response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.SuccessVerify.rawValue)
        }
        activateViewModel.activate(withMobile: "2679025237", code: nil) { (response) in
            assert(!response.isSuccess)
            assert(response.message == ActivateModelResponseMessage.InvalidVerificationCode.rawValue)
        }
    }
    class MockActivateService: activateByCodeUsingPOSTProtocol {
        func activateByCodeUsingPOST(activationDto: ActivationDto, completion: @escaping activateByCodeUsingPOSTCompletation) {
            completion(nil, nil)
        }
    }
}
